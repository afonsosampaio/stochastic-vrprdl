package rdlp.test;

import ilog.concert.IloException;
import rdlp.algorithms.exact.mip.MIP;
import rdlp.algorithms.heuristic.constructive.ConstructiveHeuristic;
import rdlp.io.RDLParser;
import rdlp.model.RDLPInstance;
import rdlp.model.RDLPSolution;
import rdlp.util.TimeUtils;

import java.io.File;

import static rdlp.util.TimeUtils.NANOSECONDS_TO_MILLISECONDS;

/**
 * Created by jkinable on 7/2/15.
 */
public class ConstructiveHeuristicTest {
  public static void main(String[] args) throws IloException {
    //        RDLPInstance dataModel= RDLParser.parse(new
    //        File("./data/set1/testInstance.txt"));
    RDLPInstance dataModel =
        RDLParser.parse(new File("./data/set1/instance2.txt"), 1.0);

    ConstructiveHeuristic heuristic = new ConstructiveHeuristic(dataModel);

    double runTime = TimeUtils.getCpuTime();
    System.out.println("Starting constructive heuristic for " + dataModel.name);
    heuristic.run();
    runTime = (TimeUtils.getCpuTime() - runTime) * NANOSECONDS_TO_MILLISECONDS;
    RDLPSolution solution = heuristic.getSolution();
    if (solution != null) {
      solution.verify(dataModel.routingGraph);
      System.out.println(solution);
      System.out.println("Runtime: " + runTime);
      System.out.println("Objective: " + solution.objective);
    } else {
      System.out.println("No feasible solution found!");
    }
  }
}
