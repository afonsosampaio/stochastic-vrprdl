package rdlp.test;

import rdlp.io.RDLParser;
import rdlp.model.Customer;
import rdlp.model.RDLPInstance;

import java.io.File;

/**
 * Test the implementation of the RDLParser
 */
public class RDLParserTest {
  public static void main(String[] args) {
    RDLPInstance inst =
        RDLParser.parse(new File("./data/set1/instance1.txt"), 1.0);

    System.out.println("NODES: " + inst.routingGraph.vertexSet());
    System.out.println("EDGES: " + inst.routingGraph.edgeSet());
  }
}
