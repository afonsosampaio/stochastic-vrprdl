package rdlp.test;

import ilog.concert.IloException;
import rdlp.algorithms.exact.mip.MIP;
import rdlp.algorithms.heuristic.constructive.ConstructiveHeuristic;
import rdlp.io.RDLParser;
import rdlp.model.RDLPInstance;
import rdlp.model.RDLPSolution;
import rdlp.util.TimeUtils;

import java.io.File;

import static rdlp.util.TimeUtils.NANOSECONDS_TO_MILLISECONDS;

/**
 * Created by jkinable on 7/2/15.
 */
public class MIPTest {
  public static void main(String[] args) throws IloException {
    RDLPInstance dataModel =
        RDLParser.parse(new File("./data/example/testInstance.txt"), 1.0);
    //        RDLPInstance dataModel= RDLParser.parse(new
    //        File("./data/set1/instance2.txt"));

    MIP mip = new MIP(dataModel);

    // OPTIONAL: warmstart MIP model
    ConstructiveHeuristic heuristic = new ConstructiveHeuristic(dataModel);
    heuristic.run();
    RDLPSolution initSolution = heuristic.getSolution();
    if (initSolution != null) {
      initSolution.verify(dataModel.routingGraph);
      mip.warmstart(initSolution);
    }

    double runTime = TimeUtils.getCpuTime();
    System.out.println("Starting branch and bound for " + dataModel.name);
    mip.solve();
    runTime = (TimeUtils.getCpuTime() - runTime) * NANOSECONDS_TO_MILLISECONDS;
    RDLPSolution solution = null;
    if (mip.isFeasible()) {
      solution = mip.getSolution();
      solution.verify(dataModel.routingGraph);
      System.out.println(solution);
      System.out.println("Runtime: " + runTime);
      System.out.println("Is optimal: " + mip.isOptimal());
      System.out.println("Objective: " + mip.getObjective());
      System.out.println("Bound: " + mip.getLowerBound());
    } else {
      System.out.println("MIP infeasible!");
    }
    mip.close();
  }
}
