package rdlp.test;

import rdlp.model.RDLPSolution;
import rdlp.model.RDLPInstance;
import rdlp.model.Location;

import rdlp.io.RDLParser;

import org.jgrapht.Graph;
import org.jgrapht.graph.AsWeightedGraph;
import org.jgrapht.graph.DefaultWeightedEdge;

import rdlp.algorithms.stochastic.*;

import ilog.concert.IloException;
import java.io.File;

import java.util.*;

import java.text.DecimalFormat;

public class GammaSamplingTest {
  public static void main(String[] args) throws IloException {
    File file = new File(args[0]);
    RDLPInstance dataModel = RDLParser.parse(file, 1.0);

    ScenarioGenerator scenarioGenerator = new ScenarioGenerator(dataModel);

    Graph<Location, DefaultWeightedEdge> scenario;

    DefaultWeightedEdge edge = null;
    int edge_idx = 2019;
    int aux = 0;
    for (DefaultWeightedEdge e : dataModel.routingGraph.edgeSet())
      if (aux == edge_idx) {
        edge = e;
        break;
      } else
        aux++;

    int samples = 10000;

    double[] sampled_1 = new double[samples];
    // double[] sampled_2 = new double[samples];

    for (int i = 0; i < samples; ++i) {
      scenario = scenarioGenerator.getScenario();  // G(a,b)*t_ij
      sampled_1[i] = scenario.getEdgeWeight(edge);
      // scenario = scenarioGenerator.getScenarioByArc();  //G(a,b*t_ij)
      // sampled_2[i] = scenario.getEdgeWeight(edge);
      // System.out.println(sampled_new[i]+"---"+sampled_old[i]);
      // System.out.println(scenario.getEdgeWeight(edge));
    }

    double avg_sampled_1 =
        Arrays.stream(sampled_1).average().orElse(Double.NaN);
    // double avg_sampled_2 =
    // Arrays.stream(sampled_2).average().orElse(Double.NaN);
    double var_sampled_1 = 0.0;
    // double var_sampled_2 = 0.0;
    for (int i = 0; i < samples; ++i) {
      var_sampled_1 +=
          ((sampled_1[i] - avg_sampled_1) * (sampled_1[i] - avg_sampled_1)) /
          (samples - 1);
      // var_sampled_2 +=
      // ((sampled_2[i]-avg_sampled_2)*(sampled_2[i]-avg_sampled_2))/(samples-1);
    }

    DecimalFormat df = new DecimalFormat();
    df.setMaximumFractionDigits(2);

    System.out.println("Original value: " +
                       dataModel.routingGraph.getEdgeWeight(edge));
    System.out.println("Avg sampling " + df.format(avg_sampled_1));
    System.out.println("Var sampling " + df.format(Math.sqrt(var_sampled_1)));
    // System.out.println("G(a,b)*t_ij sampling:
    // "+df.format(avg_sampled_1)+"/"+df.format(Math.sqrt(var_sampled_1)));
    // System.out.println("G(a,b*t_ij) sampling:
    // "+df.format(avg_sampled_2)+"/"+df.format(Math.sqrt(var_sampled_2)));
  }
}
