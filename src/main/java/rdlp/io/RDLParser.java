package rdlp.io;

import javafx.geometry.Point2D;
import org.jgrapht.Graph;
import org.jgrapht.Graphs;
import org.jgrapht.alg.util.Pair;
import org.jgrapht.graph.DefaultDirectedWeightedGraph;
import org.jgrapht.graph.DefaultWeightedEdge;
import rdlp.model.Customer;
import rdlp.model.Location;
import rdlp.model.RDLPInstance;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Parser for the RDL instances
 */
public class RDLParser {
  /**s
  @param file VRPRDL instance file
  @param cv2 Coefficient of variance for sampling scnearios
  */
  public static RDLPInstance parse(File file, double cv2) {
    try (BufferedReader br = new BufferedReader(new FileReader(file))) {
      String name = file.getName();

      // Set localization parameters, (using '.' as decimal separator)
      Locale.setDefault(new Locale("en", "US"));

      // Read general parameters
      String line = br.readLine().trim();
      if (!line.equals("General parameters"))
        throw new RuntimeException("Expected: 'General parameters'");
      br.readLine();                // skip blank line
      line = br.readLine().trim();  // read parameters

      int[] params =
          Arrays.stream(line.split(" ")).mapToInt(Integer::parseInt).toArray();
      int NR_CUST = params[0];
      int NR_LOC = params[1];
      int TIME_HORIZON = params[2];  // should be the same as the right hand
                                     // side of the time window of the depots
      int VEHIC_CAP = params[3];

      // Read customer schedules
      br.readLine();  // skip blank line
      br.readLine();  // skip blank line
      line = br.readLine().trim();
      if (!line.equals("Customer schedules"))
        throw new RuntimeException("Expected: 'Customer schedules'");
      br.readLine();  // skip blank line

      Map<Integer, Integer> customerDemands = new HashMap<>();
      Map<Integer, List<Integer>> locationsIDsPerCustomer = new HashMap<>();
      Map<Integer, Pair<Integer, Integer>> locationTimeWindows =
          new HashMap<>();

      for (int i = 0; i < NR_CUST + 2; i++) {  // Parse all customers + 2 depots
        line = br.readLine().trim().replaceAll(
            "\\D+",
            " ");  // The last bit gets rid of all non-digits in the string
        Scanner scanner = new Scanner(line);
        int custID = scanner.nextInt();
        customerDemands.put(custID, scanner.nextInt());

        List<Integer> locationsForCustomer = new ArrayList<>();

        // Read customer locations and corresponding time windows
        while (scanner.hasNextInt()) {
          int locID = scanner.nextInt();
          int start = scanner.nextInt();
          int end = scanner.nextInt();
          locationsForCustomer.add(locID);
          locationTimeWindows.put(locID, new Pair<>(start, end));
        }
        locationsIDsPerCustomer.put(custID, locationsForCustomer);
      }

      // Read location coordinates
      br.readLine();  // skip blank line
      br.readLine();  // skip blank line
      line = br.readLine().trim();
      if (!line.equals("Location coordinates"))
        throw new RuntimeException("Expected: 'Location coordinates'");
      br.readLine();  // skip blank line
      List<Point2D> locationCoordinates = new ArrayList<>();
      for (int i = 0; i < NR_LOC; i++) {  // Parse all locations
        line = br.readLine().trim();
        Scanner scanner = new Scanner(line);
        int locID = scanner.nextInt();
        float xCor = scanner.nextFloat();
        float yCor = scanner.nextFloat();
        Point2D coordinate = new Point2D(xCor, yCor);
        locationCoordinates.add(coordinate);
      }

      // Generate the locations
      List<Location> locations = new ArrayList<>(NR_LOC);
      for (int i = 0; i < NR_LOC; i++)
        locations.add(new Location(i, locationCoordinates.get(i),
                                   locationTimeWindows.get(i).getFirst(),
                                   locationTimeWindows.get(i).getSecond()));

      // Generate the customers. The first and the last customers are special in
      // the sense that they only contain the source and target depot
      List<Customer> customers = new ArrayList<>(NR_CUST + 2);
      for (int i = 0; i < NR_CUST + 2; i++) {
        Set<Location> locationsForCustomer =
            locationsIDsPerCustomer.get(i)
                .stream()
                .map(locations::get)
                .collect(Collectors.toCollection(LinkedHashSet::new));
        customers.add(
            new Customer(i, customerDemands.get(i), locationsForCustomer));
      }
      Location sourceDepot = locations.get(0);
      Location targetDepot = locations.get(NR_LOC - 1);

      // Read distance matrix
      br.readLine();  // skip blank line
      br.readLine();  // skip blank line
      line = br.readLine().trim();
      if (!line.equals("Travel time matrix"))
        throw new RuntimeException("Expected: 'Travel time matrix'");
      br.readLine();  // skip blank line
      // Create routing graph
      Graph<Location, DefaultWeightedEdge> routingGraph =
          new DefaultDirectedWeightedGraph<>(DefaultWeightedEdge.class);
      // Graph<Location, DefaultWeightedEdge> distanceGraph=new
      // DefaultDirectedWeightedGraph<>(DefaultWeightedEdge.class);
      Graphs.addAllVertices(routingGraph, locations);
      // Graphs.addAllVertices(distanceGraph, locations);
      line = br.readLine();
      while (
          line !=
          null) {  // Read distance matrix until the end of the file is reached
        Scanner scanner = new Scanner(line.trim().replaceAll("\\D+", " "));
        int idLoc1 = scanner.nextInt();
        int idLoc2 = scanner.nextInt();
        Location loc1 = locations.get(idLoc1);
        Location loc2 = locations.get(idLoc2);
        int dist = scanner.nextInt();

        //                if(loc1.start+dist > loc2.end)
        //                    throw new RuntimeException("Weird, why's there an
        //                    edge between these 2 locations");

        // Ignore self loops, or edge to the source depot, or edges from the
        // target depot, or edges within a cluster
        if (idLoc1 != idLoc2 && idLoc2 != sourceDepot.ID &&
            idLoc1 != targetDepot.ID)  // && loc1.customer != loc2.customer)
        {
          Graphs.addEdge(routingGraph, loc1, loc2, dist);
          // Graphs.addEdge(distanceGraph, loc1, loc2, dist);
          // Graphs.addEdge(routingGraph, loc1, loc2,
          // loc1.coordinate.distance(loc2.coordinate));
          // System.out.println(loc1+" "+loc2+" "+dist+"
          // "+loc1.coordinate.distance(loc2.coordinate));
          // System.out.println(routingGraph.getEdgeWeight(routingGraph.getEdge(loc1,
          // loc2)));
        }
        line = br.readLine();
      }
      return new RDLPInstance(name, customers, routingGraph, sourceDepot,
                              targetDepot, VEHIC_CAP, cv2);

    } catch (IOException e) {
      e.printStackTrace();
    }
    return null;
  }
}
