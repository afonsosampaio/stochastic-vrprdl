package rdlp.algorithms.heuristic;

import org.jgrapht.Graph;
import org.jgrapht.graph.AsWeightedGraph;
import org.jgrapht.graph.DefaultWeightedEdge;

import rdlp.algorithms.stochastic.RevisitRecourse;
import rdlp.model.Customer;
import rdlp.model.Location;
import rdlp.model.RDLPInstance;

import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Queue;

/**
 * A route implemented as a double Linked List. Removal and insertion of nodes
 * can be performed in O(1) time.
 */
public class Route implements Iterable<Node> {
  private final RDLPInstance dataModel;

  /* Start of the route (cannot be changed)*/
  public final Node routeStart;
  /* End of the route (cannot be changed)*/
  public final Node routeEnd;

  /* Vehicle capacity required to satisfy all deliveries in this route */
  public int capacity;
  /* Cost of this route (sum of setup times) */
  public double cost;
  /* Number of locations visited in this route */
  public int nrLocations = 2;

  public double recourseCost = 0.0;

  public Route(RDLPInstance dataModel) {
    this.dataModel = dataModel;
    routeStart = new Node(dataModel.sourceDepot);
    routeEnd = new Node(dataModel.targetDepot);
    routeStart.next = routeEnd;
    routeEnd.prev = routeStart;
    cost =
        dataModel.getTravelTime(dataModel.sourceDepot, dataModel.targetDepot);
  }

  public void clear() {
    routeStart.next = routeEnd;
    routeEnd.prev = routeStart;
    routeStart.start = dataModel.sourceDepot.start;
    routeEnd.start = dataModel.targetDepot.start;
    routeStart.end = dataModel.sourceDepot.end;
    routeEnd.end = dataModel.targetDepot.end;
    capacity = 0;
    cost =
        dataModel.getTravelTime(dataModel.sourceDepot, dataModel.targetDepot);
  }

  /**
   * Insert node after another node in this route
   * @param insertNode node to be inserted
   * @param afterNode node after which this node is inserted
   */
  public void insertNodeAfter(
      Node insertNode, Node afterNode,
      Graph<Location, DefaultWeightedEdge> routingGraph) {
    //        System.out.println("Insert + "+insertNode.location+" after:
    //        "+afterNode.location);
    Objects.requireNonNull(insertNode);
    Objects.requireNonNull(afterNode);
    assert afterNode != routeEnd;

    Node beforeNode = afterNode.next;
    capacity += insertNode.location.customer.demand;
    // cost-=dataModel.getTravelTime(afterNode.location, beforeNode.location);
    cost -= routingGraph.getEdgeWeight(
        routingGraph.getEdge(afterNode.location, beforeNode.location));
    nrLocations++;

    // change effective windows (assuming feasibility was checked!)
    // double travel1 = dataModel.getTravelTime(afterNode.location,
    // insertNode.location); double travel2 =
    // dataModel.getTravelTime(insertNode.location, beforeNode.location);
    double travel1 = routingGraph.getEdgeWeight(
        routingGraph.getEdge(afterNode.location, insertNode.location));
    double travel2 = routingGraph.getEdgeWeight(
        routingGraph.getEdge(insertNode.location, beforeNode.location));
    insertNode.start =
        Math.max(insertNode.location.start, (int)(afterNode.start + travel1));
    insertNode.end =
        Math.min(insertNode.location.end, (int)(beforeNode.end - travel2));

    afterNode.next = insertNode;
    beforeNode.prev = insertNode;
    insertNode.prev = afterNode;
    insertNode.next = beforeNode;
    cost += routingGraph.getEdgeWeight(
        routingGraph.getEdge(afterNode.location, insertNode.location));
    cost += routingGraph.getEdgeWeight(
        routingGraph.getEdge(insertNode.location, beforeNode.location));
    // cost+=dataModel.getTravelTime(afterNode.location, insertNode.location);
    // cost += dataModel.getTravelTime(insertNode.location,
    // beforeNode.location);
  }

  /**
   * Remove a node from the route
   * @param node node to be removed
   */
  public void remove(Node node,
                     Graph<Location, DefaultWeightedEdge> routingGraph) {
    Objects.requireNonNull(node);
    assert node != routeStart && node != routeEnd && node.prev != null &&
        node.next != null;

    capacity -= node.location.customer.demand;
    cost -= routingGraph.getEdgeWeight(
        routingGraph.getEdge(node.prev.location, node.location));
    cost -= routingGraph.getEdgeWeight(
        routingGraph.getEdge(node.location, node.next.location));
    cost += routingGraph.getEdgeWeight(
        routingGraph.getEdge(node.prev.location, node.next.location));
    // cost -= dataModel.getTravelTime(node.prev.location, node.location);
    // cost -= dataModel.getTravelTime(node.location, node.next.location);
    // cost += dataModel.getTravelTime(node.prev.location, node.next.location);
    nrLocations--;

    node.prev.next = node.next;
    node.next.prev = node.prev;
  }

  /**
   * Determine whether the route is feasible, i.e. meets both capacity and time
   * window requirements. This method runs in time linear to the size of the
   * route.
   * @return true if the route is feasible, false otherwise
   */
  public boolean isFeasible(Graph<Location, DefaultWeightedEdge> routingGraph) {
    // Test whether the vehicles have enough capacity to execute this route.
    if (capacity > dataModel.VEHIC_CAP) {
      // System.out.println("ERROR:CAPACITY");
      return false;
    }
    // Test whether all time windows can be met. Simply test whether a feasible
    // schedule exists for the route
    int time = 0;
    Node u = routeStart;
    while (u.next != null) {
      Node v = u.next;
      DefaultWeightedEdge e =
          routingGraph.getEdge(u.location, v.location);  // dataModel.
      time += routingGraph.getEdgeWeight(e);             // dataModel
      time = Math.max(time, v.location.start);

      if (time > v.location.end + 1) {
        // System.out.println("ERROR:TIME WINDOWS");
        return false;  // Time window exceeded
      }
      u = v;
    }
    return true;
  }

  public boolean isFeasibleVERBOSE(
      Graph<Location, DefaultWeightedEdge> routingGraph) {
    // Test whether the vehicles have enough capacity to execute this route.
    if (capacity > dataModel.VEHIC_CAP) {
      System.out.println("ERROR:CAPACITY:" + capacity);
      return false;
    }
    // Test whether all time windows can be met. Simply test whether a feasible
    // schedule exists for the route
    int time = 0;
    Node u = routeStart;
    while (u.next != null) {
      Node v = u.next;
      DefaultWeightedEdge e =
          routingGraph.getEdge(u.location, v.location);  // dataModel.
      time += routingGraph.getEdgeWeight(e);             // dataModel
      time = Math.max(time, v.location.start);

      System.out.println(v.location+"@"+time+" "+u.location +","+v.location+"&"+routingGraph.getEdgeWeight(e));

      if (time > v.location.end + 1) {
        System.out.println("ERROR:TIME WINDOWS @"+v.location+" time:"+time+" latest:"+v.location.end);
        return false;  // Time window exceeded
      }
      u = v;
    }
    return true;
  }

  public void resetServiceTimes() {
    for (Node node : this) {
      node.start = node.location.start;
      node.end = node.location.end;
    }
  }

  public void updateEarliest(
      Node from, Graph<Location, DefaultWeightedEdge> routingGraph) {
    int earliest = from.start;
    int update;
    Node prev = from;
    double travel;
    boolean flag = false;

    for (Node node : this) {
      if (!flag && node == from) {
        flag = true;
        continue;
      }
      if (!flag) continue;

      travel = routingGraph.getEdgeWeight(
          routingGraph.getEdge(prev.location, node.location));
      // travel = dataModel.getTravelTime(prev.location, node.location);
      // System.out.println(node+","+travel+","+earliest+","+node.start);
      update = Math.max(node.start, (int)(earliest + travel));
      // if(update != node.start)
      {
        earliest = update;
        node.start = earliest;
      }
      prev = node;
    }
  }

  public void updateLatest(Node from,
                           Graph<Location, DefaultWeightedEdge> routingGraph) {
    int latest = from.end;
    int update;
    Node prev;
    Node next = from;
    double travel;

    Iterator<Node> iter = this.reverse_iterator();
    boolean stop = false;

    while (iter.hasNext() && !stop) {
      if (iter.next() == from) stop = true;
    }

    // start updating from here
    while (iter.hasNext()) {
      prev = iter.next();
      travel = routingGraph.getEdgeWeight(
          routingGraph.getEdge(prev.location, next.location));
      // travel = dataModel.getTravelTime(prev.location, next.location);
      update = Math.min(prev.end, (int)(latest - travel));
      // if(update != prev.end)
      {
        latest = update;
        prev.end = latest;
      }
      next = prev;
    }
  }

  public void recomputeCost() {
    Node prev = routeStart;
    cost = 0.0;
    for (Node node : this) {
      if (node == routeStart) continue;
      cost += dataModel.routingGraph.getEdgeWeight(
          dataModel.routingGraph.getEdge(prev.location, node.location));
      prev = node;
    }
  }

  /**
   * Returns the size of this route, expressed in the number of locations.
   * @return number of locations in the route.
   */
  public int size() { return nrLocations; }

  public Iterator<Node> reverse_iterator() {
    return new Iterator<Node>() {
      Node node = routeEnd;

      public boolean hasNext() { return node != null; }

      public Node next() {
        Node result = node;
        node = node.prev;
        return result;
      }
    };
  }

  @Override
  public Iterator<Node> iterator() {
    return new Iterator<Node>() {
      Node node = routeStart;

      @Override
      public boolean hasNext() {
        return node != null;
      }

      @Override
      public Node next() {
        Node result = node;
        node = node.next;
        return result;
      }
    };
  }

  @Override
  public String toString() {
    String s = "";
    for (Node node : this)
      s += node.toString() + "[" + node.start + "," + node.end + "], ";
    s += (" cap:" + capacity + " ");
    return s.substring(0, s.length() - 1);
  }

  public Route deepCopy() {
    Route new_route = new Route(dataModel);
    new_route.routeStart.start = this.routeStart.start;
    new_route.routeStart.end = this.routeStart.end;
    new_route.routeEnd.start = this.routeEnd.start;
    new_route.routeEnd.end = this.routeEnd.end;
    new_route.capacity = this.capacity;

    Node curr = new_route.routeStart;
    Node nxt = null;
    for (Node node : this) {
      if (node == this.routeStart || node == this.routeEnd)
        continue;
      nxt = new Node(node.location);
      nxt.start = node.start;
      nxt.end = node.end;
      nxt.prev = curr;
      curr.next = nxt;
      curr = nxt;
    }
    if (nxt == null)
      return new_route;

    new_route.routeEnd.prev = nxt;
    nxt.next = new_route.routeEnd;
    return new_route;
  }

  public boolean isNewLocationAfter(Location missedLoc, Node after, int time,
                                    Map<Location, Node> locationToNodeMap,
                                    Graph<Location, DefaultWeightedEdge> scenario,
                                    Queue<SingleInsertion> insertionCandidates) {

    Customer cust = missedLoc.customer;
    Node node;
    double travel1, travel2;
    int earliest, latest;
    int after_earliest, before_latest;
    double delta, insertionCost;
    boolean visitedRoutePart;

    insertionCandidates.clear();

    for (Location loc : cust.locations) {
      node = locationToNodeMap.get(loc);

      if ((node.location.start <= 0 && node.location.end <= 0)
        || node.location.end <= time || node.location == missedLoc)
        continue; // location is not available

      visitedRoutePart = false;
      for (Node current : this) {
        if (current == this.routeEnd)
          break;
        if (current == after)
          visitedRoutePart = true;
        if (current.end <= time || !visitedRoutePart)
          continue;

//        System.out.println(after.location + ", " + node.location);
//        System.out.println(node.location + ", " +  after.next.location);
//        System.out.println(after.location + ", " +  after.next.location);
        if (current == after) {
          travel1 = (int) scenario.getEdgeWeight(
            scenario.getEdge(after.location, node.location));
          travel2 = (int) dataModel.routingGraphExp.getEdgeWeight(
            dataModel.routingGraphExp.getEdge(node.location, after.next.location));
          insertionCost = travel1 + travel2 - (int)scenario.getEdgeWeight(
            scenario.getEdge(after.location, after.next.location));
        } else {
          travel1 = (int) dataModel.routingGraphExp.getEdgeWeight(
            dataModel.routingGraphExp.getEdge(current.location, node.location));
          travel2 = (int) dataModel.routingGraphExp.getEdgeWeight(
            dataModel.routingGraphExp.getEdge(node.location, current.next.location));
          insertionCost = travel1 + travel2 - (int)dataModel.routingGraphExp.getEdgeWeight(
           dataModel.routingGraphExp.getEdge(current.location, current.next.location));
        }

        after_earliest = current.start;
        before_latest = current.next.end;
        earliest =
          Math.max(node.location.start, (int)(after_earliest + travel1));
        latest =
          Math.min(node.location.end, (int)(before_latest - travel2));

        if (earliest <= latest) {// feasible?
          insertionCandidates.add(
            new Route.SingleInsertion(node, current, this, insertionCost));
        }
      }
    }

    if (insertionCandidates.peek() != null)  // there is a feasible insertion!
    {
      Node nxt = insertionCandidates.peek().node;
      Node curr = insertionCandidates.peek().after;

      if (curr.start <= time)
        this.insertNodeAfter(nxt, curr, scenario);
      else
        this.insertNodeAfter(nxt, curr, dataModel.routingGraphExp);
      this.updateEarliest(nxt, dataModel.routingGraph);
      this.updateLatest(nxt, dataModel.routingGraph);
      return true;
    }

    return false;
  }

  public class SingleInsertion implements Comparable<Route.SingleInsertion> {
    final double cost;
    final Node node;
    final Node after;
    final Route ins_route;

    public SingleInsertion(Node nod, Node aft, Route rot, double cost) {
      this.node = nod;
      this.after = aft;
      this.ins_route = rot;
      this.cost = cost;
    }

    void execute(Graph<Location, DefaultWeightedEdge> scenario) {
      ins_route.insertNodeAfter(node, after, scenario);
    }

    public String toString() {
      String s = "";
      s += "Insert " + this.node + " after " + this.after + " at cost " +
        this.cost + "\n";
      return s;
    }

    @Override
    public int compareTo(Route.SingleInsertion d) {
      return Double.compare(d.cost, this.cost);
    }
  }
}
