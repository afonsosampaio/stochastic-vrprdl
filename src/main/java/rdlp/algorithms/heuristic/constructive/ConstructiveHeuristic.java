package rdlp.algorithms.heuristic.constructive;

import rdlp.algorithms.heuristic.*;

import rdlp.model.Customer;
import rdlp.model.Location;
import rdlp.model.RDLPInstance;
import rdlp.model.RDLPSolution;
import rdlp.util.Constants;

import java.util.*;

/**
 * Implementation of the Constructive Heuristic from
 *  Reyes, D. Savelsbergh, M. Toriello, A. Vehicle Routing with Roaming Delivery
 * Locations. Transportation Research part C.
 */
public class ConstructiveHeuristic {
  private final RDLPInstance dataModel;
  private SplittableRandom rand = new SplittableRandom(0);

  /* Tracks the best solution */
  private RDLPSolution bestSolution = null;
  private HeuristicSolution bestHeuristicSolution = null;

  private Map<Location, Node> locationToNodeMap = new HashMap<>();
  /* Queue which tracks candidate moves in reverse order. The head of the queue
   * is the lowest quality move. */
  private Queue<Move> candidateMoves =
      new PriorityQueue<>(Constants.K + 1, Collections.reverseOrder());

  /**
   * Constructs a new constructive heuristic
   * @param dataModel data model
   */
  public ConstructiveHeuristic(RDLPInstance dataModel) {
    this.dataModel = dataModel;
  }

  /**
   * Setup data structures
   */
  private void init() {
    for (Location loc : dataModel.routingGraph.vertexSet())
      locationToNodeMap.put(loc, new Node(loc));
  }

  public void run() {
    this.init();
    double aux, best;
    best = 999999;
    // Construct N initial solutions and return the best one
    for (int iteration = 0; iteration < Constants.N; iteration++) {
      //            System.out.println("Starting iteration "+iteration);
      HeuristicSolution sol = getNewConstructiveSolution();
      if (sol == null)  // Failed to construct new solution. Retry.
        continue;
      RDLPSolution rdlpSolution = this.convertSolution(sol);

      aux = 0.0;
      for (Customer c : sol.getNotServicedCustomers())
        aux += Constants.UNVISITED_MULTIPLIER * c.penalty;

      assert rdlpSolution.objective == sol.getObjective();
      if (bestSolution == null ||
          bestSolution.objective + best >
              rdlpSolution.objective + aux)  // Found new best solution
      {
        try {
          bestHeuristicSolution = (HeuristicSolution)sol.clone();
        } catch (CloneNotSupportedException e) {
        }
        bestSolution = rdlpSolution;  // TODO: why this works?!?!?
        best = aux;
      }
    }
  }

  /**
   * Compute an initial solution
   * @return an initial solution
   */
  private HeuristicSolution getNewConstructiveSolution() {
    HeuristicSolution sol = new HeuristicSolution(dataModel);
    RDLPSolution rdlpSolution = this.convertSolution(sol);
    sol.addRoute();  // Add 1 empty route to start
    candidateMoves.clear();

    Set<Customer> remainingCustomers = new LinkedHashSet<>(dataModel.customers);
    remainingCustomers.remove(dataModel.sourceDepot.customer);
    remainingCustomers.remove(dataModel.targetDepot.customer);

    while (!remainingCustomers.isEmpty()) {
      //            System.out.println("customers remaining:
      //            "+remainingCustomers);
      for (Customer customer : remainingCustomers)
        this.generateMoves(sol, customer);
      if (candidateMoves.isEmpty())  // Failed to find a move: terminate
        return sol;
      // Select a random move from our candidate list, and execute the move
      Move move = candidateMoves.peek();
      for (int count = rand.nextInt(candidateMoves.size()); count >= 0; count--)
        move = candidateMoves.poll();
      move.execute();

      candidateMoves.clear();
      remainingCustomers.remove(move.getCustomer());

      //            System.out.println("Partial sol after move:\n"+sol);
    }
    return sol;
  }

  public RDLPSolution getBasicSolution() {
    HeuristicSolution sol = new HeuristicSolution(dataModel);
    Set<Customer> remainingCustomers = new LinkedHashSet<>(dataModel.customers);
    Route aux;

    for (Customer customer : remainingCustomers) {
      candidateMoves.clear();
      sol.addRoute();
      aux = sol.getLastRoute();

      for (Location loc : customer.locations) {
        if (loc == dataModel.sourceDepot || loc == dataModel.targetDepot) break;
        Node n1 = locationToNodeMap.get(loc);
        for (Node n2 : aux) {
          if (n2 == aux.routeEnd) break;
          // Attempt to insert node n1 after n2.
          Move move = new Move(sol, n1, aux, n2);
          if (move.isFeasible()) candidateMoves.add(move);
          // Check that we don't have more than K candidate moves. If we have
          // more, just delete the worst candidate.
          if (candidateMoves.size() > Constants.K) candidateMoves.poll();
        }
      }
      if (candidateMoves.size() > 0) {
        Move move = candidateMoves.peek();
        move.execute();
        // System.out.println(aux);
      }
    }
    return this.convertSolution(sol);
  }

  /**
   * Attempts to insert the customer in any of the (partial) routes
   * @param sol partial solution
   * @param customer customer for which moves should be generated
   */
  private void generateMoves(HeuristicSolution sol, Customer customer) {
    // Consider every location, and every possible insert location
    for (Location loc : customer.locations) {
      Node n1 = locationToNodeMap.get(loc);
      for (Route route : sol.routes) {
        for (Node n2 : route) {
          if (n2 == route.routeEnd) break;
          // Attempt to insert node n1 after n2.
          Move move = new Move(sol, n1, route, n2);
          if (move.isFeasible()) candidateMoves.add(move);
          // Check that we don't have more than K candidate moves. If we have
          // more, just delete the worst candidate.
          if (candidateMoves.size() > Constants.K) candidateMoves.poll();
        }
      }
    }
  }

  /**
   * Converts a @{@link HeuristicSolution} to a @{@link RDLPSolution}
   * @return an rdlp solution
   */
  private RDLPSolution convertSolution(HeuristicSolution constructiveSolution) {
    if (constructiveSolution == null) return null;

    // else, construct RDLPSolution
    List<List<Location>> routes = new ArrayList<>();
    for (Route route : constructiveSolution.routes) {
      if (route.size() ==
          2)  // empty route containing just the source and target depot.
        continue;
      List<Location> locationSequence = new ArrayList<>();
      for (Node node : route) locationSequence.add(node.location);
      routes.add(locationSequence);
    }
    return new RDLPSolution(dataModel, routes,
                            (int)constructiveSolution.getObjective());
  }

  /**
   * Returns the best solution found, or null if no solution was found.
   * @return the best solution, or null if no solution was found.
   */
  public RDLPSolution getSolution() { return bestSolution; }

  public HeuristicSolution _getSolution() { return bestHeuristicSolution; }

  /**
   * Implementation of an insert move
   */
  private class Move implements Comparable<Move> {
    /* Partial solution */
    private final HeuristicSolution sol;
    /* Cost of the move (lower is beter) */
    private final double cost;
    /* Node to be inserted */
    private final Node node;
    /* Route in which the node gets inserted */
    private final Route insertRoute;
    /* Existing node in the route after which node is inserted */
    private final Node insertAfter;

    private Move(HeuristicSolution sol, Node node, Route insertRoute,
                 Node insertAfter) {
      this.sol = sol;
      this.node = node;
      this.insertRoute = insertRoute;
      this.insertAfter = insertAfter;
      this.cost =
          dataModel.getTravelTime(insertAfter.location, node.location) +
          dataModel.getTravelTime(node.location, insertAfter.next.location) -
          dataModel.getTravelTime(insertAfter.location,
                                  insertAfter.next.location);
    }

    /**
     * Checks whether the move is feasible
     * @return true if the move is feasible
     */
    protected boolean isFeasible() {
      // Just perform the move, check whether the move is feasible, and revert
      insertRoute.insertNodeAfter(node, insertAfter, dataModel.routingGraph);
      boolean feasible = insertRoute.isFeasible(dataModel.routingGraph);
      insertRoute.remove(node, dataModel.routingGraph);
      return feasible;
    }

    /**
     * Performs the move
     */
    void execute() {
      if (insertRoute.size() == 2)  // Inserting in a new route.
        sol.addRoute();  // Add a new empty route for future iterations
      insertRoute.insertNodeAfter(node, insertAfter, dataModel.routingGraph);
    }

    /**
     * Returns the customer affected by this move
     * @return customer
     */
    public Customer getCustomer() { return node.location.customer; }

    @Override
    public int compareTo(Move o) {
      return Double.compare(this.cost, o.cost);
    }
  }
}
