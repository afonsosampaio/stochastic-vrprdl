package rdlp.algorithms.heuristic;

import rdlp.model.RDLPInstance;
import rdlp.model.RDLPSolution;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import rdlp.model.Location;
import rdlp.model.Customer;
import rdlp.util.Constants;

import org.jgrapht.Graph;
import org.jgrapht.graph.DefaultWeightedEdge;

import rdlp.algorithms.stochastic.*;

import javafx.util.Pair;

public class HeuristicSolution implements Cloneable {
  private final RDLPInstance dataModel;
  private double stStageObj;
  private double ndStageObj;
  public List<Route> routes = new ArrayList<>();

  // stochastic
  private ScenarioGenerator scenarioGenerator;
  private List<Graph<Location, DefaultWeightedEdge> > scenarios;
  private int numSamples;
  // private ScenarioGenerator scenarioGenerator;

  public HeuristicSolution(RDLPInstance dataModel) {
    this.dataModel = dataModel;
    this.stStageObj = Double.POSITIVE_INFINITY;
    this.ndStageObj = 0;
    this.numSamples = 0;
    this.scenarios = null;
    this.scenarioGenerator = new ScenarioGenerator(dataModel);
  }

  public HeuristicSolution(RDLPInstance dataModel,
                           List<Graph<Location, DefaultWeightedEdge> > sce) {
    this.dataModel = dataModel;
    this.stStageObj = Double.POSITIVE_INFINITY;
    this.ndStageObj = 0;
    this.scenarios = sce;
    this.numSamples = sce.size();
  }

  public HeuristicSolution(RDLPSolution sol, RDLPInstance dataModel,
                           List<Graph<Location, DefaultWeightedEdge> > sce) {
    this.dataModel = dataModel;
    this.stStageObj = Double.POSITIVE_INFINITY;
    this.ndStageObj = 0;
    this.scenarios = sce;
    this.numSamples = sce.size();
    Route new_route;
    Node curr;
    Node nxt;

    for (List<Location> route : sol.routes) {
      this.addRoute();
      new_route = this.getLastRoute();
      curr = new_route.routeStart;
      for (Location location : route.subList(1, route.size() - 1)) {
        nxt = new Node(location);
        new_route.insertNodeAfter(nxt, curr, dataModel.routingGraph);
        curr = nxt;
      }
      new_route.updateEarliest(new_route.routeStart, dataModel.routingGraph);
      new_route.updateLatest(new_route.routeEnd, dataModel.routingGraph);
    }

    this.stStageObj = routes.stream().mapToDouble(r -> r.cost).sum();
  }

  public void addRoute() { routes.add(new Route(dataModel)); }

  public Route getLastRoute() { return routes.get(routes.size() - 1); }

  public double getObjective() {
    this.stStageObj = routes.stream().mapToDouble(r -> r.cost).sum();
    if (this.getNumRoutes() > this.dataModel.MAX_ROUTES)
      this.stStageObj += 100000;
    // for(Customer c: this.getNotServicedCustomers())
    //   this.stStageObj += Constants.UNVISITED_MULTIPLIER*c.penalty;
    return this.stStageObj + this.ndStageObj;
  }

  public double getObjective(Recourse rec) {
    this.stStageObj = routes.stream().mapToDouble(r -> r.cost).sum();
    if (this.getNumRoutes() > this.dataModel.MAX_ROUTES)
      this.stStageObj += 100000;
    double missed = 0;
    double total = 0;

    for (Route route : this.routes) route.recourseCost = 0;

    this.ndStageObj = 0;
    if (numSamples > 0) {
      for (int i = 0; i < numSamples; ++i) {
        total += rec.evaluateScenario(this, scenarios.get(i));
        missed += rec.missedCustomers;
      }
      this.ndStageObj = (double)(total / numSamples);

      for (Route route : this.routes)
        route.recourseCost = (double)(route.recourseCost / numSamples);
      rec.missedCustomers = (double)(missed / numSamples);
    }

    // for(int i=0; i<numSamples; ++i)
    // {
    //   int counter = 0;
    //   for(DefaultWeightedEdge e : scenarios.get(i).edgeSet())
    //   {
    //     System.out.println(e+"="+scenarios.get(i).getEdgeWeight(e));
    //     if(counter++>10)
    //     break;
    //   }
    //   System.out.println("===============================");
    //   System.exit(1);
    // }

    return this.stStageObj + this.ndStageObj;
  }

  public List<Customer> getNotServicedCustomers() {
    List<Customer> notServiced = new ArrayList<>();

    for (Customer customer : dataModel.customers) customer.isServiced = false;
    for (Route route : this.routes)
      for (Node node : route) node.location.customer.isServiced = true;

    for (Customer customer : dataModel.customers)
      if (!customer.isServiced) notServiced.add(customer);

    return notServiced;
  }

  public double getAverageSlackTW() {
    double avg = 0.0;
    for (Route route : this.routes)
      for (Node node : route)
        if (node != route.routeStart && node != route.routeEnd)
          avg += node.end - node.start;

    return avg / (dataModel.NR_CUST - 2);
  }

  public int getNumRoutes() {
    int ret = 0;
    for (Route route : this.routes)
      if (route.size() > 2) ret++;
    return ret;
  }

  public double getRoutingCost() { return this.stStageObj; }

  public double getRecourseExpCost() { return this.ndStageObj; }

  public String toString() {
    String s = "";
    for (int i = 0; i < routes.size(); i++)
      if (routes.get(i).size() > 2)
        s += "Route: " + i + ": " + routes.get(i).toString() + "\n";
    return s;
  }

  public void setScenarios(int N) {
    List<Graph<Location, DefaultWeightedEdge> > scenariosEval =
        new ArrayList<Graph<Location, DefaultWeightedEdge> >();
    for (int i = 0; i < N; ++i)
      scenariosEval.add(scenarioGenerator.getScenario());

    this.scenarios = scenariosEval;
    this.numSamples = scenariosEval.size();
  }

  public void dump(Graph<Location, DefaultWeightedEdge> sce) {
    int aux1, aux2;
    for (int i = 0; i < routes.size(); i++) {
      System.out.println(routes.get(i).toString() + ": " + routes.get(i).cost);
      for (Node node : routes.get(i)) {
        if (node == routes.get(i).routeStart || node == routes.get(i).routeEnd)
          continue;
        aux1 = (int)sce.getEdgeWeight(
            sce.getEdge(dataModel.sourceDepot, node.location));
        aux2 = (int)sce.getEdgeWeight(
            sce.getEdge(node.location, dataModel.targetDepot));
        System.out.println(aux1 + " " + aux2);
        aux1 = (int)dataModel.routingGraph.getEdgeWeight(
            dataModel.routingGraph.getEdge(dataModel.sourceDepot,
                                           node.location));
        aux2 = (int)dataModel.routingGraph.getEdgeWeight(
            dataModel.routingGraph.getEdge(node.location,
                                           dataModel.targetDepot));
        System.out.println(aux1 + " " + aux2);
      }
    }
  }

  public Object clone() throws CloneNotSupportedException {
    HeuristicSolution sol = (HeuristicSolution)super.clone();
    sol.routes = new ArrayList<>();
    sol.scenarios = this.scenarios;
    sol.numSamples = this.numSamples;

    // copy the routes
    Node current_node, new_node;
    for (Route route : this.routes) {
      sol.addRoute();
      current_node = sol.getLastRoute().routeStart;
      for (Node node : route) {
        if (node == route.routeStart || node == route.routeEnd) continue;
        new_node = new Node(node.location);
        sol.getLastRoute().insertNodeAfter(new_node, current_node,
                                           dataModel.routingGraph);
        current_node = new_node;
      }
      // set earliest and latest times
      sol.getLastRoute().routeStart.start = dataModel.sourceDepot.start;
      sol.getLastRoute().routeEnd.end = dataModel.targetDepot.end;
      sol.getLastRoute().updateEarliest(sol.getLastRoute().routeStart,
                                        dataModel.routingGraph);
      sol.getLastRoute().updateLatest(sol.getLastRoute().routeEnd,
                                      dataModel.routingGraph);
    }

    return sol;
  }

  public Pair<Double, Double> averageAvailableTime() {
    double totalNotAvailScen = 0;
    double totalDet = 0;
    double totalStch = 0;
    double totalScen = 0;
    double availableDet = 0;
    double availableStch = 0;
    Graph<Location, DefaultWeightedEdge> scenario;
    int notAvailable, roamCustomers;
    double min_avail, max_avail;

    roamCustomers = 0;
    min_avail = Double.POSITIVE_INFINITY;
    max_avail = 0;
    for (Customer customer : dataModel.customers) {
      availableDet = 0;
      // save original time windows
      if (customer.locations.size() > 1) {
        for (Location loc : customer.locations) {
          loc.start_aux = loc.start;
          loc.end_aux = loc.end;
          availableDet += (loc.end - loc.start);
        }
        if (availableDet < min_avail) min_avail = availableDet;
        if (availableDet > max_avail) max_avail = availableDet;
        totalDet += availableDet;
        roamCustomers++;
      }
    }
    // totalDet = totalDet/roamCustomers;//dataModel.customers.size();
    // System.out.println("Avg Det: "+totalDet);
    // System.out.println("Min avail: "+min_avail);
    // System.out.println("Max avail: "+max_avail);
    // System.out.println("Roaming customers: "+roamCustomers);
    // System.exit(1);

    for (int i = 0; i < numSamples; ++i) {
      scenario = scenarios.get(i);
      totalStch = 0;
      notAvailable = 0;
      for (Customer customer : dataModel.customers) {
        availableStch = 0;
        availableDet = 0;
        // save original time windows
        for (Location loc : customer.locations) {
          loc.start_aux = loc.start;
          loc.end_aux = loc.end;
          // availableDet += (loc.end-loc.start);
        }
        // System.out.println(customer+" "+availableDet);
        // totalDet += availableDet;

        Location prev = null;
        int travelTime, order = 0;

        // new time windows given the revealed scenario
        for (Location loc : customer.locations) {
          if (order > 0) {
            travelTime =
                (int)scenario.getEdgeWeight(scenario.getEdge(prev, loc));
            // loc.start = Math.max(loc.start, prev.start + travelTime);
            loc.start = Math.max(loc.start, prev.end + travelTime);
            loc.end = Math.max(loc.end, loc.start);
            if (loc.start > loc.end_aux) {
              loc.start = loc.end = 0;  // not available
              notAvailable++;
            }
          }
          availableStch += (loc.end - loc.start);
          prev = loc;
          order++;
        }

        // return original windows
        for (Location loc : customer.locations) {
          loc.start = loc.start_aux;
          loc.end = loc.end_aux;
        }

        totalStch += availableStch;
      }

      totalNotAvailScen += notAvailable;
      totalScen += (totalStch / dataModel.customers.size());
      // System.out.println("Not available: "+notAvailable);
      // System.out.println("Total Not available: "+totalNotAvailScen);
    }
    return new Pair<Double, Double>(totalNotAvailScen / numSamples,
                                    totalScen / numSamples);
  }

  @Override
  /* Overriding finalize method to check which object is garbage collected */
  protected void finalize() throws Throwable {
    ;
    // System.out.println("FREE THE MALLOCS!");
  }
}
