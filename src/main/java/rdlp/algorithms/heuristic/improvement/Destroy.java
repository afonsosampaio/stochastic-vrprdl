package rdlp.algorithms.heuristic.improvement;

import rdlp.algorithms.heuristic.*;

import rdlp.model.RDLPSolution;
import rdlp.model.RDLPInstance;
import rdlp.model.Customer;

import java.util.*;

abstract class Destroy {
  String operator_name = "";
  RDLPInstance dataModel;

  Queue<Deletion> deletionCandidates;
  SplittableRandom rand = new SplittableRandom(0);  // for random stuff...

  // performance
  int executed = 0;  // how many times operator was used
  int improved =
      0;  // how many times the operator has improved upon current solution

  Destroy(String name, RDLPInstance dataModel) {
    this.operator_name = name;
    this.dataModel = dataModel;
  }

  abstract public void execute(HeuristicSolution solution,
                               List<Customer> removed);

  class Deletion implements Comparable<Deletion> {
    private final HeuristicSolution solution;
    final double cost;
    final Node node;
    final Route rem_route;

    public Deletion(HeuristicSolution sol, Node nod, Route rot, boolean rnd) {
      this.solution = sol;
      this.node = nod;
      this.rem_route = rot;
      if (!rnd) {
        this.cost =
            dataModel.getTravelTime(nod.prev.location, node.location) +
            dataModel.getTravelTime(node.location, node.next.location) -
            dataModel.getTravelTime(nod.prev.location, node.next.location);
      } else
        this.cost = 1000 * rand.nextDouble();
    }

    public String toString() {
      String s = "";
      s += "Remove node " + this.node + " at cost " + this.cost + "\n";
      return s;
    }

    @Override
    public int compareTo(Deletion d) {
      return Double.compare(this.cost, d.cost);
    }
  }
}
