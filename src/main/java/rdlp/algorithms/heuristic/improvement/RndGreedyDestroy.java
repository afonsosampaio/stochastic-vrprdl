package rdlp.algorithms.heuristic.improvement;

import rdlp.algorithms.heuristic.*;

import rdlp.model.RDLPSolution;
import rdlp.model.RDLPInstance;
import rdlp.model.Customer;

import java.util.*;

public class RndGreedyDestroy extends Destroy {
  // RDLPInstance dataModel;
  int L;  // number of elements to remove
  int K;  // size of the restricted candidate list
  List<Deletion> queueToList;

  // NOTE: if L=K, this operator act similar as RandomDestroy
  // the difference being that the best L removals are selected in random order
  public RndGreedyDestroy(String name, RDLPInstance dataModel, int num,
                          int rest) {
    super(name, dataModel);
    L = num;
    K = rest;
    // NOTE: all customers are candidates, but only L are removed
    deletionCandidates =
        new PriorityQueue<>(dataModel.NR_CUST, Collections.reverseOrder());
    queueToList = new ArrayList<Deletion>();
  }

  public void execute(HeuristicSolution solution, List<Customer> removed) {
    // System.out.println(operator_name);

    deletionCandidates.clear();
    // queue removals by cost
    for (Route route : solution.routes) {
      for (Node n : route) {
        if (n == route.routeEnd || n == route.routeStart) continue;
        Deletion del = new Deletion(solution, n, route, false);
        deletionCandidates.add(del);
      }
    }

    queueToList.clear();
    while (deletionCandidates.peek() != null)
      queueToList.add(deletionCandidates.poll());

    // for(Deletion d : queueToList)
    //   System.out.println(d.toString());
    // System.out.println("-----------");

    // randomly selects one removal among the best L
    Deletion del;
    for (int i = 0; i < L; ++i) {
      del = queueToList.get(rand.nextInt(L - i));
      queueToList.remove(del);
      removed.add(del.node.location.customer);
      // System.out.println(del.toString());
      // also, remove from the route
      del.rem_route.remove(del.node, dataModel.routingGraph);
    }
  }
}
