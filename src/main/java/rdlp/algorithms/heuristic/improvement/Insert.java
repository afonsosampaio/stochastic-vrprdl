package rdlp.algorithms.heuristic.improvement;

import rdlp.algorithms.heuristic.*;
import rdlp.algorithms.stochastic.*;

import rdlp.model.RDLPSolution;
import rdlp.model.RDLPInstance;
import rdlp.model.Customer;
import rdlp.model.Location;

import org.jgrapht.Graph;
import org.jgrapht.graph.DefaultWeightedEdge;

import java.util.*;

abstract class Insert {
  String operator_name = "";

  RDLPInstance dataModel;

  Queue<Insertion> insertionCandidates;
  List<Insertion> queueToList;
  SplittableRandom rand = new SplittableRandom(0);  // for random stuff...

  Map<Location, Node> locationToNodeMap = new HashMap<>();

  // stochastic elements
  private final Recourse recourseStrategy;
  private final List<Graph<Location, DefaultWeightedEdge> > scenarios;

  // performance
  int executed = 0;  // how many times operator was used
  int improved =
      0;  // how many times the operator has improved upon current solution

  // deterministic
  Insert(String name, RDLPInstance dataModel) {
    this.operator_name = name;
    this.dataModel = dataModel;
    this.recourseStrategy = null;
    this.scenarios = null;

    for (Location loc : dataModel.routingGraph.vertexSet())
      locationToNodeMap.put(loc, new Node(loc));
  }

  // stochastic
  Insert(String name, RDLPInstance dataModel, Recourse rec,
         List<Graph<Location, DefaultWeightedEdge> > sce) {
    this.operator_name = name;
    this.dataModel = dataModel;
    this.recourseStrategy = rec;
    this.scenarios = sce;

    for (Location loc : dataModel.routingGraph.vertexSet())
      locationToNodeMap.put(loc, new Node(loc));
  }

  abstract public void execute(HeuristicSolution solution,
                               List<Customer> toInsert);

  public double evaluateInsertion(Node nod, Node aft, Route rot) {
    double aux1, aux2;

    // first stage (routing)
    aux1 = dataModel.getTravelTime(aft.location, nod.location) +
           dataModel.getTravelTime(nod.location, aft.next.location) -
           dataModel.getTravelTime(aft.location, aft.next.location);

    // second stage
    aux2 = 0;

    if (recourseStrategy == null) return aux1;

    for (int i = 0; i < scenarios.size(); ++i)
      aux2 +=
          recourseStrategy.evaluateScenario(rot, nod, aft, scenarios.get(i), i);
    aux2 = (double)(aux2 / (scenarios.size())) - rot.recourseCost;

    return aux1 + aux2;
  }

  class Insertion implements Comparable<Insertion> {
    private final HeuristicSolution solution;
    final double cost;
    final Node node;
    final Node after;
    final Route ins_route;

    public Insertion(HeuristicSolution sol, Node nod, Node aft, Route rot,
                     boolean rnd) {
      this.solution = sol;
      this.node = nod;
      this.after = aft;
      this.ins_route = rot;
      double pen = 1.0;

      if (rnd) pen = 1000;

      if (recourseStrategy == null)
        this.cost =
            (dataModel.getTravelTime(after.location, node.location) +
             dataModel.getTravelTime(node.location, after.next.location) -
             dataModel.getTravelTime(after.location, after.next.location)) *
            pen;
      else {
        double aux = 0.0;
        for (int i = 0; i < scenarios.size(); ++i)
          aux += recourseStrategy.evaluateScenario(rot, nod, aft,
                                                   scenarios.get(i), i);
        aux = (double)(aux / (scenarios.size())) - rot.recourseCost;

        this.cost =
            aux +
            (dataModel.getTravelTime(after.location, node.location) +
             dataModel.getTravelTime(node.location, after.next.location) -
             dataModel.getTravelTime(after.location, after.next.location)) *
                pen;
      }
    }

    void execute() {
      if (ins_route.size() == 2)  // Inserting in a new route.
      {
        boolean flag = true;
        // check whether there is still another empty route in the solution
        for (Route route : solution.routes)
          if (route.size() == 2 && route != ins_route) {
            flag = false;  // does not need to insert a new empty route
            break;
          }
        if (flag == true)
          solution.addRoute();  // Add a new empty route for future iterations
      }
      ins_route.insertNodeAfter(node, after, dataModel.routingGraph);
    }

    public String toString() {
      String s = "";
      s += "Insert " + this.node + " after " + this.after + " at cost " +
           this.cost + "\n";
      return s;
    }

    @Override
    public int compareTo(Insertion d) {
      return Double.compare(this.cost, d.cost);
    }
  }
}
