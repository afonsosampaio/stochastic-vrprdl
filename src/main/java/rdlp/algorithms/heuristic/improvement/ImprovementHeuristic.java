package rdlp.algorithms.heuristic.improvement;

import rdlp.algorithms.heuristic.*;

import rdlp.model.Customer;
import rdlp.model.Location;
import rdlp.model.RDLPInstance;
import rdlp.model.RDLPSolution;

import org.jgrapht.Graph;
import org.jgrapht.graph.DefaultWeightedEdge;

import rdlp.algorithms.stochastic.*;

import java.text.DecimalFormat;
import rdlp.util.Constants;

import java.util.stream.Collectors;
import javafx.util.Pair;
import java.util.*;

/**
 * Implementation of the Improvement Heuristic from
 *  Reyes, D. Savelsbergh, M. Toriello, A. Vehicle Routing with Roaming Delivery
 * Locations. Transportation Research part C.
 */
public class ImprovementHeuristic {
  private final RDLPInstance dataModel;
  private final RDLPSolution initialSolution;
  private HeuristicSolution currentSolution;
  private HeuristicSolution backupSolution;
  private HeuristicSolution bestSolution;
  private List<Customer> customerBank;
  private List<Destroy> destroyOperators;
  private List<Insert> insertOperators;
  private RDLPSolution resultSolution = null;

  private int lastSwitch;
  private int lastImprovement;
  private int lastIncumbent;
  private int currentNeighborhood;

  // stochastic
  private final ScenarioGenerator scenarioGenerator;
  private int numSamples;
  private final Recourse recourseStrategy;
  private final List<Graph<Location, DefaultWeightedEdge>> scenarios;
  private List<List>[] timeWindowsScenarios;

  /**
   * @param initial  initial solution e.g. obtained with the constructive
   * heuristic
   * @param dataModel the correspondent RDLP instance
   */
  public ImprovementHeuristic(RDLPSolution initial, RDLPInstance dataModel) {
    this.dataModel = dataModel;
    this.initialSolution = initial;
    this.recourseStrategy = null;  // deterministic case
    this.scenarioGenerator = null;
    this.scenarios = null;
  }

  public ImprovementHeuristic(RDLPSolution initial, RDLPInstance dataModel,
                              Recourse rec) {
    this.dataModel = dataModel;
    this.initialSolution = initial;
    this.recourseStrategy = rec;  // compute second stage costs
    this.scenarioGenerator = new ScenarioGenerator(dataModel);
    this.numSamples = 0;
    this.scenarios = new ArrayList<Graph<Location, DefaultWeightedEdge>>();
  }

  /**
   * @param num number of scenarios to generate during the second stage
   * evaluation
   */
  private void init(int num) {
    numSamples = num;
    if (numSamples > 0)  // construct the set of scenarios to consider in
                         // solving the SAA problem
    {
      for (int i = 0; i < numSamples; ++i)
        scenarios.add(scenarioGenerator.getScenario());
      currentSolution = new HeuristicSolution(dataModel, scenarios);

      timeWindowsScenarios = new List[numSamples];
      int custOrder;
      for (int i = 0; i < numSamples; ++i) {
        custOrder = 0;
        timeWindowsScenarios[i] = new ArrayList<List>();
        // compute available time windows for customers given the travel times
        // in the scenario
        this.availableLocations(scenarios.get(i));
        for (Customer c : dataModel.customers) {
          timeWindowsScenarios[i].add(new ArrayList<Pair<Integer, Integer>>());
          for (Location l : c.locations) {
            timeWindowsScenarios[i].get(custOrder).add(
                new Pair<Integer, Integer>(l.start, l.end));
            // System.out.print(l.start+","+l.end+" ");
          }
          custOrder++;
        }
        this.rollBackCustomersTW();
      }
      this.recourseStrategy.setTimeWindowsScenarios(timeWindowsScenarios,
                                                    scenarios);

      // for(Customer c : dataModel.customers)
      // {
      //   System.out.println("Customer:"+c);
      //   for(Location l : c.locations)
      //     System.out.print(l+"["+l.start+","+l.end+"] ");
      //   System.out.println();
      // }
    } else
      currentSolution = new HeuristicSolution(dataModel);

    Node current_node;
    Node next_node;

    // System.out.println("Initial set of routes:");
    for (List<Location> route : initialSolution.routes) {
      currentSolution.addRoute();
      current_node = currentSolution.getLastRoute().routeStart;
      for (Location location : route.subList(1, route.size() - 1)) {
        next_node = new Node(location);
        // System.out.println(current_node.location+" "+next_node.location);
        currentSolution.getLastRoute().insertNodeAfter(next_node, current_node,
                                                       dataModel.routingGraph);
        current_node = next_node;
      }
    }
    // add an empty route, in case a customer cannot be inserted
    // Insert.execute maintains this property
    currentSolution.addRoute();

    // adjust the number of samples to evauate the recourse cost
    // currentSolution.setNumSamples(num);
    if (this.recourseStrategy != null)
      currentSolution.getObjective(this.recourseStrategy);

    // for(Route route : currentSolution.routes)
    //   System.out.println(route);
    // System.out.println("Initial objective:"+currentSolution.getObjective());
    // set the schedule (i.e. time windows) as in the initial solution

    customerBank = new ArrayList<Customer>();
  }

  public void run(int num) {
    init(num);
    // Instantiate the neighbohoods operators
    destroyOperators = new ArrayList<Destroy>();
    insertOperators = new ArrayList<Insert>();

    // FIXME: dataModel.NR_CUST also counts depots (i.e. number of customers +
    // 2)
    GreedyDestroy greedy_r = new GreedyDestroy(
        "Greedy_r", this.dataModel, (int)(Math.ceil(dataModel.NR_CUST * 0.2)),
        (int)(Math.ceil(dataModel.NR_CUST * 0.2)));
    RandomDestroy random_r = new RandomDestroy(
        "Random_r", this.dataModel, (int)(Math.ceil(dataModel.NR_CUST * 0.2)),
        (int)(Math.ceil(dataModel.NR_CUST * 0.2)));
    RndGreedyDestroy rndgrd_r =
        new RndGreedyDestroy("RndGreedy_r", this.dataModel,
                             (int)(Math.ceil(dataModel.NR_CUST * 0.2)),
                             (int)(Math.ceil(dataModel.NR_CUST * 0.2)));
    destroyOperators.add(greedy_r);
    destroyOperators.add(random_r);
    destroyOperators.add(rndgrd_r);

    // GreedyInsert greedy_i = new GreedyInsert("Greedy_i", this.dataModel,
    // (int)(Math.ceil(dataModel.NR_CUST*0.05))); RndGreedyInsert rndgrd_i = new
    // RndGreedyInsert("RndGreedy_i", this.dataModel,
    // (int)(Math.ceil(dataModel.NR_CUST*0.05)));
    GreedyInsert greedy_i = new GreedyInsert(
        "Greedy_i", this.dataModel, (int)(Math.ceil(dataModel.NR_CUST * 0.05)),
        this.recourseStrategy, this.scenarios);
    RndGreedyInsert rndgrd_i =
        new RndGreedyInsert("RndGreedy_i", this.dataModel,
                            (int)(Math.ceil(dataModel.NR_CUST * 0.1)),
                            this.recourseStrategy, this.scenarios);

    insertOperators.add(greedy_i);
    insertOperators.add(rndgrd_i);

    // greedy_r,greedy_i 0,0   //GDGR
    // random_r,greedy_i 1,0   //RDGR
    // rndgrd_r,rndgrd_i 2,1   //rGDrGR
    currentNeighborhood = 1;  // FIXME: check paper for initial one...
    lastSwitch = 0;
    lastImprovement = 0;
    lastIncumbent = 0;

    boolean saveBuffer = true;
    try {  // buffer current solution, in case neighbohood is not accepted
      bestSolution = (HeuristicSolution)currentSolution.clone();
      // bestSolution.setNumSamples(num);
      // System.out.println(bestSolution.getObjective()+"
      // "+currentSolution.getObjective()); return;
    } catch (CloneNotSupportedException e) {
    }

    // System.out.println("\nSearching");
    for (Route r : this.currentSolution.routes) {
      r.resetServiceTimes();
      r.routeStart.start = dataModel.sourceDepot.start;
      r.routeEnd.end = dataModel.targetDepot.end;
      r.updateEarliest(r.routeStart, dataModel.routingGraph);
      r.updateLatest(r.routeEnd, dataModel.routingGraph);
      // System.out.println(r);
    }

    for (int iter = 0; iter < Constants.I; ++iter) {
      // System.out.println("Initial objective
      // "+currentSolution.getObjective()); System.out.println("current
      // neighbohood: "+currentNeighborhood); if(iter%100==0)
      //   System.out.print(".");
      if (saveBuffer)
        try {  // buffer current solution, in case neighbohood is not accepted
          backupSolution = (HeuristicSolution)currentSolution.clone();
        } catch (CloneNotSupportedException e) {
        }

      if (iter == Constants.SPLIT) {
        // TODO: split route large cost
        System.gc();
      }

      // destroy current solution
      destroyOperators.get(currentNeighborhood)
          .execute(this.currentSolution, this.customerBank);
      // recompute earliest and lates times
      // System.out.println("After removal:");
      for (Route r : this.currentSolution.routes) {
        r.resetServiceTimes();
        r.routeStart.start = dataModel.sourceDepot.start;
        r.routeEnd.end = dataModel.targetDepot.end;
        r.updateEarliest(r.routeStart, dataModel.routingGraph);
        r.updateLatest(r.routeEnd, dataModel.routingGraph);
        // System.out.println(r);
      }
      // System.out.println("Removed customers");
      // for(Customer c : customerBank)
      //   System.out.println(c);

      // recompute objective, in particular second stage costs given the
      // recourse
      double obj = currentSolution.getObjective(recourseStrategy);
      // obj = 0.0;
      // for(Route route: currentSolution.routes)
      //   obj += route.recourseCost;
      // if(Math.abs(currentSolution.getRecourseExpCost()-obj) > 0.01)
      //   System.out.println("recourse:
      //   "+currentSolution.getRecourseExpCost()+" per route:"+obj);

      // System.out.println("After removal:");
      // for(Route route : currentSolution.routes)
      // System.out.println(route);
      // System.out.println("------------------");

      // reinsert removed customers
      insertOperators.get(currentNeighborhood - 1)
          .execute(this.currentSolution, this.customerBank);
      // insertions maintain earliest and latest values correctly

      // System.out.println("After insertion:");
      // for(Route route : currentSolution.routes)
      // System.out.println(route);
      // System.out.println("New objective "+currentSolution.getObjective());
      // System.out.println("------------------");
      saveBuffer = true;
      if (this.backupSolution.getObjective() <=
          this.currentSolution
              .getObjective()) {  // new solution did not improve over current
                                  // solution
        if (iter - lastImprovement >= Constants.SWITCH &&
            iter - lastSwitch >= Constants.SWITCH) {
          this.customerBank.clear();
          destroyOperators.get(0).execute(this.currentSolution,
                                          this.customerBank);  // GREEDY destroy
          // TODO: run DP
          insertOperators.get(0).execute(
              this.currentSolution, this.customerBank);  // GREEDY insertion
          lastSwitch = iter;
          currentNeighborhood =
              (currentNeighborhood) % 2 + 1;  // 0 is only used during a switch
        } else {
          // roll back solution
          saveBuffer = false;
          try {
            // IDEA: instead of clone, copy elements in the same object, to
            // avoid mallocs
            this.currentSolution = (HeuristicSolution)backupSolution.clone();
          } catch (CloneNotSupportedException e) {
          }
        }
      } else {
        lastImprovement = iter;
        // System.out.println(currentSolution.getRoutingCost()+"+"+currentSolution.getRecourseExpCost());
      }

      double auxObj = Double.POSITIVE_INFINITY;
      if (this.recourseStrategy != null)
        auxObj = this.currentSolution.getObjective(this.recourseStrategy);
      else
        auxObj = this.currentSolution.getObjective();

      if (this.bestSolution.getObjective() > auxObj) {
        try {  // buffer current solution, in case neighbohood is not accepted
          bestSolution = (HeuristicSolution)currentSolution.clone();
        } catch (CloneNotSupportedException e) {
        }
        // System.out.println("New Incumbent at "+iter);
        // System.out.println(bestSolution.getObjective()+"
        // "+currentSolution.getObjective());
        lastIncumbent = iter;
      }
      if (iter - lastIncumbent > Constants.STOP_NO_IMPROVEMENT)
        iter = Constants.I;  // stop

      this.customerBank.clear();
    }  // for
    // System.out.println("|==");
    // System.out.println("0,0,0");

    // report best solution
    // System.out.println("Best solution: "+bestSolution.getObjective());
    for (Route route : bestSolution.routes) {
      if (route.isFeasible(dataModel.routingGraph) == false) {
        System.out.println(route);
        System.out.println("Route is infeasible...");
      }
      // assert route.isFeasible(dataModel.routingGraph) == true;
    }

    // convert to RDLPSolution
    resultSolution = this.convertSolution(bestSolution);
  }

  public RDLPSolution getSolution() { return resultSolution; }

  public HeuristicSolution _getSolution() { return bestSolution; }

  private RDLPSolution convertSolution(HeuristicSolution solution) {
    if (solution == null) return null;

    // else, construct RDLPSolution
    List<List<Location>> routes = new ArrayList<>();
    for (Route route : solution.routes) {
      if (route.size() ==
          2)  // empty route containing just the source and target depot.
        continue;
      List<Location> locationSequence = new ArrayList<>();
      for (Node node : route) locationSequence.add(node.location);
      routes.add(locationSequence);
    }
    return new RDLPSolution(dataModel, routes, (int)solution.getObjective());
  }

  private void availableLocations(
      Graph<Location, DefaultWeightedEdge> scenario) {
    for (Customer customer : dataModel.customers) {
      // System.out.println("Customer:"+customer);
      // save original time windows
      for (Location loc : customer.locations) {
        // node = locationToNodeMap.get(loc);
        loc.start_aux = loc.start;
        loc.end_aux = loc.end;
        // System.out.print("["+loc.start+","+loc.end+"],");
      }

      if (customer.locations.size() <= 1) {
        // System.out.println("\n---------------------");
        continue;
      }

      // System.out.println("\nAfter travel time reveal:");
      Location prev = null;
      int travelTime, order = 0;

      // new time windows given the revealed scenario
      for (Location loc : customer.locations) {
        if (order > 0) {
          travelTime = (int)scenario.getEdgeWeight(scenario.getEdge(prev, loc));
          // loc.start = Math.max(loc.start, prev.start + travelTime);
          loc.start = Math.max(loc.start, prev.end + travelTime);
          loc.end = Math.max(loc.end, loc.start);
          if (loc.start > loc.end_aux) loc.start = loc.end = 0;  // not
                                                                 // available
          // else
          // System.out.print("["+loc.start+","+loc.end+"],");
        }
        prev = loc;
        order++;
      }
    }
  }

  private void rollBackCustomersTW() {
    for (Customer customer : dataModel.customers) {
      for (Location loc : customer.locations) {
        loc.start = loc.start_aux;
        loc.end = loc.end_aux;
      }
    }
  }

  public List<Graph<Location, DefaultWeightedEdge>> getScenarios() {
    return this.scenarios;
  }
}
