package rdlp.algorithms.heuristic.improvement;

import rdlp.algorithms.heuristic.*;
import rdlp.algorithms.stochastic.*;

import rdlp.model.RDLPSolution;
import rdlp.model.RDLPInstance;
import rdlp.model.Customer;
import rdlp.model.Location;

import org.jgrapht.Graph;
import org.jgrapht.graph.DefaultWeightedEdge;

import java.util.*;

public class RndGreedyInsert extends Insert {
  // RDLPInstance dataModel;
  int K;  // size of the restricted candidate list

  // deterministic
  public RndGreedyInsert(String name, RDLPInstance dataModel, int rest) {
    super(name, dataModel);
    K = rest;
    insertionCandidates = new PriorityQueue<>(100, Collections.reverseOrder());
    queueToList = new ArrayList<Insertion>();
  }

  // stochastic
  public RndGreedyInsert(String name, RDLPInstance dataModel, int rest,
                         Recourse rec,
                         List<Graph<Location, DefaultWeightedEdge> > sce) {
    super(name, dataModel, rec, sce);
    K = rest;
    insertionCandidates = new PriorityQueue<>(100, Collections.reverseOrder());
    queueToList = new ArrayList<Insertion>();
  }

  public void execute(HeuristicSolution solution, List<Customer> toInsert) {
    // System.out.println(operator_name);
    Node node;
    double travel1, travel2;
    int earliest, latest;
    int after_earliest, before_latest;
    double delta;

    for (Customer cust : toInsert) {
      insertionCandidates.clear();
      // find an insertion for each location associated with the customer
      for (Location loc : cust.locations) {
        node = locationToNodeMap.get(loc);
        if (node.location.start <= 0 &&
            node.location.end <= 0)  // location is not available
        {
          // System.out.println("Not available");
          // System.exit(1);
          continue;
        }

        for (Route route : solution.routes) {
          if (route.capacity + node.location.customer.demand >
              dataModel.VEHIC_CAP)
            continue;
          for (Node after : route) {
            if (after == route.routeEnd) break;
            travel1 = dataModel.getTravelTime(after.location, node.location);
            // System.out.println(node.location+" "+after.next.location);
            travel2 =
                dataModel.getTravelTime(node.location, after.next.location);
            after_earliest = after.start;
            before_latest = after.next.end;
            earliest =
                Math.max(node.location.start, (int)(after_earliest + travel1));
            latest =
                Math.min(node.location.end, (int)(before_latest - travel2));

            if (earliest < latest)  // feasible?
            {
              if (insertionCandidates.size() >= K)  // keep oonly the best K
              {
                delta = this.evaluateInsertion(node, after, route);
                if (insertionCandidates.peek().cost >
                    delta)  // remove the worst one
                  insertionCandidates.poll();
              }
              // System.out.println(node+" "+after+" "+after.end);
              if (route.size() == 2 &&
                  solution.getNumRoutes() > dataModel.MAX_ROUTES)
                insertionCandidates.add(
                    new Insertion(solution, node, after, route, true));
              else
                insertionCandidates.add(
                    new Insertion(solution, node, after, route, false));
            }
          }
        }  // route
      }    // locations
      // reverse order of insertion profit
      queueToList.clear();
      while (insertionCandidates.peek() != null)
        queueToList.add(insertionCandidates.poll());

      // for(int i=0; i<queueToList.size(); ++i)
      //   System.out.println(queueToList.get(i));

      // performs a random insertion among the best K
      Insertion in;
      int listSize = queueToList.size();
      if (listSize > 0) {
        in = queueToList.get(rand.nextInt(listSize > K ? K : listSize));
        in.execute();
        node = in.node;
        in.ins_route.updateEarliest(node, dataModel.routingGraph);
        in.ins_route.updateLatest(node, dataModel.routingGraph);
        // System.out.println(ins.ins_route);
      }
      // else //infeasible insertion for the given routes
      // {
      //   System.out.println(cust+" not inserted");
      // }
    }  // customer
  }
}
