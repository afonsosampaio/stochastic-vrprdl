package rdlp.algorithms.heuristic.improvement;

import rdlp.algorithms.heuristic.*;
import rdlp.algorithms.stochastic.*;

import rdlp.model.RDLPSolution;
import rdlp.model.RDLPInstance;
import rdlp.model.Customer;
import rdlp.model.Location;

import org.jgrapht.Graph;
import org.jgrapht.graph.DefaultWeightedEdge;

import java.util.*;

public class GreedyInsert extends Insert {
  // RDLPInstance dataModel;
  int K;  // size of the restricted candidate list

  // deterministic
  public GreedyInsert(String name, RDLPInstance dataModel, int rest) {
    super(name, dataModel);
    K = rest;
    insertionCandidates = new PriorityQueue<>(100, Collections.reverseOrder());
    queueToList = new ArrayList<Insertion>();
  }

  // stochastic
  public GreedyInsert(String name, RDLPInstance dataModel, int rest,
                      Recourse rec,
                      List<Graph<Location, DefaultWeightedEdge> > sce) {
    super(name, dataModel, rec, sce);
    K = rest;
    insertionCandidates = new PriorityQueue<>(100, Collections.reverseOrder());
    queueToList = new ArrayList<Insertion>();
  }

  public void execute(HeuristicSolution solution, List<Customer> toInsert) {
    // System.out.println(operator_name);
    Node node;
    double travel1, travel2;
    int earliest, latest;
    int after_earliest, before_latest;
    double delta;

    for (Customer cust : toInsert) {
      insertionCandidates.clear();
      // find an insertion for each location associated with the customer
      for (Location loc : cust.locations) {
        node = locationToNodeMap.get(loc);
        if (node.location.start <= 0 &&
            node.location.end <= 0)  // location is not available
        {
          // System.out.println("Customer:"+cust);
          // for(Location l : cust.locations)
          //   System.out.print(l+"["+l.start+","+l.end+"] ");
          // System.out.println();
          // System.out.println("ERROR: Location not available!");
          // System.exit(1);
          continue;
          // here, only the original time windows are considered, so all should
          // be available some of the general instances contain not available
          // ([0,0] windows)
        }

        for (Route route : solution.routes) {
          if (route.capacity + node.location.customer.demand >
              dataModel.VEHIC_CAP)
            continue;
          for (Node after : route) {
            if (after == route.routeEnd) break;
            travel1 = dataModel.getTravelTime(after.location, node.location);
            // System.out.println(node.location+" "+after.next.location);
            travel2 =
                dataModel.getTravelTime(node.location, after.next.location);
            after_earliest = after.start;
            before_latest = after.next.end;
            earliest =
                Math.max(node.location.start, (int)(after_earliest + travel1));
            latest =
                Math.min(node.location.end, (int)(before_latest - travel2));

            if (earliest < latest)  // feasible?
            {
              if (insertionCandidates.size() >= K)  // keep oonly the best K
              {
                delta = this.evaluateInsertion(node, after, route);
                if (insertionCandidates.peek().cost >
                    delta)  // remove the worst one
                  insertionCandidates.poll();
              }
              // System.out.println(node+" "+after+" "+after.end);
              if (route.size() == 2 &&
                  solution.getNumRoutes() > dataModel.MAX_ROUTES)
                insertionCandidates.add(
                    new Insertion(solution, node, after, route, true));
              else
                insertionCandidates.add(
                    new Insertion(solution, node, after, route, false));
            }
          }
        }  // route
      }    // locations
      // reverse order of insertion profit
      queueToList.clear();
      while (insertionCandidates.peek() != null)
        queueToList.add(insertionCandidates.poll());

      // for(int i=0; i<queueToList.size(); ++i)
      //   System.out.println(queueToList.get(i));

      // performs the best insertion found
      if (queueToList.size() > 0) {
        queueToList.get(queueToList.size() - 1).execute();
        node = queueToList.get(queueToList.size() - 1).node;
        queueToList.get(queueToList.size() - 1)
            .ins_route.updateEarliest(node, dataModel.routingGraph);
        queueToList.get(queueToList.size() - 1)
            .ins_route.updateLatest(node, dataModel.routingGraph);
        // System.out.println(queueToList.get(queueToList.size()-1).ins_route);
      }
      // else //infeasible insertion for the given routes
      // {
      //   System.out.println(cust+" not inserted");
      // }
    }  // customer
  }
}
