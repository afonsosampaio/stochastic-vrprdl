package rdlp.algorithms.heuristic;

import rdlp.model.Location;

/**
 * Implementation of a doubly linked list node
 */
public class Node {
  /* Pointer to next node in linked list */
  public Node next = null;
  /* Pointer to previous node in linked list */
  public Node prev = null;

  /*effective windows*/
  public int start;
  public int end;

  /* Payload */
  public final Location location;

  public Node(Location location) {
    this.location = location;
    start = location.start;
    end = location.end;
  }

  @Override
  public String toString() {
    return location.toString();
  }
}
