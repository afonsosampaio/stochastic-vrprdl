package rdlp.algorithms.stochastic;

import rdlp.model.RDLPSolution;
import rdlp.model.RDLPInstance;
import rdlp.model.Customer;
import rdlp.model.Location;

import org.jgrapht.Graph;
import org.jgrapht.graph.AsWeightedGraph;
import org.jgrapht.graph.DefaultWeightedEdge;

import rdlp.algorithms.heuristic.HeuristicSolution;
import rdlp.algorithms.heuristic.constructive.ConstructiveHeuristic;
import rdlp.algorithms.heuristic.improvement.ImprovementHeuristic;

import rdlp.algorithms.stochastic.*;
import rdlp.algorithms.heuristic.Route;

import java.util.*;
import rdlp.util.*;
import static rdlp.util.TimeUtils.NANOSECONDS_TO_MILLISECONDS;
import java.text.DecimalFormat;

public class SAA {
  private int N;                         // size of the small sample set
  private int Np;                        // size of the larger sample set
  private int M;                         // Number of SAA replications to solve
  private static final int maxM = 1000;  // max number of SAA iterations

  private int initN;
  private int initNp;
  private int conv_iter;
  private boolean stop_conv;

  private double saa_gap, lower_bound;
  private int bestIndex;
  private double bestEstimateZ;
  private double bestSoFar;
  private int iterBest;
  private double SAAGap;  // target solution within this gap
  private double avgMissed;
  private double avgRescheduled;

  private double elapsedTime;
  private double TimeLimit;
  private double timeToIncumbent;

  private ConstructiveHeuristic constructive;
  private ImprovementHeuristic improvement;
  private HeuristicSolution current;
  private HeuristicSolution incumbent;
  private HeuristicSolution tentativeSol;

  private final RDLPInstance dataModel;
  private RDLPSolution initialSolution;

  private final Recourse recourseStrategy;

  private ScenarioGenerator scenarioGenerator;
  private final List<Graph<Location, DefaultWeightedEdge> > scenariosLarge;
  private final List<Graph<Location, DefaultWeightedEdge> > scenariosSmall;
  private final List<Graph<Location, DefaultWeightedEdge> > scenariosLargeEval;

  private List<HeuristicSolution> solutionTrace;

  private List<Recourse> recoursesList;

  /**
   * @param mdl the base instance to solve
   * @param rec a recourse function to evaluate in the second stage
   */
  public SAA(RDLPInstance mdl, Recourse rec, int np, int n, List<Graph<Location, DefaultWeightedEdge>> evalScenarios) {
    this.dataModel = mdl;
    this.recourseStrategy = rec;
    scenarioGenerator = new ScenarioGenerator(mdl);

    // NOTE: initial solution only taking into account routing costs (with t_ij
    // from instance)
    constructive = new ConstructiveHeuristic(dataModel);
    constructive.run();
    initialSolution = constructive.getSolution();
    scenariosLarge = new ArrayList<Graph<Location, DefaultWeightedEdge> >();
    scenariosSmall = new ArrayList<Graph<Location, DefaultWeightedEdge> >();
    scenariosLargeEval = evalScenarios;

    solutionTrace = new ArrayList<HeuristicSolution>();

    recoursesList = null;
    tentativeSol = null;

    // choose initial samples sizes N and N', tolerance epsilon
    initNp = Np = np;
    initN = N = n;
    M = maxM;
    conv_iter = Constants.CONVERGENCE_ITERATIONS;  // 25;
    stop_conv = false;
    SAAGap = Constants.SAA_GAP;
    TimeLimit = Double.POSITIVE_INFINITY;
    timeToIncumbent = 0.0;

    for (int i = 0; i < Np; ++i)
      scenariosLarge.add(scenarioGenerator.getScenario());
  }

  public void run() {
    double z_N;          // SAA problems
    double z_Np;         // over large sample size
    double v_zN, v_zNp;  // variances
    double aux, auxV;
    double avgErrors, varErrors, error_m;
    double sumErrors = 0.0;
    int iterN = 0;

    int replExec = 0;  // counts the number of SAA replications solved

    // stop criteria
    boolean lb_conv, gap_conv;

    List<Double> scenarioValues = new ArrayList<Double>();
    List<Double> scenarioEval = new ArrayList<Double>();

    DecimalFormat df = new DecimalFormat("0.00");
    // df.setMaximumFractionDigits(2);
    DecimalFormat df0 = new DecimalFormat();
    df0.setMaximumFractionDigits(0);

    for (int i = 0; i < Np; ++i) scenarioValues.add(0.0);

//    for (int i = 0; i < Np; ++i)
//      scenariosLargeEval.add(scenarioGenerator.getScenario());

//    for (int i = 0; i < N; ++i)
//      scenariosSmall.add(scenarioGenerator.getScenario());

    // statistics
    List<Double> averageTrace = new ArrayList<Double>();
    List<Double> evalNTrace = new ArrayList<Double>();
    List<Double> evalNpTrace = new ArrayList<Double>();
    List<Double> avgNpTrace = new ArrayList<Double>();
    List<Double> avgNTrace = new ArrayList<Double>();
    List<Double> bestXTrace = new ArrayList<Double>();
    List<Double> error_i = new ArrayList<Double>();
    List<Integer> NTrace = new ArrayList<Integer>();

    double avgZN;
    double avgAux;
    // double avgOmega;

    // List<ImprovementHeuristic> SAAProblems = new  ArrayList<ImprovementHeuristic>();
    List<Graph<Location, DefaultWeightedEdge> > SAAscenarios;

    // profiling
    long freeMemory = Runtime.getRuntime().freeMemory();
    long totalMemory = Runtime.getRuntime().totalMemory();
    long maxMemory = Runtime.getRuntime().maxMemory();
    // System.out.printf("Available:
    // %5s\nFree%5s\nMax:%5s\n",totalMemory/Constants.MBYTES,
    // freeMemory/Constants.MBYTES, maxMemory/Constants.MBYTES);

    z_N = z_Np = 0;
    bestSoFar = Double.POSITIVE_INFINITY;
    iterBest = 0;
    int start = 0;
    double runTimeStart = TimeUtils.getCpuTime();

    // here dataModel.routingGraph should have the travel time without
    // (expected) delay
    // int counter = 0;
    // for(DefaultWeightedEdge e : dataModel.routingGraph.edgeSet())
    // {
    //   System.out.println(e+"="+dataModel.routingGraph.getEdgeWeight(e));
    //   if(counter++>10)
    //     break;
    // }
    // System.out.println("-------------------------------");

    // VERBOSE
    // System.out.println("conv_iter "+conv_iter);
    System.out.println(
        "+---------------------------------------------------------------------------------------------------------------------+");
    System.out.printf("%38s %35s %35s",
                      "|         Current Replication          |",
                      "                  Bounds                 |",
                      "            Work                  |");
    System.out.println(
        "\n+---------------------------------------------------------------------------------------------------------------------+");
    System.out.printf("%38s %31s %31s",
                      "|Size  1st Stage   2nd Stage  Value(O) |",
                      "Incumbent   LowerBnd    Std    Gap   Std |",
                      " Time(s) JVMHeap JVMUsed  JVMFree |");
    System.out.println(
        "\n+---------------------------------------------------------------------------------------------------------------------+");

    // NOTE: Local search (solving SAA replication)
    // starts with deterministic solutions
    // provides better lower bound estimates and more stable results!
    if (tentativeSol != null)
      initialSolution = RDLPSolution.convertSolution(tentativeSol, dataModel);

    for (int m = 1; m <= M; ++m) {
      {
        // solve the SAA problem with sample size N
        improvement = new ImprovementHeuristic(initialSolution, dataModel,
                                               recourseStrategy);
        improvement.run(N);
        current = improvement._getSolution();
        // check whether max number of routes is respected
        // if not, ignore the solution obtained...
        if (dataModel.MAX_ROUTES < current.getNumRoutes()) {
          System.out.println("*H");
          m--;
          continue;
        }
      }

      solutionTrace.add(current);
      // SAAProblems.add(improvement);

      // VEROBOSE
      // df.format(current.getObjective())
      // System.out.println(current);

      // evaluates x_curr over the large sample set
      // System.out.println("Evaluating:");
      for (int i = 0; i < Np; ++i)
        scenarioValues.set(i, recourseStrategy.evaluateScenario(
                                  current, scenariosLarge.get(i)));
      z_Np = 0;
      for (int i = 0; i < Np; ++i) z_Np += scenarioValues.get(i);
      z_Np = current.getRoutingCost() + z_Np / Np;
      evalNpTrace.add(z_Np);
      if (z_Np < bestSoFar) {
        bestSoFar = z_Np;
        iterBest = m;
        timeToIncumbent = 0.001 * (TimeUtils.getCpuTime() - runTimeStart) *
                          NANOSECONDS_TO_MILLISECONDS;
      }
      bestXTrace.add(bestSoFar);

      if (m < 2)  // cannot compute variance (div by 0)
      {
        elapsedTime = 0.001 * (TimeUtils.getCpuTime() - runTimeStart) *
                      NANOSECONDS_TO_MILLISECONDS;

        averageTrace.add(solutionTrace.get(0).getObjective());
        evalNTrace.add(solutionTrace.get(0).getObjective());
        error_i.add(Math.abs(bestSoFar - averageTrace.get(0)) / bestSoFar);
        NTrace.add(N);
        NTrace.add(m);
        NTrace.add((int)(averageTrace.get(0).doubleValue()));

        freeMemory =
            Runtime.getRuntime().freeMemory() / Constants.BYTE_TO_MBYTE;
        totalMemory =
            Runtime.getRuntime().totalMemory() / Constants.BYTE_TO_MBYTE;
        maxMemory = Runtime.getRuntime().maxMemory() / Constants.BYTE_TO_MBYTE;

        System.out.format(
            "%5s %10s %10s %10s |", N, df.format(current.getRoutingCost()),
            df.format(current.getRecourseExpCost()), df.format(z_Np));
        System.out.format("%10s  %10s   %4s   %3s  %3s|", df.format(bestSoFar),
                          df.format(averageTrace.get(0)), df.format(0.00),
                          df.format(0.00), df.format(0.00));
        System.out.format("%7s %7s %7s %7s", df0.format(elapsedTime),
                          totalMemory, maxMemory - freeMemory, freeMemory);
        // System.out.println(" routes:"+current.getNumRoutes());
        System.out.println();

        continue;
      }

      // conv_iter last replications
      start = (m - iterN) > conv_iter ? m - conv_iter : iterN;  // variance of the past conv_iter values

      // computes average of SAA solutions computed so far (for the current
      // value of N) given an estimate lower bound
      z_N = 0;
      for (int i = iterN; i < m; ++i)
        z_N += solutionTrace.get(i).getObjective();
      z_N = z_N / (m - iterN);
      averageTrace.add(z_N);

      // average z_N over past conv_iter replications
      avgAux = 0.0;
      for (int i = start; i < m; ++i) avgAux += averageTrace.get(i);
      avgAux = avgAux / (m - start);

      v_zN = 0;  // variance of the past conv_iter values z_N
      for (int i = start; i < m; ++i) {
        aux = (averageTrace.get(i) - avgAux);
        v_zN += aux * aux;
      }
      v_zN = v_zN / (m - start - 1);

      // average of solutions found over large sample set (for a given N)
      avgZN = 0.0;
      for (int i = iterN; i < m; ++i) avgZN += evalNpTrace.get(i);
      avgZN = avgZN / (m - iterN);
      avgNpTrace.add(avgZN);  // NOTE:only for statistics and graphs
      // variance of the past avgZ
      auxV = 0.0;
      for (int i = start; i < m; ++i) {
        aux = (evalNpTrace.get(i) - avgZN);
        auxV += aux * aux;
      }
      auxV = auxV / ((m - iterN - 1) *
                     (m - iterN));  // NOTE:only for statistics and graphs

      // current error(gap) estimate
      // error_m = Math.abs(bestSoFar-z_N)/bestSoFar;
      // //(Math.abs(avgZN-z_N)/avgZN))
      error_m = Math.abs(z_Np - current.getObjective()) / z_Np;
      error_i.add(error_m);
      // average of errors in the past conv_iter iterations
      sumErrors = 0.0;
      for (int i = start; i < m; ++i) sumErrors += error_i.get(i);
      avgErrors = sumErrors / (m - start);
      // variance of the last conv_iter errors
      varErrors = 0.0;
      for (int i = start; i < m; ++i) {
        aux = (error_i.get(i) - avgErrors);
        varErrors += aux * aux;
      }
      varErrors = varErrors / (m - start - 1);

      elapsedTime = 0.001 * (TimeUtils.getCpuTime() - runTimeStart) *
                    NANOSECONDS_TO_MILLISECONDS;

      freeMemory = Runtime.getRuntime().freeMemory() / Constants.BYTE_TO_MBYTE;
      totalMemory =
          Runtime.getRuntime().totalMemory() / Constants.BYTE_TO_MBYTE;
      maxMemory = Runtime.getRuntime().maxMemory() / Constants.BYTE_TO_MBYTE;

      System.out.format(
          "%5s %10s %10s %10s |", N, df.format(current.getRoutingCost()),
          df.format(current.getRecourseExpCost()), df.format(z_Np));
      System.out.format("%10s  %10s   %4s   %3s  %3s|", df.format(bestSoFar),
                        df.format(z_N), df.format(Math.sqrt(v_zN) / avgAux),
                        df.format(error_m), df.format(Math.sqrt(varErrors)));
      System.out.format("%7s %7s %7s %7s", df0.format(elapsedTime), totalMemory,
                        maxMemory - freeMemory, freeMemory);
      System.out.println();
      // System.out.println(" routes:"+current.getNumRoutes());
      // System.out.println(" z_np="+df.format(z_Np));

      // stop criteria
      lb_conv = Math.sqrt(v_zN) / avgAux < Constants.CONVERGENCE_TOLERANCE
                    ? true
                    : false;
      gap_conv =
          Math.sqrt(varErrors) < Constants.CONVERGENCE_TOLERANCE ? true : false;
      // gap_conv && ----
      if ((lb_conv || gap_conv) &&
          m - iterN >= Constants.CONVERGENCE_ITERATIONS) {
        // NOTE: if z_N > bestSoFar increasing N will increase z_N and, thus,
        // gap will only increase... (as bestSoFar can only decrease)
        if ((Math.abs(bestSoFar - z_N) / bestSoFar < SAAGap) || stop_conv ||
            z_N >= bestSoFar)  // upper bound //&& Math.sqrt(auxV)/avgZN <=
                               // Constants.CONVERGENCE_TOLERANCE
          m = M;  // stops
        else {
          iterN = m;
          error_m = Math.abs(bestSoFar - z_N) / bestSoFar;
          if (Constants.SCENARIOS_TO_INCREASE *
                  (int)(Math.ceil(error_m / SAAGap)) >
              Constants.MAX_INCREASE)
            N = N + Constants.MAX_INCREASE;
          else
            N = N + Constants.SCENARIOS_TO_INCREASE *
                        (int)(Math.ceil(error_m / SAAGap));
          NTrace.add(N);
          NTrace.add(m);
          NTrace.add((int)(z_N));
          // System.out.println("New N:"+N);
        }
      }

      replExec++;

      // running time stop?
      if (TimeLimit < Double.POSITIVE_INFINITY)
        if (elapsedTime >= TimeLimit)
          m = M;  // stops
        else
          m = replExec;

    }
    NTrace.add(N);
    NTrace.add(replExec);
    NTrace.add((int)(z_N));

    // get the best x over Np independent samples,
    // but over an independent sample set, to obtain an unbiased estimator...
    bestIndex = -1;
    bestEstimateZ = Double.POSITIVE_INFINITY;
    double missed;
    double bestMissed = Double.POSITIVE_INFINITY;
    double rescheduled;
    double bestRescheduled = Double.POSITIVE_INFINITY;
    for (int i = 0; i < replExec; ++i) {
      aux = 0;
      missed = 0;
      rescheduled = 0;
      for (int j = 0; j < Np; ++j) {
        aux += recourseStrategy.evaluateScenario(solutionTrace.get(i),
                                                 scenariosLargeEval.get(j));
        missed += recourseStrategy.missedCustomers;
        rescheduled += recourseStrategy.rescheduledCustomers;
      }
      aux = aux / Np;
      aux += solutionTrace.get(i).getRoutingCost();
      if (bestEstimateZ > aux) {
        bestEstimateZ = aux;
        bestIndex = i;
        bestMissed = missed;
        bestRescheduled = rescheduled;
      }
    }

    lower_bound = z_N;
    saa_gap = (bestEstimateZ - lower_bound) / bestEstimateZ;
    incumbent = solutionTrace.get(bestIndex);
    avgMissed = bestMissed / Np;
    avgRescheduled = bestRescheduled / Np;

    System.out.println("Best objective " + df.format(bestSoFar) +
                       " best bound " + df.format(z_N) + " gap " +
                       df.format(saa_gap));
    System.out.println("Avg missed:" + df.format(avgMissed));
    System.out.println("Avg rescheduled:" + df.format(avgRescheduled));
    System.out.println("Avg TW:" + df.format(incumbent.getAverageSlackTW()));
    System.out.println("After " + replExec + " SAA replications");
    System.out.println(incumbent);

    System.out.println("avg1 = " + averageTrace);
    System.out.println("avg2 = " + evalNpTrace);
    System.out.println("avg3 = " + avgNpTrace);
    System.out.println("N =" + NTrace);
    System.out.println("Best = [" + (iterBest - 1) + "," + bestSoFar + "]");

  }

  public void setInitialSolution(HeuristicSolution sol) {
    this.tentativeSol = sol;
    for (Route r : sol.routes) {
      r.resetServiceTimes();
      r.routeStart.start = dataModel.sourceDepot.start;
      r.routeEnd.end = dataModel.targetDepot.end;
      r.updateEarliest(r.routeStart, dataModel.routingGraph);
      r.updateLatest(r.routeEnd, dataModel.routingGraph);
      r.recomputeCost();
    }
    System.out.println("EV solution (under stochastic tt)");
    System.out.println(sol);

    double avg1 = 0;
    for (int i = 0; i < 100; ++i) {
      sol.setScenarios(1);
      avg1 += sol.getObjective(this.recourseStrategy);
      // System.out.println(sol.getRecourseExpCost());
    }
    System.out.println(sol.getRoutingCost());
    avg1 = avg1 / 100;
    System.out.println("Lower bound: " + avg1);
  }

  public void setRecourses(List<Recourse> listRec) {
    this.recoursesList = listRec;
  }

  public void setN(int n) { initN = N = n; }

  public void setNp(int n) { initNp = Np = n; }

  public void setGap(double gap) { this.SAAGap = gap; }

  public void setCI(int ci) { conv_iter = ci; }

  public void set_stopConv() { stop_conv = true; }

  public void setTimeLim(double lim) { TimeLimit = lim; }

  public HeuristicSolution getSolution() { return incumbent; }

  public double getObjective() { return bestEstimateZ; }

  /**
   * @return the estimated cost for the incumbent solution considering the large
   * sample set NOTE: best_estimator is f(x) + E[g(x)] i.e. routing cost +
   * expected recourse cost
   */
  public double getExpRecourseCost() {
    return bestEstimateZ - incumbent.getRoutingCost();
  }

  public double getGapEstimator() { return saa_gap; }
  public double getLowerBound()  // NOTE: is it a valid lower bound if SAA not
                                 // solved to optimality?
  {
    return lower_bound;
  }
  public double getCpuTime() { return elapsedTime; }
  public double getTimeToBest() { return timeToIncumbent; }

  public double getMissed() { return avgMissed; }

  public double getRescheduled() { return avgRescheduled; }

  public List<Graph<Location, DefaultWeightedEdge> > getNpSamples() {
    return scenariosLargeEval;
  }

  public void largeSamplesTesting(HeuristicSolution sol) {
    List<Graph<Location, DefaultWeightedEdge> > scenarios =
        new ArrayList<Graph<Location, DefaultWeightedEdge> >();
    ScenarioGenerator scenarioGenerator = new ScenarioGenerator(this.dataModel);
    List<Double> evalTrace = new ArrayList<Double>();
    double bestSol, maxSol, avgSol, varSol, aux;
    int EVALS = 100;

    bestSol = Double.POSITIVE_INFINITY;
    maxSol = 0.0;
    for (int e = 0; e < EVALS; ++e) {
      scenarios.clear();
      for (int i = 0; i < Np; ++i)
        scenarios.add(scenarioGenerator.getScenario());

      aux = 0.0;
      for (int i = 0; i < Np; ++i)
        aux += recourseStrategy.evaluateScenario(sol, scenarios.get(i));
      aux = aux / Np;
      aux += sol.getRoutingCost();
      evalTrace.add(aux);
      if (bestSol > aux) bestSol = aux;
      if (maxSol < aux) maxSol = aux;
    }

    // statistics
    DecimalFormat df = new DecimalFormat("0.00");
    double sum = evalTrace.stream().mapToDouble(Double::doubleValue).sum();
    avgSol = sum / EVALS;
    System.out.println(evalTrace);
    System.out.println("Average: " + df.format(avgSol));
    System.out.println("Best: " + df.format(bestSol));
    System.out.println("Worst: " + df.format(maxSol));
    varSol = 0.0;
    for (int i = 0; i < EVALS; ++i) {
      aux = evalTrace.get(i) - avgSol;
      varSol += aux * aux;
    }
    // varSol = varSol/(EVALS*(EVALS-1));
    varSol = varSol / (EVALS - 1);
    System.out.println("Variance: " + df.format(varSol));
  }
}
