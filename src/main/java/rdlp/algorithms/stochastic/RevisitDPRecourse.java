package rdlp.algorithms.stochastic;

import rdlp.model.RDLPSolution;
import rdlp.model.RDLPInstance;
import rdlp.model.Customer;
import rdlp.model.Location;

import org.jgrapht.Graph;
import org.jgrapht.graph.AsWeightedGraph;
import org.jgrapht.graph.DefaultWeightedEdge;

import rdlp.algorithms.heuristic.HeuristicSolution;
import rdlp.algorithms.heuristic.Node;
import rdlp.algorithms.heuristic.Route;

import rdlp.util.Constants;

import java.util.*;

public class RevisitDPRecourse extends Recourse {
  private HeuristicSolution solution;
  private Graph<Location, DefaultWeightedEdge> scenario;

  private Queue<SingleInsertion> insertionCandidates;
  Map<Location, Node> locationToNodeMap = new HashMap<>();
  Set<Location> routeLocations = new HashSet<Location>();

  // to keep track of best solution in the DP recursion
  private List<Location> bestVisited;
  private double bestTotal;       // keep track of best set of visited customers
  private Stack<Location> stack;  // the path visited in the DP
  private Route evalRoute;

  public RevisitDPRecourse(RDLPInstance dataModel) {
    super(dataModel);
    insertionCandidates = new PriorityQueue<>(100, Collections.reverseOrder());

    bestVisited = new ArrayList<Location>();
    stack = new Stack<Location>();

    for (Location loc : dataModel.routingGraph.vertexSet())
      locationToNodeMap.put(loc, new Node(loc));

    evalRoute =
        new Route(dataModel);  // so, do not allocate memory every iteration...
  }

  /**
   * @return the second stage cost (travelled distance + recourse penalty, in
   time unit) for a given first stage solution (@param solution) and realized
   scenario (@param scenario)
 */
  public double evaluateScenario(HeuristicSolution sol,
                                 Graph<Location, DefaultWeightedEdge> sce) {
    this.solution = sol;
    this.scenario = sce;

    // tau_(i-1) + t_(i-1,i) > L_i ==> skip
    int missed = 0;  // total missed deliveries, considering all routes
    int startTime, travelTime, auxTime;
    boolean skip;
    Node current, next;
    double recourseCost = 0.0;
    double routeCost;

    availableLocations(scenario);

    // Allow overtime for vehicles returning to the depot.
    int aux = dataModel.targetDepot.end;
    dataModel.targetDepot.end = 999999;  // TODO: not the best solution....

    for (Route route : solution.routes) {
      if (route.size() == 2)  // skip empty routes
        continue;

      // customers visited by the route
      routeLocations.clear();
      for (Node ic : route)
        if (ic != route.routeStart && ic != route.routeEnd)
          routeLocations.add(ic.location);

      // decide which customers to skip
      notVisited.clear();
      locsNotVisited.clear();
      visited.clear();
      bestTotal = 9999999;
      bestVisited.clear();

      double res = computePath(route, route.routeStart.next, route.routeStart,
                               route.routeStart.location.start, 0);

      // the actual implemented route
      Route new_route = new Route(dataModel);
      Node nxt, curr;
      curr = new_route.routeStart;
      for (Location loc : bestVisited) {
        // remove from routeCustomers(will give the set of not visited
        // customers)
        routeLocations.remove(loc);
        nxt = new Node(loc);
        new_route.insertNodeAfter(nxt, curr, scenario);
        curr = nxt;
      }
      new_route.updateEarliest(new_route.routeStart, scenario);
      new_route.updateLatest(new_route.routeEnd, scenario);
      if (new_route.isFeasible(scenario) ==
          false)  // should always be with 999999 as latest arrival at depot
      {
        System.out.println(route);
        System.out.println(new_route);
        System.out.println(bestVisited);
        System.out.println("-------------------------");
        assert new_route.isFeasible(scenario) == true;
      }
      // get customers not visited by the realized route
      Iterator<Location> iterator = routeLocations.iterator();
      Location auxLoc;
      while (iterator.hasNext()) {
        auxLoc = iterator.next();
        notVisited.add(auxLoc.customer);
        locsNotVisited.add(auxLoc);
      }

      missed += route.size() - new_route.size();
      routeCost = recourseCost;
      // penalty incurred for skipped customers. if a new location for a skipped
      // customer is inserted, penalty is decreased accordingly remove overtime
      // for now. when all potential customers are inserted, evaluate
      recourseCost += (res - Constants.OVERTIME_MULTIPLIER *
                                 Math.max(new_route.routeEnd.start -
                                              dataModel.sourceDepot.end,
                                          0));

      // try to re-insert not visited customers into the route, visiting a
      // different location
      // if(notVisited.size() == 0)
      //   continue;

      double travel1, travel2;
      int earliest, latest;
      int after_earliest, before_latest;
      double delta;
      Node node;
      SingleInsertion ins;
      Customer cust;
      Location Cloc;  // to compute the penalty incurred by not visiting a
                      // particular customer location

      // for(Customer cust : notVisited)
      for (int cc = 0; cc < notVisited.size(); ++cc) {
        cust = notVisited.get(cc);
        Cloc = locsNotVisited.get(cc);
        insertionCandidates.clear();
        // find an insertion for each location associated with the customer
        for (Location loc : cust.locations) {
          node = locationToNodeMap.get(loc);
          for (Node after : new_route) {
            if (after == new_route.routeEnd) break;
            travel1 = (int)scenario.getEdgeWeight(
                scenario.getEdge(after.location, node.location));
            travel2 = (int)scenario.getEdgeWeight(
                scenario.getEdge(node.location, after.next.location));
            after_earliest = after.start;
            before_latest = after.next.end;
            earliest =
                Math.max(node.location.start, (int)(after_earliest + travel1));
            latest =
                Math.min(node.location.end, (int)(before_latest - travel2));

            if (earliest <= latest)  // feasible?
            {
              if (insertionCandidates.size() >= 10)  // keep oonly the best
              {
                delta = this.evaluateInsertion(node, after, new_route);
                if (insertionCandidates.peek().cost >
                    delta)  // remove the worst one
                  insertionCandidates.poll();
              }
              // System.out.println(node+" "+after+" "+after.end);
              insertionCandidates.add(
                  new SingleInsertion(node, after, new_route, scenario));
            }
          }
        }  // location

        if (insertionCandidates.peek() !=
            null)  // there is a feasible insertion!
        {
          ins = insertionCandidates.poll();
          int aux_overTime =
              Math.max(ins.ins_route.routeEnd.start - dataModel.sourceDepot.end,
                       0);  // overtime before insertion
          ins.execute(scenario);
          ins.ins_route.updateEarliest(ins.node, scenario);
          ins.ins_route.updateLatest(ins.node, scenario);

          // check the cost of re-inserting the customer
          // assess whether there's is overtime and if it compensantes the cost
          // of a dedicated service
          int overtime_aux =
              Math.max(ins.ins_route.routeEnd.start - dataModel.sourceDepot.end,
                       0);  // TODO: targetDepot was modified
          // only account for the overtime due to the insertion off the customer
          // NOTE: overtime >= aux_overTime
          // overtime -= aux_overTime;

          // compare with the total overtime in the route (not only the overtime
          // due to the insertion)
          // scenario.getEdgeWeight(scenario.getEdge(dataModel.sourceDepot,
          // Cloc)) + scenario.getEdgeWeight(scenario.getEdge(Cloc,
          // dataModel.targetDepot))
          if (overtime_aux <= Constants.MAX_OVERTIME &&
              Constants.OVERTIME_MULTIPLIER * overtime_aux + ins.cost <
                  Constants.UNVISITED_MULTIPLIER * (Cloc.customer.penalty)) {
            // System.out.println("inserted"+ins.ins_route.routeEnd.start+"
            // "+dataModel.sourceDepot.end+" "+overtime);
            missed--;
            // remove cost o dedicated service
            // dataModel.routingGraph.getEdgeWeight(dataModel.routingGraph.getEdge(dataModel.sourceDepot,
            // Cloc))+dataModel.routingGraph.getEdgeWeight(dataModel.routingGraph.getEdge(Cloc,
            // dataModel.targetDepot))
            recourseCost -=
                Constants.UNVISITED_MULTIPLIER * (Cloc.customer.penalty);
          } else  // remove the (re)inserted customer. more profitable to be
                  // serviced by dedicated vehicle
          {
            // System.out.println("removed"+ins.ins_route.routeEnd.start+"
            // "+dataModel.sourceDepot.end+" "+overtime);
            ins.ins_route.remove(ins.node, scenario);
            ins.ins_route.updateEarliest(ins.ins_route.routeStart, scenario);
            ins.ins_route.updateLatest(ins.ins_route.routeEnd, scenario);
          }
        }
      }  // customer

      // account for the current overtime in the route
      recourseCost +=
          Constants.OVERTIME_MULTIPLIER *
          Math.max(new_route.routeEnd.start - dataModel.sourceDepot.end, 0);
      route.recourseCost += (recourseCost - routeCost);
    }  // routes
    missedCustomers = missed;
    dataModel.targetDepot.end = aux;

    rollBackCustomersTW();

    return recourseCost;
  }

  public double evaluateScenario(Route rt, Node node, Node after,
                                 Graph<Location, DefaultWeightedEdge> sce,
                                 int index) {
    this.scenario = sce;

    int route_missed = 0;
    int startTime, travelTime, auxTime;
    boolean skip;
    Node current, next;
    double recourseCost = 0.0;

    evalRoute.clear();

    notVisited.clear();
    visited.clear();
    bestTotal = 9999999;
    bestVisited.clear();
    locsNotVisited.clear();

    evalRoute.routeEnd.end = dataModel.targetDepot.end;

    current = evalRoute.routeStart;
    for (Node i : rt) {
      if (i == rt.routeStart || i == rt.routeEnd) continue;
      if (i == after.next) {
        next = new Node(node.location);
        evalRoute.insertNodeAfter(next, current, dataModel.routingGraph);
        current = next;
      }
      next = new Node(i.location);
      evalRoute.insertNodeAfter(next, current, dataModel.routingGraph);
      current = next;
    }
    evalRoute.updateEarliest(evalRoute.routeStart, dataModel.routingGraph);
    evalRoute.updateLatest(evalRoute.routeEnd, dataModel.routingGraph);
    // evalRoute should always be feasible (even with original latest time for
    // targetDepot)
    if (evalRoute.isFeasible(dataModel.routingGraph) == false) {
      System.out.println(rt);
      System.out.println(evalRoute);
      evalRoute.isFeasibleVERBOSE(dataModel.routingGraph);
      assert evalRoute.isFeasible(dataModel.routingGraph) == true;
    }

    // customers visited by the route
    routeLocations.clear();
    for (Node ic : evalRoute)
      if (ic != evalRoute.routeStart && ic != evalRoute.routeEnd)
        routeLocations.add(ic.location);

    availableLocations(scenario, index);
    int aux = dataModel.targetDepot.end;
    dataModel.targetDepot.end = 999999;  // TODO: not the best solution....
    evalRoute.routeEnd.end = dataModel.targetDepot.end;

    // now evaluate the scenario on the new route, for which node @node is
    // visited after node @after
    recourseCost =
        computePath(evalRoute, evalRoute.routeStart.next, evalRoute.routeStart,
                    evalRoute.routeStart.location.start, 0);
    recourseCost -=
        Constants.OVERTIME_MULTIPLIER *
        Math.max(evalRoute.routeEnd.start - dataModel.sourceDepot.end, 0);

    // the actual implemented route
    evalRoute.clear();
    current = evalRoute.routeStart;
    for (Location loc : bestVisited) {
      routeLocations.remove(loc);
      next = new Node(loc);
      evalRoute.insertNodeAfter(next, current, scenario);
      current = next;
    }
    // System.out.println("route Locations:"+routeLocations);
    evalRoute.updateEarliest(evalRoute.routeStart, scenario);
    evalRoute.updateLatest(evalRoute.routeEnd, scenario);

    if (evalRoute.isFeasible(scenario) ==
        false)  // should always be with 999999 as latest arrival at depot
    {
      System.out.println(rt);
      System.out.println(evalRoute);
      System.out.println(bestVisited);
      System.out.println("-------------------------");
      assert evalRoute.isFeasible(scenario) == true;
    }
    // get customers not visited by the realized route
    Iterator<Location> iterator = routeLocations.iterator();
    Location auxLoc;
    while (iterator.hasNext()) {
      auxLoc = iterator.next();
      notVisited.add(auxLoc.customer);
      locsNotVisited.add(auxLoc);
    }

    // try to re-insert not visited customers into the route, visiting a
    // different location
    if (notVisited.size() == 0)  // only overtime
    {
      dataModel.targetDepot.end = aux;
      rollBackCustomersTW();
      return Constants.OVERTIME_MULTIPLIER *
          Math.max(evalRoute.routeEnd.start - dataModel.sourceDepot.end, 0);
    }

    // System.out.println("Not visited: "+notVisited);
    // System.out.println("Locations not visited: "+locsNotVisited);
    // System.out.println(evalRoute);
    // System.out.println(recourseCost);

    double travel1, travel2;
    int earliest, latest;
    int after_earliest, before_latest;
    double delta;
    SingleInsertion ins;
    Customer cust;
    Location Cloc;  // to compute the penalty incurred by not visiting a
                    // particular customer location

    route_missed = 0;
    // for(Customer cust : notVisited)
    for (int cc = 0; cc < notVisited.size(); ++cc) {
      cust = notVisited.get(cc);
      Cloc = locsNotVisited.get(cc);
      insertionCandidates.clear();
      // find an insertion for each location associated with the customer
      for (Location loc : cust.locations) {
        node = locationToNodeMap.get(loc);
        for (Node after_ : evalRoute) {
          if (after_ == evalRoute.routeEnd) break;
          travel1 = (int)scenario.getEdgeWeight(
              scenario.getEdge(after_.location, node.location));
          travel2 = (int)scenario.getEdgeWeight(
              scenario.getEdge(node.location, after_.next.location));
          after_earliest = after_.start;
          before_latest = after_.next.end;
          earliest =
              Math.max(node.location.start, (int)(after_earliest + travel1));
          latest = Math.min(node.location.end, (int)(before_latest - travel2));

          if (earliest <= latest)  // feasible?
          {
            if (insertionCandidates.size() >= 10)  // keep oonly the best
            {
              delta = this.evaluateInsertion(node, after_, evalRoute);
              if (insertionCandidates.peek().cost >
                  delta)  // remove the worst one
                insertionCandidates.poll();
            }
            // System.out.println(node+" "+after+" "+after.end);
            insertionCandidates.add(
                new SingleInsertion(node, after_, evalRoute, scenario));
          }
        }
      }  // location

      if (insertionCandidates.peek() != null)  // there is a feasible insertion!
      {
        ins = insertionCandidates.poll();
        int aux_overTime =
            Math.max(ins.ins_route.routeEnd.start - dataModel.sourceDepot.end,
                     0);  // overtime before insertion
        ins.execute(scenario);
        ins.ins_route.updateEarliest(ins.node, scenario);
        ins.ins_route.updateLatest(ins.node, scenario);

        // check the cost of re-inserting the customer
        // assess whether there's is overtime and if it compensantes the cost of
        // a dedicated service
        int overtime =
            Math.max(ins.ins_route.routeEnd.start - dataModel.sourceDepot.end,
                     0);  // TODO: targetDepot was modified
        // only account for the overtime due to the insertion off the customer
        // NOTE: overtime >= aux_overTime
        // overtime -= aux_overTime;

        // compare with the total overtime in the route (not only the overtime
        // due to the insertion)
        // scenario.getEdgeWeight(scenario.getEdge(dataModel.sourceDepot, Cloc))
        // + scenario.getEdgeWeight(scenario.getEdge(Cloc,
        // dataModel.targetDepot))
        if (overtime <= Constants.MAX_OVERTIME &&
            Constants.OVERTIME_MULTIPLIER * overtime + ins.cost <
                Constants.UNVISITED_MULTIPLIER * (Cloc.customer.penalty)) {
          // System.out.println("inserted"+ins.ins_route.routeEnd.start+"
          // "+dataModel.sourceDepot.end+" "+overtime);
          // remove cost o dedicated service
          // dataModel.routingGraph.getEdgeWeight(dataModel.routingGraph.getEdge(dataModel.sourceDepot,
          // Cloc))+dataModel.routingGraph.getEdgeWeight(dataModel.routingGraph.getEdge(Cloc,
          // dataModel.targetDepot))
          recourseCost -=
              Constants.UNVISITED_MULTIPLIER * (Cloc.customer.penalty);
        } else  // remove the (re)inserted customer. more profitable to be
                // serviced by dedicated vehicle
        {
          // System.out.println("removed"+ins.ins_route.routeEnd.start+"
          // "+dataModel.sourceDepot.end+" "+overtime);
          ins.ins_route.remove(ins.node, scenario);
          ins.ins_route.updateEarliest(ins.ins_route.routeStart, scenario);
          ins.ins_route.updateLatest(ins.ins_route.routeEnd, scenario);
        }
      }
    }  // customer

    // account for the current overtime in the route
    recourseCost +=
        Constants.OVERTIME_MULTIPLIER *
        Math.max(evalRoute.routeEnd.start - dataModel.sourceDepot.end, 0);

    dataModel.targetDepot.end = aux;
    rollBackCustomersTW();

    return recourseCost;
  }

  public double evaluateInsertion(Node nod, Node aft, Route rot) {
    return dataModel.getTravelTime(aft.location, nod.location) +
        dataModel.getTravelTime(nod.location, aft.next.location) -
        dataModel.getTravelTime(aft.location, aft.next.location);
  }

  /**
   * @param route the route being evaluated
   * @param next the next customer (node) to be visited
   * @param last the last (current) node visited
   * @param lastArrival time at last node
   * @param acc accumalated penalty for not visiting customers
   * @return
   */
  private double computePath(Route route, Node next, Node last, int lastArrival,
                             double acc) {
    if (next == route.routeEnd) {
      int travelTime = (int)this.scenario.getEdgeWeight(
          this.scenario.getEdge(last.location, next.location));
      double overtime =
          Constants.OVERTIME_MULTIPLIER *
          Math.max((lastArrival + travelTime) - dataModel.sourceDepot.end, 0);
      acc += overtime;
      if (bestTotal >
          acc)  // to keep track of the best set of visited customers...
      {
        bestTotal = acc;
        bestVisited.clear();
        Iterator<Location> iterator = stack.iterator();
        while (iterator.hasNext()) bestVisited.add(iterator.next());
      }
      return overtime;
    }

    // penalty for skipping: cost of a dedicated service
    //(int)dataModel.routingGraph.getEdgeWeight(dataModel.routingGraph.getEdge(dataModel.sourceDepot,
    //next.location))+(int)dataModel.routingGraph.getEdgeWeight(dataModel.routingGraph.getEdge(next.location,
    //dataModel.targetDepot))
    double penalty =
        Constants.UNVISITED_MULTIPLIER * (next.location.customer.penalty);

    int travelTime = (int)this.scenario.getEdgeWeight(
        this.scenario.getEdge(last.location, next.location));
    if (lastArrival + travelTime > next.location.end)  // have to skip
      return computePath(route, next.next, last, lastArrival, acc + penalty) +
          penalty;

    // decide whether to visit or skip
    stack.push(next.location);
    double visit = computePath(
        route, next.next, next,
        Math.max(lastArrival + travelTime, next.location.start), acc);  // visit
    stack.pop();

    double skip =
        computePath(route, next.next, last, lastArrival, acc + penalty) +
        penalty;  // does not visit

    return Math.min(visit, skip);
  }

  class SingleInsertion implements Comparable<SingleInsertion> {
    final double cost;
    final Node node;
    final Node after;
    final Route ins_route;

    public SingleInsertion(Node nod, Node aft, Route rot,
                           Graph<Location, DefaultWeightedEdge> scenario) {
      this.node = nod;
      this.after = aft;
      this.ins_route = rot;
      this.cost = (int)scenario.getEdgeWeight(
                      scenario.getEdge(after.location, node.location)) +
                  (int)scenario.getEdgeWeight(
                      scenario.getEdge(node.location, after.next.location)) -
                  (int)scenario.getEdgeWeight(
                      scenario.getEdge(after.location, after.next.location));
    }

    void execute(Graph<Location, DefaultWeightedEdge> scenario) {
      ins_route.insertNodeAfter(node, after, scenario);
    }

    public String toString() {
      String s = "";
      s += "Insert " + this.node + " after " + this.after + " at cost " +
           this.cost + "\n";
      return s;
    }

    @Override
    public int compareTo(SingleInsertion d) {
      return Double.compare(this.cost, d.cost);
    }
  }
}
