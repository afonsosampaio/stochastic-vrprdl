package rdlp.algorithms.stochastic;

import rdlp.model.RDLPInstance;
import rdlp.model.Location;

import org.jgrapht.Graph;
import org.jgrapht.graph.DefaultWeightedEdge;

import rdlp.algorithms.heuristic.HeuristicSolution;
import rdlp.algorithms.heuristic.Node;
import rdlp.algorithms.heuristic.Route;

import rdlp.util.Constants;

public class SimpleRecourse extends Recourse {
  public SimpleRecourse(RDLPInstance dataModel) { super(dataModel); }

  /**
   * @return the second stage cost (travelled distace + recourse penalty, in
   time unit) for a given first stage solution (@param solution) and realized
   scenario (@param scenario)
 */
  public double evaluateScenario(
      HeuristicSolution solution,
      Graph<Location, DefaultWeightedEdge> scenario) {
    int missed = 0;
    int startTime, travelTime;
    double recourseCost = 0;
    double routeCost = 0;
    double routeOvertime = 0;
    int routeMissed;
    double overtime = 0.0;
    Node last = null;

    availableLocations(scenario);

    for (Route route : solution.routes) {
      if (route.size() == 2)  // skip empty routes
        continue;

      visited.clear();

      routeCost = recourseCost;
      routeMissed = missed;

      startTime = route.routeStart.location.start;
      // NOTE: i starts with the depot!
      for (Node i : route) {
        if (i.next == route.routeEnd) {
          last = i;
          break;
        }
        // System.out.println(i+" "+i.next);
        travelTime = (int)scenario.getEdgeWeight(
            scenario.getEdge(i.location, i.next.location));
        startTime = Math.max(startTime + travelTime, i.next.location.start);
        if (startTime > i.next.location.end) {
          missed++;
          // cost from depot to that location (dedicated vehicle)
          recourseCost += Constants.UNVISITED_MULTIPLIER *
                          (i.next.location.customer.penalty);
          // System.out.println(route.get(i+1));
        } else
          visited.add(i.next.location);
      }  // locations

      travelTime = (int)scenario.getEdgeWeight(
          scenario.getEdge(last.location, route.routeEnd.location));
      startTime += travelTime;
      routeOvertime = Constants.OVERTIME_MULTIPLIER *
                      Math.max(startTime - dataModel.sourceDepot.end, 0);
      overtime += routeOvertime;
      // recourseCost += overtime;

      routeCost = recourseCost - routeCost;
      routeMissed = missed - routeMissed;
      route.recourseCost += (routeCost + routeOvertime);
    }  // routesSAA
    missedCustomers = missed;

    rollBackCustomersTW();
    return recourseCost + overtime;
  }

  /**
   * @return the second stage cost (travelled distace + recourse penalty, in
   time unit) for a given first stage route (@param rt), where @param node is
   visited after @param after, and realized scenario (@param scenario)
 */
  public double evaluateScenario(Route rt, Node node, Node after,
                                 Graph<Location, DefaultWeightedEdge> scenario,
                                 int index) {
    int missed = 0;
    int startTime, travelTime;
    double recourseCost = 0;
    double overtime = 0.0;
    double wait = 0.0;
    Node last = null;

    // System.out.println("Inserting "+node+" after "+after);
    // System.out.println(rt);

    availableLocations(scenario, index);
    startTime = rt.routeStart.location.start;
    for (Node i : rt) {
      if (i.next == rt.routeEnd && i != after) {
        last = i;
        break;
      }

      // split in (after,node) -- (node, after.next)
      if (i == after) {
        // arc (after --> node)
        travelTime = (int)scenario.getEdgeWeight(
            scenario.getEdge(after.location, node.location));
        startTime = Math.max(startTime + travelTime, node.location.start);
        if (startTime > node.location.end) {
          missed++;
          // cost from depot to that location (dedicated vehicle)
          recourseCost +=
              Constants.UNVISITED_MULTIPLIER * (node.location.customer.penalty);
          // System.out.println(route.get(i+1));
        } else {
          visited.add(node.location);
          wait += Math.max(node.location.start - startTime, 0);
        }

        // arc (node,after.next)
        if (i.next == rt.routeEnd) {
          last = node;
          break;
        }

        travelTime = (int)scenario.getEdgeWeight(
            scenario.getEdge(node.location, i.next.location));
        startTime = Math.max(startTime + travelTime, i.next.location.start);
        if (startTime > i.next.location.end) {
          missed++;
          // cost from depot to that location (dedicated vehicle)
          recourseCost += Constants.UNVISITED_MULTIPLIER *
                          (i.next.location.customer.penalty);
          // System.out.println(route.get(i+1));
        } else {
          visited.add(i.next.location);
          wait += Math.max(node.location.start - startTime, 0);
        }
      } else  // arcs not in the insertion
      {
        // System.out.print(i);
        // System.out.println(i+" "+i.next);
        travelTime = (int)scenario.getEdgeWeight(
            scenario.getEdge(i.location, i.next.location));
        startTime = Math.max(startTime + travelTime, i.next.location.start);
        if (startTime > i.next.location.end) {
          missed++;
          // cost from depot to that location (dedicated vehicle)
          recourseCost += Constants.UNVISITED_MULTIPLIER *
                          (i.next.location.customer.penalty);
          // System.out.println(route.get(i+1));
        } else {
          visited.add(i.next.location);
          wait += Math.max(node.location.start - startTime, 0);
        }
      }
    }
    // accounts for overtime
    travelTime = (int)scenario.getEdgeWeight(
        scenario.getEdge(last.location, rt.routeEnd.location));
    startTime += travelTime;

    // System.out.println("\n----------------------------");
    recourseCost += Constants.OVERTIME_MULTIPLIER *
                    Math.max(startTime - dataModel.sourceDepot.end, 0);

    rollBackCustomersTW();
    return recourseCost;
  }
}
