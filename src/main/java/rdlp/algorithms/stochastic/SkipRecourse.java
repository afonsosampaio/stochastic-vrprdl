package rdlp.algorithms.stochastic;

import rdlp.model.RDLPSolution;
import rdlp.model.RDLPInstance;
import rdlp.model.Customer;
import rdlp.model.Location;

import org.jgrapht.Graph;
import org.jgrapht.graph.AsWeightedGraph;
import org.jgrapht.graph.DefaultWeightedEdge;

import rdlp.algorithms.heuristic.HeuristicSolution;
import rdlp.algorithms.heuristic.Node;
import rdlp.algorithms.heuristic.Route;

import rdlp.util.Constants;

import java.util.*;

public class SkipRecourse extends Recourse {
  private Route evalRoute;
  private SkipDPRecourse skipDPRecourse;

  public SkipRecourse(RDLPInstance dataModel) {
    super(dataModel);
    evalRoute =
        new Route(dataModel);  // so, do not allocate memory every iteration...
    //  skipDPRecourse = new SkipDPRecourse(dataModel);
  }

  /**
   * @return the second stage cost (travelled distace + recourse penalty, in
   time unit) for a given first stage solution (@param solution) and realized
   scenario (@param scenario)
 */
  public double evaluateScenario(
      HeuristicSolution solution,
      Graph<Location, DefaultWeightedEdge> scenario) {
    // tau_(i-1) + t_(i-1,i) > L_i ==> skip
    int missed = 0;
    int startTime, travelTime, auxTime;
    boolean skip;
    Node current, next;
    double recourseCost = 0.0;
    int routeMissed;
    double routeCost = 0;
    double routeOvertime = 0;
    double overtime = 0.0;

    availableLocations(scenario);

    // Allow overtime for vehicles returning to the depot.
    int aux = dataModel.targetDepot.end;
    dataModel.targetDepot.end = 999999;  // TODO: not the best solution....

    for (Route route : solution.routes) {
      if (route.size() == 2)  // skip empty routes
        continue;

      startTime = route.routeStart.location.start;
      current = route.routeStart;
      next = route.routeStart.next;

      routeMissed = missed;
      routeCost = recourseCost;

      visited.clear();

      // NOTE next start with the first customer in the route
      while (next != route.routeEnd) {
        travelTime = (int)scenario.getEdgeWeight(
            scenario.getEdge(current.location, next.location));
        auxTime = startTime;
        startTime = Math.max(startTime + travelTime, next.location.start);
        if (startTime > next.location.end) {
          missed++;
          // travel time from depot to customer (dedicated vehicle)
          recourseCost +=
              Constants.UNVISITED_MULTIPLIER * (next.location.customer.penalty);
          startTime = auxTime;
          next = next.next;
        } else {
          visited.add(next.location);
          current = next;
          next = current.next;
        }
      }  // locations
      // get the overtime
      travelTime = (int)scenario.getEdgeWeight(
          scenario.getEdge(current.location, route.routeEnd.location));
      startTime += travelTime;
      routeOvertime = Constants.OVERTIME_MULTIPLIER *
                      Math.max(startTime - dataModel.sourceDepot.end, 0);
      overtime += routeOvertime;
      // recourseCost += overtime;

      routeCost = recourseCost - routeCost;
      routeMissed = missed - routeMissed;
      route.recourseCost += (routeCost + routeOvertime);
    }  // routes
    missedCustomers = missed;
    dataModel.targetDepot.end = aux;

    rollBackCustomersTW();

    // double dp_cost = skipDPRecourse.evaluateScenario(solution, scenario);
    // if(dp_cost > recourseCost+overtime)
    // {
    //   System.out.println(dp_cost+" "+(recourseCost+overtime));
    //   System.exit(1);
    // }

    return recourseCost + overtime;
  }

  /**
   * @return the second stage cost (travelled distace + recourse penalty, in
   time unit) for a given first stage route (@param rt), where @param node is
   visited after @param after, and realized scenario (@param scenario)
 */
  public double evaluateScenario(Route rt, Node node, Node after,
                                 Graph<Location, DefaultWeightedEdge> scenario,
                                 int index) {
    int missed = 0;
    int startTime, travelTime, auxTime;
    double recourseCost = 0;
    double overtime = 0.0;
    Node current, next;
    Node last = null;

    visited.clear();
    startTime = rt.routeStart.location.start;
    current = rt.routeStart;
    next = rt.routeStart.next;

    evalRoute.clear();

    evalRoute.routeEnd.end = dataModel.targetDepot.end;

    Node curr;
    curr = evalRoute.routeStart;
    for (Node i : rt) {
      if (i == rt.routeStart || i == rt.routeEnd) continue;
      if (i == after.next) {
        next = new Node(node.location);
        evalRoute.insertNodeAfter(next, curr, dataModel.routingGraph);
        curr = next;
      }
      next = new Node(i.location);
      evalRoute.insertNodeAfter(next, curr, dataModel.routingGraph);
      curr = next;
    }
    evalRoute.updateEarliest(evalRoute.routeStart, dataModel.routingGraph);
    evalRoute.updateLatest(evalRoute.routeEnd, dataModel.routingGraph);
    // evalRoute should always be feasible (even with original latest time for
    // targetDepot)
    if (evalRoute.isFeasible(dataModel.routingGraph) == false) {
      System.out.println(rt);
      System.out.println(evalRoute);
      evalRoute.isFeasibleVERBOSE(dataModel.routingGraph);
      assert evalRoute.isFeasible(dataModel.routingGraph) == true;
    }

    startTime = evalRoute.routeStart.location.start;
    current = evalRoute.routeStart;
    next = evalRoute.routeStart.next;

    availableLocations(scenario, index);
    int aux = dataModel.targetDepot.end;
    dataModel.targetDepot.end = 999999;  // TODO: not the best solution....
    evalRoute.routeEnd.end = dataModel.targetDepot.end;

    // NOTE next start with the first customer in the route
    while (next != evalRoute.routeEnd) {
      travelTime = (int)scenario.getEdgeWeight(
          scenario.getEdge(current.location, next.location));
      auxTime = startTime;
      startTime = Math.max(startTime + travelTime, next.location.start);
      if (startTime > next.location.end) {
        missed++;
        // System.out.println(next+" missed!" +startTime+"["+next.location.start+" "+next.location.end+"]");
        // travel time from depot to customer (dedicated vehicle)
        recourseCost +=
            Constants.UNVISITED_MULTIPLIER * (next.location.customer.penalty);
        startTime = auxTime;
        next = next.next;
      } else {
        // System.out.println(next+" visited at: "+startTime+"["+next.location.start+" "+next.location.end+"]");
        visited.add(next.location);
        current = next;
        next = current.next;
      }
    }  // locations
    // get the overtime
    travelTime = (int)scenario.getEdgeWeight(
        scenario.getEdge(current.location, evalRoute.routeEnd.location));
    startTime += travelTime;
    recourseCost += Constants.OVERTIME_MULTIPLIER *
                    Math.max(startTime - dataModel.sourceDepot.end, 0);

    rollBackCustomersTW();
    dataModel.targetDepot.end = aux;

    // skipDPRecourse.setTimeWindowsScenarios(this.timeWindowsScenarios,
    // this.checkScenarios); double dp_cost =
    // skipDPRecourse.evaluateScenario(rt,node,after,scenario,index); if(dp_cost
    // > recourseCost)
    // {
    //   System.out.println(dp_cost+" "+recourseCost);
    //   System.exit(1);
    // }

    return recourseCost;
  }
}
