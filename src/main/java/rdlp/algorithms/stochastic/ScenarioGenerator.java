package rdlp.algorithms.stochastic;

import org.jgrapht.Graph;
import org.jgrapht.graph.AsWeightedGraph;
import org.jgrapht.graph.DefaultWeightedEdge;
import org.jgrapht.graph.GraphDelegator;
import rdlp.model.Location;
import rdlp.model.RDLPInstance;

import java.io.Serializable;
import java.util.function.Function;

/*
ISSUES:
1. To generate a scenario, do you compute one random value and multiple all
distances with the same value, or do you generate a new random value per
distance? Currently a new gamma
2. Is this gamme distribution appropriate? What values should we use for shape
and scale? It seems that other papers use normal, log-normal, beta, shifted
gamma and gamma distributions.
*/

/**
 * Generator for random scenarios. A scenario is an instantiation of the
 * distance matrix, where distances between 2 locations are represented as a
 * random variable having some known distribution. Currently distances are drawn
 * from a single gamme distribution, that is, every edge has the same
 * distribution.
 */
public class ScenarioGenerator {
  private final RDLPInstance dataModel;

  public ScenarioGenerator(RDLPInstance dataModel) {
    this.dataModel = dataModel;
  }

  /**
   * Computes a new scenario. A scenario is an instantiation of the distance
   * matrix, where distances between 2 locations are represented as a random
   * variable having some known distribution. Note: each time this method is
   * invoked, a new scenario is generated. The caller must store the result if
   * needed.
   *
   * @return a routing graph with updated distance matrix
   */
  public Graph<Location, DefaultWeightedEdge> getScenario() {  // ByArc
    return new AsWeightedGraph<>(dataModel.routingGraph,
      e-> dataModel.routingGraph.getEdgeWeight(e) + dataModel.arcMapDistribution.get(e).sample(),
      true, false);
  }

  public Graph<Location, DefaultWeightedEdge> getPercentileScenario(double percentile) {
    return new AsWeightedGraph<>(dataModel.routingGraph,
      e-> dataModel.routingGraph.getEdgeWeight(e) + dataModel.arcMapDistribution.get(e).inverseCumulativeProbability(percentile),
      true, false);
  }

  public Graph<Location, DefaultWeightedEdge> getNumericalMean() {
    return new AsWeightedGraph<>(dataModel.routingGraph,
      e->dataModel.routingGraph.getEdgeWeight(e) + dataModel.arcMapDistribution.get(e).getNumericalMean(),
      true, false);
  }
}
