package rdlp.algorithms.stochastic;

import rdlp.model.RDLPInstance;
import rdlp.model.Location;

import org.jgrapht.Graph;
import org.jgrapht.graph.DefaultWeightedEdge;

import rdlp.algorithms.heuristic.HeuristicSolution;
import rdlp.algorithms.heuristic.Node;
import rdlp.algorithms.heuristic.Route;

import rdlp.util.Constants;

import java.util.*;

public class SkipDPRecourseEV extends Recourse {
  private HeuristicSolution solution;
  private Graph<Location, DefaultWeightedEdge> scenario;
  private List<Node> bestNotVisited;
  private double bestTotal;       // keep track of best set of visited customers
  private Stack<Node> stack;  // the path visited in the DP
  private Route evalRoute;
  private Node startDPNode;

  public SkipDPRecourseEV(RDLPInstance dataModel) {
    super(dataModel);
    bestNotVisited = new ArrayList<Node>();
    stack = new Stack<Node>();
    evalRoute =
      new Route(dataModel);  // so, do not allocate memory every iteration...
  }

  /**
   * @return the second stage cost (travelled distace + recourse penalty, in
  time unit) for a given first stage solution (@param solution) and realized
  scenario (@param scenario)
   */
  public double evaluateScenario(HeuristicSolution sol,
                                 Graph<Location, DefaultWeightedEdge> sce) {
    this.solution = sol;
    this.scenario = sce;

    double recourseCost = 0.0;
    double overtime = 0.0;
    int missed = 0;
    int routeMissed;
    int startTime, travelTime, auxTime;
    Node current, next;
    double routeCost = 0;
    double routeOvertime = 0;

    availableLocations(scenario);

    // Allow overtime for vehicles returning to the depot.
    int aux = dataModel.targetDepot.end;
    dataModel.targetDepot.end = 999999;  // TODO: not the best solution....

    for (Route route : solution.routes) {
      if (route.size() == 2)  // skip empty routes
        continue;

      startTime = route.routeStart.location.start;
      current = route.routeStart;
      next = route.routeStart.next;

      routeMissed = missed;
      routeCost = recourseCost;

      visited.clear();

      // NOTE next start with the first customer in the route
      while (next != route.routeEnd) {
        travelTime = (int)scenario.getEdgeWeight(
          scenario.getEdge(current.location, next.location));
        auxTime = startTime;
        startTime = Math.max(startTime + travelTime, next.location.start);
        if (startTime > next.location.end) {
          missed++;
          // travel time from depot to customer (dedicated vehicle)
          recourseCost +=
            Constants.UNVISITED_MULTIPLIER * (next.location.customer.penalty);
          startTime = auxTime;
          next = next.next;
        } else {
          notVisited.clear();
          locsNotVisited.clear();
          bestTotal = 9999999;
          bestNotVisited.clear();

          startDPNode = current;
          double res = computePath(route, next, current, auxTime, 0);

          if (bestNotVisited.contains(next)) {
            missed++;

            recourseCost +=
              Constants.UNVISITED_MULTIPLIER * (next.location.customer.penalty);
            startTime = auxTime;
            next = next.next;
          } else {
            visited.add(next.location);
            current = next;
            next = current.next;
          }
        }
      }  // locations
      // get the overtime
      travelTime = (int)scenario.getEdgeWeight(
        scenario.getEdge(current.location, route.routeEnd.location));
      startTime += travelTime;
      routeOvertime = Constants.OVERTIME_MULTIPLIER *
        Math.max(startTime - dataModel.sourceDepot.end, 0);
      overtime += routeOvertime;
      // recourseCost += overtime;

      routeCost = recourseCost - routeCost;
      routeMissed = missed - routeMissed;
      route.recourseCost += (routeCost + routeOvertime);
    }  // routes

    missedCustomers = missed;
    dataModel.targetDepot.end = aux;

    rollBackCustomersTW();

    return recourseCost + overtime;
  }

  public double evaluateScenario(Route rt, Node node, Node after,
                                 Graph<Location, DefaultWeightedEdge> scenario,
                                 int index) {
    this.scenario = scenario;

    double recourseCost = 0.0;
    int startTime, travelTime, auxTime;
    Node current, next;
    int missed = 0;

    evalRoute.clear();
    evalRoute.routeEnd.end = dataModel.targetDepot.end;

    current = evalRoute.routeStart;
    for (Node i : rt) {
      if (i == rt.routeStart || i == rt.routeEnd) continue;
      if (i == after.next) {
        next = new Node(node.location);
        evalRoute.insertNodeAfter(next, current, dataModel.routingGraph);
        current = next;
      }
      next = new Node(i.location);
      evalRoute.insertNodeAfter(next, current, dataModel.routingGraph);
      current = next;
    }
    evalRoute.updateEarliest(evalRoute.routeStart, dataModel.routingGraph);
    evalRoute.updateLatest(evalRoute.routeEnd, dataModel.routingGraph);
    // evalRoute should always be feasible (even with original latest time for
    // targetDepot)
    if (evalRoute.isFeasible(dataModel.routingGraph) == false) {
      System.out.println(rt);
      System.out.println(evalRoute);
      evalRoute.isFeasibleVERBOSE(dataModel.routingGraph);
      assert evalRoute.isFeasible(dataModel.routingGraph) == true;
    }

    startTime = evalRoute.routeStart.location.start;
    current = evalRoute.routeStart;
    next = evalRoute.routeStart.next;

    availableLocations(scenario, index);

    // Allow overtime for vehicles returning to the depot.
    int aux = dataModel.targetDepot.end;
    dataModel.targetDepot.end = 999999;  // TODO: not the best solution....
    evalRoute.routeEnd.end = dataModel.targetDepot.end;

    while (next != evalRoute.routeEnd) {
      travelTime = (int)scenario.getEdgeWeight(
        scenario.getEdge(current.location, next.location));
      auxTime = startTime;
      startTime = Math.max(startTime + travelTime, next.location.start);
      if (startTime > next.location.end) {
        missed++;
        // travel time from depot to customer (dedicated vehicle)
        recourseCost +=
          Constants.UNVISITED_MULTIPLIER * (next.location.customer.penalty);
        startTime = auxTime;
        next = next.next;
      } else {
        notVisited.clear();
        locsNotVisited.clear();
        bestTotal = 9999999;
        bestNotVisited.clear();

        startDPNode = current;
        double res = computePath(evalRoute, next, current, auxTime, 0);

        if (bestNotVisited.contains(next)) {
          missed++;

          recourseCost +=
            Constants.UNVISITED_MULTIPLIER * (next.location.customer.penalty);
          startTime = auxTime;
          next = next.next;
        } else {
          visited.add(next.location);
          current = next;
          next = current.next;
        }
      }
    }

    // account for the current overtime in the route
    travelTime = (int)scenario.getEdgeWeight(
      scenario.getEdge(next.prev.location, next.location));
    startTime += travelTime;
    recourseCost +=  Constants.OVERTIME_MULTIPLIER *
      Math.max(startTime - dataModel.sourceDepot.end, 0);

    dataModel.targetDepot.end = aux;
    rollBackCustomersTW();

    return recourseCost;
  }

  /**
   * @param route the route being evaluated
   * @param next the next customer (node) to be visited
   * @param last the last (current) node visited
   * @param lastArrival time at last node
   * @param acc accumalated penalty for not visiting customers
   * @return
   */
  private double computePath(Route route, Node next, Node last, int lastArrival,
                             double acc) {
    Graph<Location, DefaultWeightedEdge> sce;
    if (this.startDPNode == last)
      sce = this.scenario;
    else
      sce = dataModel.routingGraphExp;

    if (next == route.routeEnd) {
      int travelTime = (int)sce.getEdgeWeight(sce.getEdge(last.location, next.location));
      double overtime =
        Constants.OVERTIME_MULTIPLIER *
          Math.max((lastArrival + travelTime) - dataModel.sourceDepot.end, 0);
      acc += overtime;
      if (bestTotal > acc)  // to keep track of the best set of visited customers...
      {
        bestTotal = acc;
        bestNotVisited.clear();
        Iterator<Node> iterator = stack.iterator();
        while (iterator.hasNext()) bestNotVisited.add(iterator.next());
      }
      return overtime;
    }

    // penalty for skipping: cost of a dedicated service
    //(int)dataModel.routingGraph.getEdgeWeight(dataModel.routingGraph.getEdge(dataModel.sourceDepot,
    //next.location))+(int)dataModel.routingGraph.getEdgeWeight(dataModel.routingGraph.getEdge(next.location,
    //dataModel.targetDepot))
    double penalty =
      Constants.UNVISITED_MULTIPLIER * (next.location.customer.penalty);

    int travelTime = (int)sce.getEdgeWeight(sce.getEdge(last.location, next.location));
    if (lastArrival + travelTime > next.location.end)  // have to skip
      return computePath(route, next.next, last, lastArrival, acc + penalty) +
        penalty;

    // decide whether to visit or skip
    double visit = computePath(
      route, next.next, next,
      Math.max(lastArrival + travelTime, next.location.start), acc);  // visit

    stack.push(next);
    double skip =
      computePath(route, next.next, last, lastArrival, acc + penalty) + penalty;  // does not visit
    stack.pop();

    return Math.min(visit, skip);
  }

}
