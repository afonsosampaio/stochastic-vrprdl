package rdlp.algorithms.stochastic;

import org.jgrapht.Graph;
import org.jgrapht.graph.DefaultWeightedEdge;
import rdlp.algorithms.heuristic.HeuristicSolution;
import rdlp.algorithms.heuristic.Node;
import rdlp.algorithms.heuristic.Route;
import rdlp.model.Location;
import rdlp.model.RDLPInstance;
import rdlp.util.Constants;

import java.util.*;

public class RevisitDPRecourseEV extends Recourse {
  private HeuristicSolution solution;
  private Graph<Location, DefaultWeightedEdge> scenario;

  private Queue<Route.SingleInsertion> insertionCandidates;
  Map<Location, Node> locationToNodeMap = new HashMap<>();
  private Set<Location> rescheduledSet = new HashSet<>();

  // to keep track of best solution in the DP recursion
  private List<Node> bestNotVisited;
  private double bestTotal;       // keep track of best set of visited customers
  private Stack<Node> stack;  // the path visited in the DP
  private Route evalRoute;
  private Node startDPNode;

  public RevisitDPRecourseEV(RDLPInstance dataModel) {
    super(dataModel);
    insertionCandidates = new PriorityQueue<>(100, Collections.reverseOrder());

    bestNotVisited = new ArrayList<Node>();
    stack = new Stack<Node>();

    for (Location loc : dataModel.routingGraph.vertexSet())
      locationToNodeMap.put(loc, new Node(loc));

    evalRoute =
      new Route(dataModel);  // so, do not allocate memory every iteration...
  }

  /**
   * @return the second stage cost (travelled distance + recourse penalty, in
   * time unit) for a given first stage solution (@param solution) and realized
   * scenario (@param scenario)
   */
  public double evaluateScenario(HeuristicSolution sol,
                                 Graph<Location, DefaultWeightedEdge> sce) {

    this.solution = sol;
    this.scenario = sce;

    int missed = 0;  // total missed deliveries, considering all routes
    int rescheduled = 0; //total rescheduled deliveries
    int startTime, travelTime, auxTime;
    boolean skip;
    Node current, next;
    double recourseCost = 0.0;
    double overtime = 0.0;

    int routeMissed;
    int routeRescheduled;
    double routeCost = 0;
    double routeOvertime = 0;

    availableLocations(scenario);

    // Allow overtime for vehicles returning to the depot.
    int aux = dataModel.targetDepot.end;
    dataModel.targetDepot.end = 999999;  // TODO: not the best solution....

    for (Route route : solution.routes) {
      if (route.size() == 2)  // skip empty routes
        continue;

      visited.clear();
      notVisited.clear();
      locsNotVisited.clear();

      routeMissed = missed;
      routeCost = recourseCost;

      routeRescheduled = rescheduled;
      rescheduledSet.clear();

      Route workingCopy = route.deepCopy();
//      System.out.println("Evaluating: ");
//      System.out.println(route.toString());
//      System.out.println("Copying: ");
//      System.out.println(workingCopy);

      startTime = workingCopy.routeStart.location.start;
      current = workingCopy.routeStart;
      next = workingCopy.routeStart.next;

      while (next != workingCopy.routeEnd) {
        travelTime = (int)scenario.getEdgeWeight(
          scenario.getEdge(current.location, next.location));
        auxTime = startTime;
        startTime = Math.max(startTime + travelTime, next.location.start);

        notVisited.clear();
        locsNotVisited.clear();
        bestTotal = 9999999;
        bestNotVisited.clear();

        if (startTime <= next.location.end) {
          startDPNode = current;
          double res = computePath(workingCopy, next, current, auxTime, 0);

          if (!bestNotVisited.contains(next)) {
            visited.add(next.location);
            current = next;
            next = current.next;
          }
        }
        if (startTime > next.location.end || bestNotVisited.contains(next)) {
          workingCopy.remove(next, dataModel.routingGraph);
          workingCopy.updateEarliest(current, dataModel.routingGraph);
          workingCopy.updateLatest(current, dataModel.routingGraph);
          boolean updatedRoute = workingCopy.isNewLocationAfter(next.location, current, auxTime,
                                                locationToNodeMap, scenario, insertionCandidates);
          if (updatedRoute) {
            ++rescheduled;
            rescheduledSet.add(next.location);
          } else {
            missed++;
            // travel time from depot to customer (dedicated vehicle)
            recourseCost +=
              Constants.UNVISITED_MULTIPLIER * (next.location.customer.penalty);

            if (rescheduledSet.contains(next.location))
              --rescheduled;
          }
          next = current.next;
          travelTime = (int)scenario.getEdgeWeight(
            scenario.getEdge(current.location, next.location));
          startTime = Math.max(auxTime + travelTime, next.location.start);
          current = current.next;
          if (next != workingCopy.routeEnd)
            next = current.next;
        }
      }  // locations

      // account for the current overtime in the route
      travelTime = (int)scenario.getEdgeWeight(
        scenario.getEdge(next.prev.location, next.location));
      startTime += travelTime;
      routeOvertime =  Constants.OVERTIME_MULTIPLIER *
        Math.max(startTime - dataModel.sourceDepot.end, 0);
      overtime += routeOvertime;
      routeCost = recourseCost - routeCost;
      routeMissed = missed - routeMissed;
      routeRescheduled = rescheduled - routeRescheduled;
      route.recourseCost += (routeCost + routeOvertime);
    } // routes

    this.missedCustomers = missed;
    this.rescheduledCustomers = rescheduled;
    dataModel.targetDepot.end = aux;

    rollBackCustomersTW();

    return recourseCost + overtime;
  }

  public double evaluateScenario(Route rt, Node node, Node after,
                                 Graph<Location, DefaultWeightedEdge> sce,
                                 int index) {
    this.scenario = sce;

    double recourseCost = 0.0;
    int startTime, travelTime, auxTime;
    Node current, next;
    int missed = 0;
    int rescheduled = 0;

    // create the route with node visited after after
    evalRoute.clear();
    evalRoute.routeEnd.end = dataModel.targetDepot.end;

    current = evalRoute.routeStart;
    for (Node i : rt) {
      if (i == rt.routeStart || i == rt.routeEnd) continue;
      if (i == after.next) {
        next = new Node(node.location);
        evalRoute.insertNodeAfter(next, current, dataModel.routingGraph);
        current = next;
      }
      next = new Node(i.location);
      evalRoute.insertNodeAfter(next, current, dataModel.routingGraph);
      current = next;
    }
    evalRoute.updateEarliest(evalRoute.routeStart, dataModel.routingGraph);
    evalRoute.updateLatest(evalRoute.routeEnd, dataModel.routingGraph);
    if (evalRoute.isFeasible(dataModel.routingGraph) == false) {
      System.out.println(rt);
      System.out.println(evalRoute);
      evalRoute.isFeasibleVERBOSE(dataModel.routingGraph);
      assert evalRoute.isFeasible(dataModel.routingGraph) == true;
    }

    startTime = evalRoute.routeStart.location.start;
    current = evalRoute.routeStart;
    next = evalRoute.routeStart.next;

    availableLocations(scenario, index);

    // Allow overtime for vehicles returning to the depot.
    int aux = dataModel.targetDepot.end;
    dataModel.targetDepot.end = 999999;  // TODO: not the best solution....
    evalRoute.routeEnd.end = dataModel.targetDepot.end;

    while (next != evalRoute.routeEnd) {
      travelTime = (int)scenario.getEdgeWeight(
        scenario.getEdge(current.location, next.location));
      auxTime = startTime;
      startTime = Math.max(startTime + travelTime, next.location.start);

      notVisited.clear();
      locsNotVisited.clear();
      bestTotal = 9999999;
      bestNotVisited.clear();

      if (startTime <= next.location.end) {
        startDPNode = current;
        double res = computePath(evalRoute, next, current, auxTime, 0);

        if (!bestNotVisited.contains(next)) {
          visited.add(next.location);
          current = next;
          next = current.next;
        }
      }
      if (startTime > next.location.end || bestNotVisited.contains(next)) {
        evalRoute.remove(next, dataModel.routingGraph);
        evalRoute.updateEarliest(current, dataModel.routingGraph);
        evalRoute.updateLatest(current, dataModel.routingGraph);
        boolean updatedRoute = evalRoute.isNewLocationAfter(next.location, current, auxTime,
          locationToNodeMap, scenario, insertionCandidates);
        if (updatedRoute) {
          ++rescheduled;
          rescheduledSet.add(next.location);
        } else {
          missed++;
          // travel time from depot to customer (dedicated vehicle)
          recourseCost +=
            Constants.UNVISITED_MULTIPLIER * (next.location.customer.penalty);

          if (rescheduledSet.contains(next.location))
            --rescheduled;
        }
        next = current.next;
        travelTime = (int)scenario.getEdgeWeight(
          scenario.getEdge(current.location, next.location));
        startTime = Math.max(auxTime + travelTime, next.location.start);
        current = current.next;
        if (next != evalRoute.routeEnd)
          next = current.next;
      }
    }  // locations

    // account for the current overtime in the route
    travelTime = (int)scenario.getEdgeWeight(
      scenario.getEdge(next.prev.location, next.location));
    startTime += travelTime;
    recourseCost +=  Constants.OVERTIME_MULTIPLIER *
      Math.max(startTime - dataModel.sourceDepot.end, 0);

    dataModel.targetDepot.end = aux;
    rollBackCustomersTW();

    return recourseCost;
  }

  /**
   * @param route the route being evaluated
   * @param next the next customer (node) to be visited
   * @param last the last (current) node visited
   * @param lastArrival time at last node
   * @param acc accumalated penalty for not visiting customers
   * @return
   */
  private double computePath(Route route, Node next, Node last, int lastArrival,
                             double acc) {
    Graph<Location, DefaultWeightedEdge> sce;
    if (this.startDPNode == last)
      sce = this.scenario;
    else
      sce = dataModel.routingGraphExp;

    if (next == route.routeEnd) {
      int travelTime = (int)sce.getEdgeWeight(sce.getEdge(last.location, next.location));
      double overtime =
        Constants.OVERTIME_MULTIPLIER *
          Math.max((lastArrival + travelTime) - dataModel.sourceDepot.end, 0);
      acc += overtime;
      if (bestTotal >
        acc)  // to keep track of the best set of visited customers...
      {
        bestTotal = acc;
        bestNotVisited.clear();
        Iterator<Node> iterator = stack.iterator();
        while (iterator.hasNext()) bestNotVisited.add(iterator.next());
      }
      return overtime;
    }

    // penalty for skipping: cost of a dedicated service
    //(int)dataModel.routingGraph.getEdgeWeight(dataModel.routingGraph.getEdge(dataModel.sourceDepot,
    //next.location))+(int)dataModel.routingGraph.getEdgeWeight(dataModel.routingGraph.getEdge(next.location,
    //dataModel.targetDepot))
    double penalty =
      Constants.UNVISITED_MULTIPLIER * (next.location.customer.penalty);

    int travelTime = (int)sce.getEdgeWeight(sce.getEdge(last.location, next.location));
    if (lastArrival + travelTime > next.location.end)  // have to skip
      return computePath(route, next.next, last, lastArrival, acc + penalty) +
        penalty;

    // decide whether to visit or skip
    double visit = computePath(
      route, next.next, next,
      Math.max(lastArrival + travelTime, next.location.start), acc);  // visit

    stack.push(next);
    double skip =
      computePath(route, next.next, last, lastArrival, acc + penalty) + penalty;  // does not visit
    stack.pop();

    return Math.min(visit, skip);
  }

}