package rdlp.algorithms.stochastic;

import rdlp.model.RDLPSolution;
import rdlp.model.RDLPInstance;
import rdlp.model.Customer;
import rdlp.model.Location;

import org.jgrapht.Graph;
import org.jgrapht.graph.AsWeightedGraph;
import org.jgrapht.graph.DefaultWeightedEdge;

import rdlp.algorithms.heuristic.HeuristicSolution;
import rdlp.algorithms.heuristic.Node;
import rdlp.algorithms.heuristic.Route;

import rdlp.util.Constants;

import java.util.*;

public class SkipDPRecourse extends Recourse {
  private HeuristicSolution solution;
  private Graph<Location, DefaultWeightedEdge> scenario;
  private List<Location> bestVisited;
  private double bestTotal;       // keep track of best set of visited customers
  private Stack<Location> stack;  // the path visited in the DP
  private Route evalRoute;
  private SkipRecourse skipRecourse;

  public SkipDPRecourse(RDLPInstance dataModel) {
    super(dataModel);
    bestVisited = new ArrayList<Location>();
    stack = new Stack<Location>();
    evalRoute =
        new Route(dataModel);  // so, do not allocate memory every iteration...

    skipRecourse = new SkipRecourse(dataModel);
  }

  /**
   * @return the second stage cost (travelled distace + recourse penalty, in
   time unit) for a given first stage solution (@param solution) and realized
   scenario (@param scenario)
 */
  public double evaluateScenario(HeuristicSolution sol,
                                 Graph<Location, DefaultWeightedEdge> sce) {
    this.solution = sol;
    this.scenario = sce;
    double recourseCost = 0.0;
    double overtime = 0.0;
    // double routeCost = 0;
    // double routeOvertime = 0;
    int missed = 0;

    availableLocations(scenario);

    // Allow overtime for vehicles returning to the depot.
    int aux = dataModel.targetDepot.end;
    dataModel.targetDepot.end = 999999;  // TODO: not the best solution....

    for (Route route : this.solution.routes) {
      if (route.size() == 2)  // skip empty routes
        continue;

      notVisited.clear();
      visited.clear();
      bestTotal = 9999999;
      bestVisited.clear();

      double res = computePath(route, route.routeStart.next, route.routeStart,
                               route.routeStart.location.start, 0);
      recourseCost += res;

      // the actual implemented route
      Route new_route = new Route(dataModel);
      Node nxt, curr;
      curr = new_route.routeStart;
      for (Location loc : bestVisited) {
        nxt = new Node(loc);
        new_route.insertNodeAfter(nxt, curr, scenario);
        curr = nxt;
      }
      new_route.updateEarliest(new_route.routeStart, scenario);
      new_route.updateLatest(new_route.routeEnd, scenario);
      if (new_route.isFeasible(scenario) ==
          false)  // should always be with 999999 as latest arrival at depot
      {
        System.out.println(route);
        System.out.println(new_route);
        System.out.println(bestVisited);
        System.out.println("-------------------------");
        assert new_route.isFeasible(scenario) == true;
      }

      // overtime penalty (already computed on DP solution)
      // routeOvertime =
      // Constants.OVERTIME_MULTIPLIER*Math.max(new_route.routeEnd.start -
      // dataModel.sourceDepot.end, 0);
      route.recourseCost += res;  // + routeOvertime);
      missed += route.size() - new_route.size();
    }
    missedCustomers = missed;

    rollBackCustomersTW();

    dataModel.targetDepot.end = aux;

    // recourseCost = skipRecourse.evaluateScenario(sol, sce);

    return recourseCost;  // + overtime;
  }

  public double evaluateScenario(Route rt, Node node, Node after,
                                 Graph<Location, DefaultWeightedEdge> scenario,
                                 int index) {
    this.scenario = scenario;

    double recourseCost = 0.0;

    evalRoute.clear();

    notVisited.clear();
    visited.clear();
    bestTotal = 9999999;
    bestVisited.clear();

    evalRoute.routeEnd.end = dataModel.targetDepot.end;

    Node next, curr;
    curr = evalRoute.routeStart;
    for (Node i : rt) {
      if (i == rt.routeStart || i == rt.routeEnd) continue;
      if (i == after.next) {
        next = new Node(node.location);
        evalRoute.insertNodeAfter(next, curr, dataModel.routingGraph);
        curr = next;
      }
      next = new Node(i.location);
      evalRoute.insertNodeAfter(next, curr, dataModel.routingGraph);
      curr = next;
    }
    evalRoute.updateEarliest(evalRoute.routeStart, dataModel.routingGraph);
    evalRoute.updateLatest(evalRoute.routeEnd, dataModel.routingGraph);
    // evalRoute should always be feasible (even with original latest time for
    // targetDepot)
    if (evalRoute.isFeasible(dataModel.routingGraph) == false) {
      System.out.println(rt);
      System.out.println(evalRoute);
      evalRoute.isFeasibleVERBOSE(dataModel.routingGraph);
      assert evalRoute.isFeasible(dataModel.routingGraph) == true;
    }

    availableLocations(scenario, index);

    int aux = dataModel.targetDepot.end;
    dataModel.targetDepot.end = 999999;  // TODO: not the best solution....
    evalRoute.routeEnd.end = dataModel.targetDepot.end;

    // now evaluate the scenario on the new route, for which node @node is
    // visited after node @after
    recourseCost =
        computePath(evalRoute, evalRoute.routeStart.next, evalRoute.routeStart,
                    evalRoute.routeStart.location.start, 0);

    // the actual implemented route
    // evalRoute.clear();
    // curr = evalRoute.routeStart;
    // for(Location loc : bestVisited)
    // {
    //   next = new Node(loc);
    //   evalRoute.insertNodeAfter(next, curr, scenario);
    //   curr = next;
    // }
    // evalRoute.updateEarliest(evalRoute.routeStart, scenario);
    // evalRoute.updateLatest(evalRoute.routeEnd, scenario);
    // //evalRoute should always be with 999999 as latest arrival at depot
    // //NOTE: evalRoute.isFeasible(scenario) will return false, since
    // evalRoute.end
    // //is created with targetDepot considering the orignal end time.
    //
    // recourseCost +=
    // Constants.OVERTIME_MULTIPLIER*Math.max(evalRoute.routeEnd.start -
    // dataModel.sourceDepot.end, 0);

    rollBackCustomersTW();

    dataModel.targetDepot.end = aux;

    // skipRecourse.setTimeWindowsScenarios(this.timeWindowsScenarios,
    // this.checkScenarios); double gr_cost =
    // skipRecourse.evaluateScenario(rt,node,after,scenario,index); if(gr_cost <
    // recourseCost)
    // {
    //   System.out.println(gr_cost+" "+recourseCost);
    //   System.out.println(evalRoute);
    //   System.out.println("--------------");
    //   gr_cost = skipRecourse.evaluateScenarioV(rt,node,after,scenario,index);
    //   System.exit(1);
    // }
    // recourseCost = gr_cost;

    return recourseCost;
  }

  /**
   * @param route the route being evaluated
   * @param next the next customer (node) to be visited
   * @param last the last (current) node visited
   * @param lastArrival time at last node
   * @param acc accumalated penalty for not visiting customers
   * @return
   */
  private double computePath(Route route, Node next, Node last, int lastArrival,
                             double acc) {
    if (next == route.routeEnd) {
      int travelTime = (int)this.scenario.getEdgeWeight(
          this.scenario.getEdge(last.location, next.location));
      double overtime =
          Constants.OVERTIME_MULTIPLIER *
          Math.max((lastArrival + travelTime) - dataModel.sourceDepot.end, 0);
      acc += overtime;
      // System.out.println(acc);
      if (bestTotal >
          acc)  // to keep track of the best set of visited customers...
      {
        bestTotal = acc;
        bestVisited.clear();
        Iterator<Location> iterator = stack.iterator();
        while (iterator.hasNext()) bestVisited.add(iterator.next());
      }
      return overtime;
    }

    // penalty for skipping: cost of a dedicated service
    double penalty =
        Constants.UNVISITED_MULTIPLIER * (next.location.customer.penalty);

    int travelTime = (int)this.scenario.getEdgeWeight(
        this.scenario.getEdge(last.location, next.location));
    if (lastArrival + travelTime > next.location.end)  // have to skip
      return computePath(route, next.next, last, lastArrival, acc + penalty) +
          penalty;

    // decide whether to visit or skip
    stack.push(next.location);
    double visit = computePath(
        route, next.next, next,
        Math.max(lastArrival + travelTime, next.location.start), acc);  // visit

    stack.pop();
    double skip =
        computePath(route, next.next, last, lastArrival, acc + penalty) +
        penalty;  // does not visit

    return Math.min(visit, skip);
  }
}
