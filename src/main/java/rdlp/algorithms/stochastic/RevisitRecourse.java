package rdlp.algorithms.stochastic;

import rdlp.model.RDLPSolution;
import rdlp.model.RDLPInstance;
import rdlp.model.Customer;
import rdlp.model.Location;

import org.jgrapht.Graph;
import org.jgrapht.graph.AsWeightedGraph;
import org.jgrapht.graph.DefaultWeightedEdge;

import rdlp.algorithms.heuristic.HeuristicSolution;
import rdlp.algorithms.heuristic.Node;
import rdlp.algorithms.heuristic.Route;

import rdlp.util.Constants;

import java.util.*;

public class RevisitRecourse extends Recourse {
  private Queue<SingleInsertion> insertionCandidates;
  Map<Location, Node> locationToNodeMap = new HashMap<>();
  private Route evalRoute;

  public RevisitRecourse(RDLPInstance dataModel) {
    super(dataModel);
    insertionCandidates = new PriorityQueue<>(100, Collections.reverseOrder());

    for (Location loc : dataModel.routingGraph.vertexSet())
      locationToNodeMap.put(loc, new Node(loc));

    evalRoute =
        new Route(dataModel);  // so, do not allocate memory every iteration...
  }

  /**
   * @return the second stage cost (travelled distance + recourse penalty, in
   time unit) for a given first stage solution (@param solution) and realized
   scenario (@param scenario)
 */
  public double evaluateScenario(
      HeuristicSolution solution,
      Graph<Location, DefaultWeightedEdge> scenario) {
    // tau_(i-1) + t_(i-1,i) > L_i ==> skip
    int missed = 0;  // total missed deliveries, considering all routes
    int startTime, travelTime, auxTime;
    boolean skip;
    Node current, next;
    double recourseCost = 0.0;
    double overtime = 0.0;

    int routeMissed;
    double routeCost = 0;
    double routeOvertime = 0;

    availableLocations(scenario);

    // Allow overtime for vehicles returning to the depot.
    int aux = dataModel.targetDepot.end;
    dataModel.targetDepot.end = 999999;  // TODO: not the best solution....

    for (Route route : solution.routes) {
      if (route.size() == 2)  // skip empty routes
        continue;

      startTime = route.routeStart.location.start;
      current = route.routeStart;
      next = route.routeStart.next;
      visited.clear();
      notVisited.clear();
      locsNotVisited.clear();

      routeMissed = missed;
      routeCost = recourseCost;

      while (next != route.routeEnd) {
        travelTime = (int)scenario.getEdgeWeight(
            scenario.getEdge(current.location, next.location));
        auxTime = startTime;
        startTime = Math.max(startTime + travelTime, next.location.start);
        if (startTime > next.location.end) {
          missed++;
          // dedicated vehicle for servicing the customer
          recourseCost +=
              Constants.UNVISITED_MULTIPLIER * (next.location.customer.penalty);
          // Constants.UNVISITED_MULTIPLIER*((int)dataModel.routingGraph.getEdgeWeight(dataModel.routingGraph.getEdge(dataModel.sourceDepot,
          // next.location))
          //+(int)dataModel.routingGraph.getEdgeWeight(dataModel.routingGraph.getEdge(next.location,
          //dataModel.targetDepot)));
          notVisited.add(next.location.customer);
          locsNotVisited.add(next.location);
          startTime = auxTime;
          next = next.next;
          // System.out.println(route.get(i+1));
        } else {
          visited.add(next.location);
          current = next;
          next = current.next;
        }
      }  // locations

      // try to re-insert not visited customers into the route, visiting a
      // different location
      if (notVisited.size() == 0) continue;

      Route new_route = new Route(dataModel);
      Node curr = new_route.routeStart;
      Node nxt;
      for (Location loc : visited) {
        nxt = new Node(loc);
        new_route.insertNodeAfter(nxt, curr, scenario);
        curr = nxt;
      }
      new_route.updateEarliest(new_route.routeStart, scenario);
      new_route.updateLatest(new_route.routeEnd, scenario);
      if (new_route.isFeasible(scenario) ==
          false)  // should always be with 999999 as latest arrival at depot
      {
        System.out.println(route);
        System.out.println(new_route);
        assert new_route.isFeasible(scenario) == true;
      }

      double travel1, travel2;
      int earliest, latest;
      int after_earliest, before_latest;
      double delta;
      Node node;
      SingleInsertion ins;
      Customer cust;
      Location Cloc;

      // for(Customer cust : notVisited)
      for (int cc = 0; cc < notVisited.size(); ++cc) {
        cust = notVisited.get(cc);
        Cloc = locsNotVisited.get(cc);
        insertionCandidates.clear();
        // find an insertion for each location associated with the customer
        for (Location loc : cust.locations) {
          node = locationToNodeMap.get(loc);
          if (node.location.start <= 0 &&
              node.location.end <= 0)  // location is not available
            continue;
          for (Node after : new_route) {
            if (after == new_route.routeEnd) break;
            travel1 = (int)scenario.getEdgeWeight(
                scenario.getEdge(after.location, node.location));
            travel2 = (int)scenario.getEdgeWeight(
                scenario.getEdge(node.location, after.next.location));
            after_earliest = after.start;
            before_latest = after.next.end;
            earliest =
                Math.max(node.location.start, (int)(after_earliest + travel1));
            latest =
                Math.min(node.location.end, (int)(before_latest - travel2));

            if (earliest <= latest)  // feasible?
            {
              if (insertionCandidates.size() >= 10)  // keep oonly the best
              {
                delta = this.evaluateInsertion(node, after, new_route);
                if (insertionCandidates.peek().cost > delta)
                  insertionCandidates.poll(); // remove the worst one
              }
              // System.out.println(node+" "+after+" "+after.end);
              insertionCandidates.add(
                  new SingleInsertion(node, after, new_route, scenario));
            }
          }
        }  // location

        if (insertionCandidates.peek() != null)  // there is a feasible insertion!
        {
          ins = insertionCandidates.poll();
          // overtime before insertion
          int aux_overTime = Math.max(
              ins.ins_route.routeEnd.start - dataModel.sourceDepot.end, 0);
          ins.execute(scenario);
          ins.ins_route.updateEarliest(ins.node, scenario);
          ins.ins_route.updateLatest(ins.node, scenario);

          // check the cost of re-inserting the customer
          // assess whether there's is overtime and if it compensantes the cost
          // of a dedicated service
          int overtime_aux =
              Math.max(ins.ins_route.routeEnd.start - dataModel.sourceDepot.end,
                       0);  // TODO: targetDepot was modified
          // only account for the overtime due to the insertion off the
          // customer? NOTE: overtime >= aux_overTime
          // overtime -= aux_overTime;

          // compare with the total overtime in the route (not only the overtime
          // due to the insertion)
          // scenario.getEdgeWeight(scenario.getEdge(dataModel.sourceDepot,
          // Cloc)) + scenario.getEdgeWeight(scenario.getEdge(Cloc,
          // dataModel.targetDepot))
          if (overtime_aux <= Constants.MAX_OVERTIME &&
              Constants.OVERTIME_MULTIPLIER * overtime_aux + ins.cost <
                  Constants.UNVISITED_MULTIPLIER * (Cloc.customer.penalty)) {
            // System.out.println("inserted"+ins.ins_route.routeEnd.start+"
            // "+dataModel.sourceDepot.end+" "+overtime);
            missed--;
            // remove cost o dedicated service
            recourseCost -=
                Constants.UNVISITED_MULTIPLIER * (Cloc.customer.penalty);
            // dataModel.routingGraph.getEdgeWeight(dataModel.routingGraph.getEdge(dataModel.sourceDepot,
            // Cloc))+
            // dataModel.routingGraph.getEdgeWeight(dataModel.routingGraph.getEdge(Cloc,
            // dataModel.targetDepot))
          } else  // remove the (re)inserted customer. more profitable to be
                  // serviced by dedicated vehicle
          {
            // System.out.println("removed"+ins.ins_route.routeEnd.start+"
            // "+dataModel.sourceDepot.end+" "+overtime);
            ins.ins_route.remove(ins.node, scenario);
            ins.ins_route.updateEarliest(ins.ins_route.routeStart, scenario);
            ins.ins_route.updateLatest(ins.ins_route.routeEnd, scenario);
          }
        }
      }  // customer

      // account for the current overtime in the route
      routeOvertime =
          Constants.OVERTIME_MULTIPLIER *
          Math.max(new_route.routeEnd.start - dataModel.sourceDepot.end, 0);
      overtime += routeOvertime;

      routeCost = recourseCost - routeCost;
      routeMissed = missed - routeMissed;
      route.recourseCost += (routeCost + routeOvertime);
      // account for the current overtime in the route
      // recourseCost +=
      // Constants.OVERTIME_MULTIPLIER*Math.max(new_route.routeEnd.start -
      // dataModel.sourceDepot.end, 0);
    }  // routes
    missedCustomers = missed;
    dataModel.targetDepot.end = aux;

    rollBackCustomersTW();

    return recourseCost + overtime;
  }

  public double evaluateScenario(Route rt, Node node, Node after,
                                 Graph<Location, DefaultWeightedEdge> scenario,
                                 int index) {
    double recourseCost = 0.0;
    int startTime, travelTime, auxTime;
    boolean skip;
    Node current, next;

    // create the route with node visited after after
    evalRoute.clear();
    evalRoute.routeEnd.end = dataModel.targetDepot.end;

    current = evalRoute.routeStart;
    for (Node i : rt) {
      if (i == rt.routeStart || i == rt.routeEnd) continue;
      if (i == after.next) {
        next = new Node(node.location);
        evalRoute.insertNodeAfter(next, current, dataModel.routingGraph);
        current = next;
      }
      next = new Node(i.location);
      evalRoute.insertNodeAfter(next, current, dataModel.routingGraph);
      current = next;
    }
    // evalRoute should always be feasible (even with original latest time for
    // targetDepot)
    if (evalRoute.isFeasible(dataModel.routingGraph) == false) {
      System.out.println("Inserting: " + node + "[" + node.location.start +
                         "," + node.location.end + "] after " + after + "[" +
                         after.location.start + "," + after.location.end);
      System.out.println(rt);
      System.out.println(evalRoute);
      evalRoute.isFeasibleVERBOSE(dataModel.routingGraph);
      rt.insertNodeAfter(node, after, dataModel.routingGraph);
      System.out.println(rt);
      rt.updateEarliest(rt.routeStart, dataModel.routingGraph);
      rt.updateLatest(rt.routeEnd, dataModel.routingGraph);
      System.out.println(rt);
      assert evalRoute.isFeasible(dataModel.routingGraph) == true;
    }

    availableLocations(scenario, index);
    int aux = dataModel.targetDepot.end;
    dataModel.targetDepot.end = 999999;  // TODO: not the best solution....
    evalRoute.routeEnd.end = dataModel.targetDepot.end;

    //*********************************
    startTime = evalRoute.routeStart.location.start;
    current = evalRoute.routeStart;
    next = evalRoute.routeStart.next;
    visited.clear();
    notVisited.clear();
    locsNotVisited.clear();

    while (next != evalRoute.routeEnd) {
      travelTime = (int)scenario.getEdgeWeight(
          scenario.getEdge(current.location, next.location));
      auxTime = startTime;
      startTime = Math.max(startTime + travelTime, next.location.start);
      if (startTime > next.location.end) {
        // dedicated vehicle for servicing the customer
        recourseCost +=
            Constants.UNVISITED_MULTIPLIER * (next.location.customer.penalty);
        //(int)dataModel.routingGraph.getEdgeWeight(dataModel.routingGraph.getEdge(dataModel.sourceDepot,
        //next.location))+(int)dataModel.routingGraph.getEdgeWeight(dataModel.routingGraph.getEdge(next.location,
        //dataModel.targetDepot))
        notVisited.add(next.location.customer);
        locsNotVisited.add(next.location);
        startTime = auxTime;
        next = next.next;
        // System.out.println(route.get(i+1));
      } else {
        visited.add(next.location);
        current = next;
        next = current.next;
      }
    }  // locations

    if (notVisited.size() == 0)  // only overtime
    {
      dataModel.targetDepot.end = aux;
      rollBackCustomersTW();
      return Constants.OVERTIME_MULTIPLIER *
          Math.max(evalRoute.routeEnd.start - dataModel.sourceDepot.end, 0);
    }

    // try to re-insert not visited customers into the route, visiting a
    // different location
    Route new_route = new Route(dataModel);
    Node curr = new_route.routeStart;
    Node nxt;
    for (Location loc : visited) {
      nxt = new Node(loc);
      new_route.insertNodeAfter(nxt, curr, scenario);
      curr = nxt;
    }
    new_route.updateEarliest(new_route.routeStart, scenario);
    new_route.updateLatest(new_route.routeEnd, scenario);
    if (new_route.isFeasible(scenario) ==
        false)  // should always be with 999999 as latest arrival at depot
    {
      System.out.println("" + node + " after " + after);
      System.out.println(rt);
      System.out.println(evalRoute);
      System.out.println(new_route);
      assert new_route.isFeasible(scenario) == true;
    }

    double travel1, travel2;
    int earliest, latest;
    int after_earliest, before_latest;
    double delta;
    SingleInsertion ins;
    Customer cust;
    Location Cloc;

    // for(Customer cust : notVisited)
    for (int cc = 0; cc < notVisited.size(); ++cc) {
      cust = notVisited.get(cc);
      Cloc = locsNotVisited.get(cc);
      insertionCandidates.clear();
      // find an insertion for each location associated with the customer
      for (Location loc : cust.locations) {
        node = locationToNodeMap.get(loc);
        if (node.location.start <= 0 &&
            node.location.end <= 0)  // location is not available
          continue;
        for (Node after_ : new_route) {
          if (after_ == new_route.routeEnd) break;
          travel1 = (int)scenario.getEdgeWeight(
              scenario.getEdge(after_.location, node.location));
          travel2 = (int)scenario.getEdgeWeight(
              scenario.getEdge(node.location, after_.next.location));
          after_earliest = after_.start;
          before_latest = after_.next.end;
          earliest =
              Math.max(node.location.start, (int)(after_earliest + travel1));
          latest = Math.min(node.location.end, (int)(before_latest - travel2));

          if (earliest <= latest)  // feasible?
          {
            if (insertionCandidates.size() >= 10)  // keep oonly the best
            {
              delta = this.evaluateInsertion(node, after_, new_route);
              if (insertionCandidates.peek().cost >
                  delta)  // remove the worst one
                insertionCandidates.poll();
            }
            // System.out.println(node+" "+after+" "+after.end);
            insertionCandidates.add(
                new SingleInsertion(node, after_, new_route, scenario));
          }
        }
      }  // location

      if (insertionCandidates.peek() != null)  // there is a feasible insertion!
      {
        ins = insertionCandidates.poll();
        int aux_overTime =
            Math.max(ins.ins_route.routeEnd.start - dataModel.sourceDepot.end,
                     0);  // overtime before insertion
        ins.execute(scenario);
        ins.ins_route.updateEarliest(ins.node, scenario);
        ins.ins_route.updateLatest(ins.node, scenario);

        // check the cost of re-inserting the customer
        // assess whether there's is overtime and if it compensantes the cost of
        // a dedicated service
        int overtime =
            Math.max(ins.ins_route.routeEnd.start - dataModel.sourceDepot.end,
                     0);  // TODO: targetDepot was modified
        // only account for the overtime due to the insertion off the customer
        // NOTE: overtime >= aux_overTime
        // overtime -= aux_overTime;

        // compare with the total overtime in the route (not only the overtime
        // due to the insertion)
        // scenario.getEdgeWeight(scenario.getEdge(dataModel.sourceDepot, Cloc))
        // + scenario.getEdgeWeight(scenario.getEdge(Cloc,
        // dataModel.targetDepot))
        if (overtime <= Constants.MAX_OVERTIME &&
            Constants.OVERTIME_MULTIPLIER * overtime + ins.cost <
                Constants.UNVISITED_MULTIPLIER * (Cloc.customer.penalty)) {
          // System.out.println("inserted"+ins.ins_route.routeEnd.start+"
          // "+dataModel.sourceDepot.end+" "+overtime);
          // remove cost of dedicated service
          // dataModel.routingGraph.getEdgeWeight(dataModel.routingGraph.getEdge(dataModel.sourceDepot,
          // Cloc))+dataModel.routingGraph.getEdgeWeight(dataModel.routingGraph.getEdge(Cloc,
          // dataModel.targetDepot))
          recourseCost -=
              Constants.UNVISITED_MULTIPLIER * (Cloc.customer.penalty);
        } else  // remove the (re)inserted customer. more profitable to be
                // serviced by dedicated vehicle
        {
          // System.out.println("removed"+ins.ins_route.routeEnd.start+"
          // "+dataModel.sourceDepot.end+" "+overtime);
          ins.ins_route.remove(ins.node, scenario);
          ins.ins_route.updateEarliest(ins.ins_route.routeStart, scenario);
          ins.ins_route.updateLatest(ins.ins_route.routeEnd, scenario);
        }
      }
    }  // customer

    // account for the current overtime in the route
    recourseCost +=
        Constants.OVERTIME_MULTIPLIER *
        Math.max(new_route.routeEnd.start - dataModel.sourceDepot.end, 0);

    dataModel.targetDepot.end = aux;
    rollBackCustomersTW();

    return recourseCost;
  }

  public double evaluateInsertion(Node nod, Node aft, Route rot) {
    return dataModel.getTravelTime(aft.location, nod.location) +
        dataModel.getTravelTime(nod.location, aft.next.location) -
        dataModel.getTravelTime(aft.location, aft.next.location);
  }

  class SingleInsertion implements Comparable<SingleInsertion> {
    final double cost;
    final Node node;
    final Node after;
    final Route ins_route;

    public SingleInsertion(Node nod, Node aft, Route rot,
                           Graph<Location, DefaultWeightedEdge> scenario) {
      this.node = nod;
      this.after = aft;
      this.ins_route = rot;
      this.cost = (int)scenario.getEdgeWeight(
                      scenario.getEdge(after.location, node.location)) +
                  (int)scenario.getEdgeWeight(
                      scenario.getEdge(node.location, after.next.location)) -
                  (int)scenario.getEdgeWeight(
                      scenario.getEdge(after.location, after.next.location));
    }

    void execute(Graph<Location, DefaultWeightedEdge> scenario) {
      ins_route.insertNodeAfter(node, after, scenario);
    }

    public String toString() {
      String s = "";
      s += "Insert " + this.node + " after " + this.after + " at cost " +
           this.cost + "\n";
      return s;
    }

    @Override
    public int compareTo(SingleInsertion d) {
      return Double.compare(this.cost, d.cost);
    }
  }
}
