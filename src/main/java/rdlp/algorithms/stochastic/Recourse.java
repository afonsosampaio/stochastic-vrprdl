package rdlp.algorithms.stochastic;

import rdlp.model.RDLPSolution;
import rdlp.model.RDLPInstance;
import rdlp.model.Customer;
import rdlp.model.Location;

import org.jgrapht.Graph;
import org.jgrapht.graph.AsWeightedGraph;
import org.jgrapht.graph.DefaultWeightedEdge;

import rdlp.algorithms.heuristic.HeuristicSolution;
import rdlp.algorithms.heuristic.Route;
import rdlp.algorithms.heuristic.Node;

import java.util.*;
import javafx.util.Pair;

abstract public class Recourse {
  final RDLPInstance dataModel;
  final List<Location> visited;
  final List<Customer> notVisited;
  final List<Location> locsNotVisited;
  public List<List>[] timeWindowsScenarios;
  public List<Graph<Location, DefaultWeightedEdge> > checkScenarios;
  public double missedCustomers;
  public double rescheduledCustomers;
  final static int CUSTOMER_MIN_TIME_AT_LOC = 10;

  Recourse(RDLPInstance dataModel) {
    this.dataModel = dataModel;
    notVisited =
        new ArrayList<Customer>();  // store customers not visited by a route
    locsNotVisited =
        new ArrayList<Location>();  // the correspondent customer's location
    visited =
        new ArrayList<Location>();  // store customers not visited by a route
    this.missedCustomers = 0;
    this.rescheduledCustomers = 0;
  }

  public void setTimeWindowsScenarios(
      List<List>[] twsce,
      List<Graph<Location, DefaultWeightedEdge> > scenarios) {
    timeWindowsScenarios = twsce;
    checkScenarios = scenarios;
  }

  public void availableLocations(Graph<Location, DefaultWeightedEdge> scenario,
                                 int sce_index) {
    assert (scenario == checkScenarios.get(sce_index)) == true;

    int custOrder = 0;
    int locOrder = 0;
    for (Customer customer : dataModel.customers) {
      // save original time windows
      for (Location loc : customer.locations) {
        loc.start_aux = loc.start;
        loc.end_aux = loc.end;
      }

      // if(customer.locations.size() <= 1)
      //   continue;

      // new time windows given the revealed scenario
      Pair<Integer, Integer> pair;
      locOrder = 0;
      for (Location loc : customer.locations) {
        // for(int j=0; j<timeWindowsScenarios[sce_index].get(custOrder).size(); ++j)
        {
          pair = (Pair<Integer, Integer>)timeWindowsScenarios[sce_index]
                     .get(custOrder)
                     .get(locOrder);
          loc.start = pair.getKey();
          loc.end = pair.getValue();
        }
        locOrder++;
      }
      custOrder++;
    }
  }

  public void availableLocations(
      Graph<Location, DefaultWeightedEdge> scenario) {
    for (Customer customer : dataModel.customers) {
      // save original time windows
      for (Location loc : customer.locations) {
        // node = locationToNodeMap.get(loc);
        loc.start_aux = loc.start;
        loc.end_aux = loc.end;
      }

      if (customer.locations.size() <= 1) continue;

      // After travel time reveal
      Location prev = null;
      int travelTime, order = 0;
      int lastEnd = 0;

      // new time windows given the revealed scenario
//      System.out.print("New customer " + customer + " itinerary: ");
      for (Location loc : customer.locations) {
        if (order > 0) {
          travelTime = (int)scenario.getEdgeWeight(scenario.getEdge(prev, loc));
          // loc.start = Math.max(loc.start, prev.start + travelTime);
          loc.start = Math.max(loc.start, lastEnd + travelTime);
          loc.end = Math.max(loc.end, loc.start);
          lastEnd = loc.end;
          if (loc.start > loc.end_aux)
          {
            lastEnd = loc.start + CUSTOMER_MIN_TIME_AT_LOC;
            loc.end = lastEnd;
//            loc.start = loc.end = 0;  // not available, too short
          }
          else if (loc.end_aux - loc.start < CUSTOMER_MIN_TIME_AT_LOC) {
            // System.out.println("Tight TW!!!");
            loc.end = loc.start + CUSTOMER_MIN_TIME_AT_LOC;
            lastEnd = loc.end;
          }
        }
        else
          lastEnd = loc.end;
//        System.out.print("["+loc.start+", "+loc.end+"] ");
        prev = loc;
        order++;
      }
//      System.out.println();
    }
  }

  public void rollBackCustomersTW() {
    for (Customer customer : dataModel.customers) {
      for (Location loc : customer.locations) {
        loc.start = loc.start_aux;
        loc.end = loc.end_aux;
      }
    }
  }

  /**
   *
   * @return cost of the recourse actions under travel times realization given
   * the first stage solution
   */
  abstract public double evaluateScenario(
      HeuristicSolution solution,
      Graph<Location, DefaultWeightedEdge> scenario);
  abstract public double evaluateScenario(
      Route solution, Node node, Node after,
      Graph<Location, DefaultWeightedEdge> scenario, int index);
}
