package rdlp.algorithms.stochastic;

import org.jgrapht.Graph;
import org.jgrapht.graph.DefaultWeightedEdge;
import rdlp.algorithms.heuristic.HeuristicSolution;
import rdlp.algorithms.heuristic.Node;
import rdlp.algorithms.heuristic.Route;
import rdlp.model.Customer;
import rdlp.model.Location;
import rdlp.model.RDLPInstance;
import rdlp.util.Constants;

import java.util.*;

public class RevisitRecourseEV extends Recourse {
  private Queue<Route.SingleInsertion> insertionCandidates;
  Map<Location, Node> locationToNodeMap = new HashMap<>();
  private Route evalRoute;
  private Set<Location> rescheduledSet = new HashSet<>();

  public RevisitRecourseEV(RDLPInstance dataModel) {
    super(dataModel);
    insertionCandidates = new PriorityQueue<>(100, Collections.reverseOrder());

    for (Location loc : dataModel.routingGraph.vertexSet())
      locationToNodeMap.put(loc, new Node(loc));

    evalRoute =
            new Route(dataModel);  // so, do not allocate memory every iteration...
  }

  /**
   * @return the second stage cost (travelled distance + recourse penalty, in
   * time unit) for a given first stage solution (@param solution) and realized
   * scenario (@param scenario)
   */
  public double evaluateScenario(
          HeuristicSolution solution,
          Graph<Location, DefaultWeightedEdge> scenario) {

    int missed = 0;  // total missed deliveries, considering all routes
    int rescheduled = 0; //total rescheduled deliveries
    int startTime, travelTime, auxTime;
    boolean skip;
    Node current, next;
    double recourseCost = 0.0;
    double overtime = 0.0;

    int routeMissed;
    int routeRescheduled;
    double routeCost = 0;
    double routeOvertime = 0;

    availableLocations(scenario);

    // Allow overtime for vehicles returning to the depot.
    int aux = dataModel.targetDepot.end;
    dataModel.targetDepot.end = 999999;  // TODO: not the best solution....

    for (Route route : solution.routes) {
      if (route.size() == 2)  // skip empty routes
        continue;

      visited.clear();
      notVisited.clear();
      locsNotVisited.clear();

      rescheduledSet.clear();

      routeMissed = missed;
      routeCost = recourseCost;

      routeRescheduled = rescheduled;

      Route workingCopy = route.deepCopy();

      startTime = workingCopy.routeStart.location.start;
      current = workingCopy.routeStart;
      next = workingCopy.routeStart.next;

      boolean nextCustomersOvertime = false;

      while (next != workingCopy.routeEnd) {
        travelTime = (int)scenario.getEdgeWeight(
                        scenario.getEdge(current.location, next.location));
        auxTime = startTime;
        startTime = Math.max(startTime + travelTime, next.location.start);

        if (startTime >= 720) {
          nextCustomersOvertime = true;
          travelTime = (int)scenario.getEdgeWeight(
            scenario.getEdge(current.location, next.location));
          startTime = Math.max(auxTime + travelTime, next.location.start);
        }

        if (startTime > next.location.end && !nextCustomersOvertime) {
          workingCopy.remove(next, dataModel.routingGraph);
          workingCopy.updateEarliest(current, dataModel.routingGraph);
          workingCopy.updateLatest(current, dataModel.routingGraph);
          boolean updatedRoute = workingCopy.isNewLocationAfter(next.location, current,
                                      auxTime, locationToNodeMap, scenario,
                                      insertionCandidates);
          if (updatedRoute) {
            ++rescheduled;
            rescheduledSet.add(next.location);
          } else {
            ++missed;
            if (rescheduledSet.contains(next.location))
              --rescheduled;

            recourseCost +=
              Constants.UNVISITED_MULTIPLIER * (next.location.customer.penalty);
            notVisited.add(next.location.customer);
            locsNotVisited.add(next.location);
          }
          next = current.next;
          travelTime = (int)scenario.getEdgeWeight(
            scenario.getEdge(current.location, next.location));
          startTime = Math.max(auxTime + travelTime, next.location.start);
          current = next;
          if (next != workingCopy.routeEnd)
            next = current.next;
        } else {
          visited.add(next.location);
          current = next;
          next = current.next;
        }
      }  // while

      // account for the current overtime in the route
      travelTime = (int)scenario.getEdgeWeight(
        scenario.getEdge(next.prev.location, next.location));
      startTime += travelTime;
      routeOvertime =  Constants.OVERTIME_MULTIPLIER *
                       Math.max(startTime - dataModel.sourceDepot.end, 0);
      overtime += routeOvertime;
      routeCost = recourseCost - routeCost;
      routeMissed = missed - routeMissed;
      routeRescheduled = rescheduled - routeRescheduled;
      route.recourseCost += (routeCost + routeOvertime);
    } // routes

    this.missedCustomers = missed;
    this.rescheduledCustomers = rescheduled;
    dataModel.targetDepot.end = aux;

    rollBackCustomersTW();

    return recourseCost + overtime;
  }

  public double evaluateScenario(Route rt, Node node, Node after,
                                 Graph<Location, DefaultWeightedEdge> scenario,
                                 int index) {
    double recourseCost = 0.0;
    int startTime, travelTime, auxTime;
    Node current, next;
    int missed = 0;
    int rescheduled = 0;

    // create the route with node visited after after
    evalRoute.clear();
    evalRoute.routeEnd.end = dataModel.targetDepot.end;

    current = evalRoute.routeStart;
    for (Node i : rt) {
      if (i == rt.routeStart || i == rt.routeEnd) continue;
      if (i == after.next) {
        next = new Node(node.location);
        evalRoute.insertNodeAfter(next, current, dataModel.routingGraph);
        current = next;
      }
      next = new Node(i.location);
      evalRoute.insertNodeAfter(next, current, dataModel.routingGraph);
      current = next;
    }
    evalRoute.updateEarliest(evalRoute.routeStart, dataModel.routingGraph);
    evalRoute.updateLatest(evalRoute.routeEnd, dataModel.routingGraph);
    if (evalRoute.isFeasible(dataModel.routingGraph) == false) {
      System.out.println(rt);
      System.out.println(evalRoute);
      evalRoute.isFeasibleVERBOSE(dataModel.routingGraph);
      assert evalRoute.isFeasible(dataModel.routingGraph) == true;
    }

    startTime = evalRoute.routeStart.location.start;
    current = evalRoute.routeStart;
    next = evalRoute.routeStart.next;

    availableLocations(scenario, index);

    // Allow overtime for vehicles returning to the depot.
    int aux = dataModel.targetDepot.end;
    dataModel.targetDepot.end = 999999;  // TODO: not the best solution....
    evalRoute.routeEnd.end = dataModel.targetDepot.end;

    boolean nextCustomersOvertime = false;

    while (next != evalRoute.routeEnd) {
      travelTime = (int)scenario.getEdgeWeight(
        scenario.getEdge(current.location, next.location));
      auxTime = startTime;
      startTime = Math.max(startTime + travelTime, next.location.start);

      if (startTime >= 720) {
        nextCustomersOvertime = true;
        travelTime = (int)scenario.getEdgeWeight(
          scenario.getEdge(current.location, next.location));
        startTime = Math.max(auxTime + travelTime, next.location.start);
      }

      if (startTime > next.location.end && !nextCustomersOvertime) {
        evalRoute.remove(next, dataModel.routingGraph);
        evalRoute.updateEarliest(current, dataModel.routingGraph);
        evalRoute.updateLatest(current, dataModel.routingGraph);
        boolean updatedRoute = evalRoute.isNewLocationAfter(next.location, current,
          auxTime, locationToNodeMap, scenario,
          insertionCandidates);
        if (updatedRoute) {
          ++rescheduled;
        } else {
          ++missed;

          recourseCost +=
            Constants.UNVISITED_MULTIPLIER * (next.location.customer.penalty);
          notVisited.add(next.location.customer);
          locsNotVisited.add(next.location);
        }
        next = current.next;
        travelTime = (int)scenario.getEdgeWeight(
          scenario.getEdge(current.location, next.location));
        startTime = Math.max(auxTime + travelTime, next.location.start);
        current = next;
        if (next != evalRoute.routeEnd)
          next = current.next;
      } else {
        visited.add(next.location);
        current = next;
        next = current.next;
      }
    }  // while

    // account for the current overtime in the route
    travelTime = (int)scenario.getEdgeWeight(
      scenario.getEdge(next.prev.location, next.location));
    startTime += travelTime;
    recourseCost +=  Constants.OVERTIME_MULTIPLIER *
      Math.max(startTime - dataModel.sourceDepot.end, 0);

    dataModel.targetDepot.end = aux;
    rollBackCustomersTW();

    return recourseCost;
  }

}
