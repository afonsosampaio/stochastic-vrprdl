package rdlp.algorithms.exact.mip.cutgeneration;

import ilog.concert.IloException;
import ilog.concert.IloIntVar;
import ilog.concert.IloRange;
import ilog.cplex.IloCplex;
import org.jgrapht.graph.DefaultWeightedEdge;
import rdlp.algorithms.exact.mip.MipData;
import rdlp.model.RDLPInstance;
import rdlp.util.MathProgrammingUtil;

import java.util.*;

public class UserCutCallbackImpl extends IloCplex.UserCutCallback {
  private final RDLPInstance dataModel;
  private MipData mipData;
  private final CutGenerator[] cutGenerators;

  private Map<DefaultWeightedEdge, Double> routingValues =
      new LinkedHashMap<>();

  public UserCutCallbackImpl(RDLPInstance dataModel, MipData mipData) {
    this.dataModel = dataModel;
    this.mipData = mipData;
    cutGenerators =
        new CutGenerator[] {new CapacityCutGenerator(dataModel, mipData)};
  }

  @Override
  protected void main() throws IloException {
    System.out.println("INVOKING USER");

    for (DefaultWeightedEdge e : dataModel.routingGraph.edgeSet()) {
      IloIntVar var = mipData.routingVars.get(e);
      double value = MathProgrammingUtil.doubleToInt(this.getValue(var));
      routingValues.put(e, value);
    }

    List<IloRange> newCuts = new ArrayList<>();
    for (CutGenerator cutGen : cutGenerators)
      newCuts.addAll(cutGen.generateInequalities(routingValues));

    for (IloRange cut : newCuts)
      this.add(cut, IloCplex.CutManagement.UseCutFilter);

    for (CutGenerator cutGen : cutGenerators) cutGen.printTotalCuts();
  }
}
