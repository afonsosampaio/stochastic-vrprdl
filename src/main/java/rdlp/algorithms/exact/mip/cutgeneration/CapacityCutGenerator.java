package rdlp.algorithms.exact.mip.cutgeneration;

import ilog.concert.IloException;
import ilog.concert.IloRange;
import org.jgrapht.Graph;
import org.jgrapht.graph.DefaultWeightedEdge;
import rdlp.algorithms.exact.mip.MipData;
import rdlp.model.Location;
import rdlp.model.RDLPInstance;

import java.util.*;

public class CapacityCutGenerator extends CutGenerator {
  private HashMap<CapacityCut<Location>, IloRange> cuts;

  private Graph<Location, DefaultWeightedEdge> workingGraph;
  //	private Intersection dummySource;
  //	private Intersection dummySink;

  //	private Map<Road, DefaultWeightedEdge> edgeMap=new HashMap<>();

  public CapacityCutGenerator(RDLPInstance dataModel, MipData mipData) {
    super(dataModel, mipData);
    this.initialize();
    cuts = new HashMap<>();
  }

  private void initialize() {
    //		//Construct working graph
    //		dummySource=new Intersection("dummySource");
    //		dummySink=new Intersection("dummySink");
    //		workingGraph=new
    //SimpleWeightedGraph<>(DefaultWeightedEdge.class);
    //		workingGraph.addVertex(dummySource);
    //		workingGraph.addVertex(dummySink);
    //		Graphs.addAllVertices(workingGraph,
    //carpIFModel.unionNetwork.vertexSet());
    //
    //		for(Road road : carpIFModel.unionNetwork.edgeSet()){
    //			RoutingNode
    //source=carpIFModel.unionNetwork.getEdgeSource(road); 			RoutingNode
    //target=carpIFModel.unionNetwork.getEdgeTarget(road); 			DefaultWeightedEdge
    //edge; 			if(workingGraph.containsEdge(source, target))
    //				edge=workingGraph.getEdge(source, target);
    //			else
    //				edge= workingGraph.addEdge(source, target);
    //			edgeMap.put(road, edge);
    //		}
    //		for(Depot supplyDepot : dataModel.saltSupplyDepots)
    //			Graphs.addEdge(workingGraph, dummySource, supplyDepot,
    //Integer.MAX_VALUE);
    //		MAX_VEHIC_CAPACITY=dataModel.vehicles.stream().mapToInt(v ->
    //v.saltCapacity).max().getAsInt(); 		for(RoutingNode intersection :
    //carpIFModel.requiredNetwork.vertexSet()){ 			double
    //edgeWeight=0;//=carpIFModel.requiredNetwork.edgesOf(intersection).stream().mapToInt(e
    //-> e.demand).sum(); 			for(Road road :
    //carpIFModel.requiredNetwork.edgesOf(intersection)){
    //				if(carpIFModel.reverseBidirectionalRoads.containsKey(road))
    //					edgeWeight+=.5*road.demand*road.lanes;
    //				else
    //					edgeWeight+=road.demand*road.lanes;
    //			}
    //			edgeWeight/=MAX_VEHIC_CAPACITY;
    //			Graphs.addEdge(workingGraph, intersection, dummySink,
    //edgeWeight);
    //		}
  }

  @Override
  public List<IloRange> generateInequalities(
      Map<DefaultWeightedEdge, Double> routingValues) throws IloException {
    CapacityCut<Location> capacityCut = this.separate(routingValues);

    if (capacityCut == null) {
      return Collections.emptyList();
    } else {
      List<IloRange> newCuts = new ArrayList<>();
      // Check whether we've separated this cut before
      if (cuts.containsKey(capacityCut))
        newCuts.add(cuts.get(capacityCut));
      else {
        IloRange cut = this.buildCut(capacityCut);
        cuts.put(capacityCut, cut);
        newCuts.add(cut);
        mipData.incrementCutCount(this, 1);
      }
      return newCuts;
    }
  }

  private CapacityCut<Location> separate(
      Map<DefaultWeightedEdge, Double> routingValues) {
    // Reset edge weights of bidirectional roads
    //		for(Road road : carpIFModel.unionNetwork.edgeSet()){
    //			workingGraph.setEdgeWeight(edgeMap.get(road), 0);
    //		}
    ////		for(DefaultWeightedEdge edge : workingGraph.edgeSet())
    ////			workingGraph.setEdgeWeight(edge, 0);
    //
    //		for(Road road : carpIFModel.unionNetwork.edgeSet()){
    //			double
    //edgeWeight=workingGraph.getEdgeWeight(edgeMap.get(road));
    //
    //
    //			/* We need to know how often an edge is deadheaded=number of
    //traversals of the edge - number of times the edge is serviced */
    //			for(Vehicle vehicle : dataModel.vehicles) {
    //				for (Double value :
    //routingValues.get(road).get(vehicle).values()) 					edgeWeight += value;
    //			}
    ////			for(int k=0; k<dataModel.vehicles.size(); k++)
    //// edgeWeight+=optionalEdgeValues[k][counter];
    ////			if(edgeWeight > 0)
    ////				System.out.println("aggregate edge: "+road+" value:
    ///"+edgeWeight);
    //
    //			if(carpIFModel.requiredNetwork.containsEdge(road)) {
    //				if(carpIFModel.reverseBidirectionalRoads.containsKey(road))
    //					edgeWeight += .5*(1.0 - 1.0 * road.demand*road.lanes /
    //MAX_VEHIC_CAPACITY); 				else 					edgeWeight += 1.0 - 1.0 * road.demand*road.lanes
    /// MAX_VEHIC_CAPACITY;
    //
    //				for(Vehicle vehicle : dataModel.vehicles) {
    //					for (Double value :
    //serviceValues.get(road).get(vehicle).values()) 						edgeWeight -= value;
    //				}
    //			}
    ////			if(workingGraph.getEdgeWeight(edgeMap.get(road)) >
    ///0) /				System.out.println("old edge weight:
    ///"+(workingGraph.getEdgeWeight(edgeMap.get(road)))+" new edge weight:
    ///"+edgeWeight);
    //			workingGraph.setEdgeWeight(edgeMap.get(road),
    //edgeWeight);
    //		}
    //
    ////		for(Road road : workingGraph.edgeSet())
    ////			System.out.println("Road: "+road+":
    ///"+workingGraph.getEdgeWeight(road));
    //
    //		//Calculate min s-t cut
    //		MinimumSTCutAlgorithm<RoutingNode, DefaultWeightedEdge>
    //cutAlgorithm=new PushRelabelMFImpl<>(workingGraph); 		double
    //cutWeight=cutAlgorithm.calculateMinCut(dummySource, dummySink);
    //
    //		Set<RoutingNode> sinkPartition=cutAlgorithm.getSinkPartition();
    //		sinkPartition.remove(dummySink);
    ////		System.out.println("Sink partition: "+sinkPartition);
    //
    //		double P=0;
    //		for(Road road : carpIFModel.requiredNetwork.edgeSet()){
    //			if(carpIFModel.reverseBidirectionalRoads.containsKey(road))
    //				P+=0.5*road.demand*road.lanes; //Bidirectional road will be
    //encountered twice: once in each direction. 			else 				P+=road.demand*road.lanes;
    //		}
    //		P*=2.0/MAX_VEHIC_CAPACITY;
    //
    //		//if(cutWeight-offset>= -Constants.EPSILON) { //No cut could be
    //found
    ////		if(cutWeight-minimumNrOfedges >= -Constants.EPSILON){ //No cut could
    ///be found
    //		if(cutWeight-P >= -Constants.EPSILON){ //No cut could be found
    ////			System.out.println("NO cut found "+ cutWeight+" sink partition:
    ///"+cutAlgorithm.getSinkPartition());
    //			return null;
    //		}
    //
    ////		System.out.println("Found cut! cutWeight: "+ cutWeight);
    ////		System.out.println("Offset: "+offset);
    //
    //		//Found cut
    ////		Set<Intersection>
    ///sinkPartition=cutAlgorithm.getSinkPartition(); /
    ///sinkPartition.remove(dummySink); /
    ///System.out.println("Sink partition: "+sinkPartition);
    //
    //		//TEMP
    ////		System.out.println("after:");
    ////		for(Road road : model.unionNetwork.edgeSet())
    ////			System.out.println(road+":
    ///"+model.unionNetwork.getEdgeWeight(road)); /		System.exit(1);
    //		//END TEMP
    //
    //		return new CapacityCut<>(sinkPartition, cutWeight);

    return null;
  }

  private IloRange buildCut(CapacityCut<Location> subtourCut)
      throws IloException {
    //		IloLinearIntExpr expr=mipData.cplex.linearIntExpr();
    //		double RHS=0;
    //		double partitionDemand=0;
    //		Set<RoutingNode> sinkPartition=subtourCut.getCutSet();
    //
    //		for(Road road : carpIFModel.unionNetwork.edgeSet()){
    //			RoutingNode
    //v1=carpIFModel.unionNetwork.getEdgeSource(road); 			RoutingNode
    //v2=carpIFModel.unionNetwork.getEdgeTarget(road);
    //			if(sinkPartition.contains(v1) ^ sinkPartition.contains(v2))
    //{
    ////				if(road.isRequired) {
    ////					RHS -= 1;
    ////				}
    ////				if(carpIFModel.requiredNetwork.containsEdge(road))
    ///{ /
    ///if(carpIFModel.reverseBidirectionalRoads.containsKey(road)) /
    ///RHS -= .5*road.lanes; /					else /
    ///RHS -= 1*road.lanes;
    ////
    ////					for(Vehicle vehicle :
    ///dataModel.vehicles){
    ////						for(IloIntVar var :
    ///mipData.serviceVars.get(road).get(vehicle).values()) /
    ///expr.addTerm(-1, var); /					} /
    ///} /				for(int k=0; k<model.NR_OF_VEHICLES;
    ///k++) /					expr.addTerm(1,
    ///mipData.optionalEdgeVars.get(k).get(road));
    //				for(Vehicle vehicle : dataModel.vehicles){
    //					for(IloIntVar var :
    //mipData.routingVars.get(road).get(vehicle).values()) 						expr.addTerm(1, var);
    //				}
    //			}
    //
    ////			if(road.isRequired && sinkPartition.contains(v1) ||
    ///sinkPartition.contains(v2)) /
    ///partitionDemand+=road.demand;
    //			if(carpIFModel.requiredNetwork.containsEdge(road) &&
    //(sinkPartition.contains(v1) || sinkPartition.contains(v2))) {
    //				if(carpIFModel.reverseBidirectionalRoads.containsKey(road))
    //					partitionDemand +=
    //.5*road.demand*road.lanes; 				else 					partitionDemand += road.demand*road.lanes;
    //			}
    //		}
    //		RHS+=2*Math.ceil(partitionDemand/MAX_VEHIC_CAPACITY);
    //		IloRange iloRange=mipData.cplex.ge(expr,
    //Math.ceil(RHS),"capacityCut"); 		System.out.println("cap cut: "+iloRange);
    //		return iloRange;

    return null;
  }

  public void printTotalCuts() {
    System.out.println("Total capacity cuts: " + cuts.size());
  }

  @Override
  public String getName() {
    return "CapacityCut";
  }
}
