package rdlp.algorithms.exact.mip.cutgeneration;

import java.util.Set;

/**
 * Created by jkinable on 10/20/16.
 */
public class CapacityCut<V> {
  private Set<V> cutSet;
  private double cutValue;

  public CapacityCut(Set<V> cutSet, double cutValue) {
    this.cutSet = cutSet;
    this.cutValue = cutValue;
  }

  public double getCutValue() { return this.cutValue; }

  public Set<V> getCutSet() { return this.cutSet; }

  public int hashCode() { return this.cutSet.hashCode(); }

  public boolean equals(Object o) {
    if (this == o) {
      return true;
    } else if (!(o instanceof CapacityCut)) {
      return false;
    } else {
      CapacityCut other = (CapacityCut)o;
      return this.cutSet.equals(other.getCutSet());
    }
  }

  public String toString() { return cutSet.toString(); }
}
