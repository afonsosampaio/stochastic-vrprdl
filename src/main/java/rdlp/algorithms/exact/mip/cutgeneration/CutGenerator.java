package rdlp.algorithms.exact.mip.cutgeneration;

import ilog.concert.IloConstraint;
import ilog.concert.IloException;
import ilog.concert.IloRange;
import org.jgrapht.graph.DefaultWeightedEdge;
import rdlp.algorithms.exact.mip.MipData;
import rdlp.model.RDLPInstance;

import java.util.List;
import java.util.Map;

public abstract class CutGenerator {
  protected final RDLPInstance dataModel;
  protected MipData mipData;
  protected List<IloConstraint> validInequalities;

  public CutGenerator(RDLPInstance dataModel, MipData mipData) {
    this.dataModel = dataModel;
    this.mipData = mipData;
    mipData.registerCut(this);
  }

  public abstract List<IloRange> generateInequalities(
      Map<DefaultWeightedEdge, Double> routingValues) throws IloException;

  public abstract void printTotalCuts();

  public abstract String getName();
}
