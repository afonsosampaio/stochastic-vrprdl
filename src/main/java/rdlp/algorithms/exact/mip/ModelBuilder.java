package rdlp.algorithms.exact.mip;

import ilog.concert.*;
import ilog.cplex.IloCplex;
import org.jgrapht.Graph;
import org.jgrapht.graph.DefaultWeightedEdge;
import rdlp.model.Customer;
import rdlp.model.Location;
import rdlp.model.RDLPInstance;
import rdlp.model.RDLPSolution;

import java.util.*;

/**
 *  Implementation of the MIP model from
 *  Reyes, D. Savelsbergh, M. Toriello, A. Vehicle Routing with Roaming Delivery
 * Locations. Transportation Research part C. This class builds the actual
 * model.
 */
public class ModelBuilder {
  private final RDLPInstance dataModel;
  private MipData mipData;

  private Map<DefaultWeightedEdge, IloIntVar> routingVars = new HashMap<>();
  private Map<Customer, IloNumVar> startTimeVars = new HashMap<>();
  private Map<Customer, IloNumVar> loadVars = new HashMap<>();

  private List<IloAddable> travelTimeConstraints;

  ModelBuilder(RDLPInstance dataModel) throws IloException {
    this.dataModel = dataModel;
    this.buildModel();
  }

  /**
   * Return the model as a data object
   * @return model
   */
  public MipData getModel() { return mipData; }

  /**
   * Construct the MIP model
   * @throws IloException
   */
  private void buildModel() throws IloException {
    IloCplex cplex = new IloCplex();

    // Create variables

    // Routing variables
    for (DefaultWeightedEdge e : dataModel.routingGraph.edgeSet()) {
      Location i = dataModel.routingGraph.getEdgeSource(e);
      Location j = dataModel.routingGraph.getEdgeTarget(e);
      IloIntVar var = cplex.boolVar("x_" + i + "_" + j);
      routingVars.put(e, var);

      // Local optimization: if i.start+d(i,j) > j.end, then we can never travel
      // from i to j, so we can fix the var to 0.
      if (i.start + dataModel.routingGraph.getEdgeWeight(e) > j.end)
        var.setMax(0);
    }

    // Starting time variables
    for (Customer customer : dataModel.customers) {
      int earliestStart =
          customer.locations.stream().mapToInt(l -> l.start).min().getAsInt();
      int latestFinish =
          customer.locations.stream().mapToInt(l -> l.end).max().getAsInt();
      IloNumVar var =
          cplex.numVar(earliestStart, latestFinish, "s_" + customer.ID);
      startTimeVars.put(customer, var);
    }

    // Load variables
    for (Customer customer : dataModel.customers) {
      IloNumVar var = cplex.numVar(0, dataModel.VEHIC_CAP - customer.demand,
                                   "L_" + customer.ID);
      loadVars.put(customer, var);
    }

    // Create objective
    IloLinearIntExpr objective = cplex.linearIntExpr();
    for (DefaultWeightedEdge e : dataModel.routingGraph.edgeSet())
      objective.addTerm((int)dataModel.routingGraph.getEdgeWeight(e),
                        routingVars.get(e));
    cplex.addMinimize(objective);

    // Create constraints

    // 1. Every customer must be visited (constraint 2c)
    for (Customer customer : dataModel.customers) {
      if (customer == dataModel.sourceDepot.customer ||
          customer == dataModel.targetDepot.customer)
        continue;
      IloLinearIntExpr expr = cplex.linearIntExpr();
      for (Location l1 : customer.locations) {
        for (DefaultWeightedEdge edge :
             dataModel.routingGraph.outgoingEdgesOf(l1)) {
          Location l2 = dataModel.routingGraph.getEdgeTarget(edge);
          if (customer ==
              l2.customer)  // l2 is a location belonging to the same customer
            continue;
          expr.addTerm(1, routingVars.get(edge));
        }
      }
      cplex.addEq(expr, 1, "visit_c" + customer.ID);
    }

    // 2. Flow preservation (constraint 2b)
    for (Location loc : dataModel.routingGraph.vertexSet()) {
      if (loc == dataModel.sourceDepot || loc == dataModel.targetDepot)
        continue;
      IloLinearIntExpr expr1 = cplex.linearIntExpr();
      IloLinearIntExpr expr2 = cplex.linearIntExpr();
      for (DefaultWeightedEdge edge :
           dataModel.routingGraph.incomingEdgesOf(loc))
        expr1.addTerm(1, routingVars.get(edge));
      for (DefaultWeightedEdge edge :
           dataModel.routingGraph.outgoingEdgesOf(loc))
        expr2.addTerm(1, routingVars.get(edge));
      cplex.addEq(expr1, expr2, "flowpreserv_" + loc);
    }

    // 3. Time window constraints (constraint 2e)
    for (Customer customer : dataModel.customers) {
      // Skip source/target depot customers, their time windows are well defined
      if (customer == dataModel.sourceDepot.customer ||
          customer == dataModel.targetDepot.customer)
        continue;
      IloLinearIntExpr expr1 = cplex.linearIntExpr();
      IloLinearIntExpr expr2 = cplex.linearIntExpr();
      for (Location loc : customer.locations) {
        for (DefaultWeightedEdge e :
             dataModel.routingGraph.outgoingEdgesOf(loc)) {
          expr1.addTerm(loc.start, routingVars.get(e));
          expr2.addTerm(loc.end, routingVars.get(e));
        }
      }
      cplex.addLe(expr1, startTimeVars.get(customer), "start_c" + customer.ID);
      cplex.addLe(startTimeVars.get(customer), expr2, "end_c" + customer.ID);
    }

    // 4. Travel time constraints (constraint 2f)
    travelTimeConstraints = new ArrayList<>(dataModel.customers.size() *
                                            dataModel.customers.size());
    for (Customer c1 : dataModel.customers) {
      if (c1 == dataModel.targetDepot.customer) continue;
      for (Customer c2 : dataModel.customers) {
        if (c2 == dataModel.sourceDepot.customer || c1 == c2) continue;

        IloLinearNumExpr expr = cplex.linearNumExpr();
        expr.addTerm(1.0, startTimeVars.get(c1));
        expr.setConstant(-dataModel.TIME_HORIZON);
        for (Location l1 : c1.locations) {
          for (Location l2 : c2.locations) {
            DefaultWeightedEdge edge = dataModel.routingGraph.getEdge(l1, l2);
            expr.addTerm(dataModel.routingGraph.getEdgeWeight(edge) +
                             dataModel.TIME_HORIZON,
                         routingVars.get(edge));
          }
        }

        IloConstraint constr = cplex.addGe(startTimeVars.get(c2), expr,
                                           "travel_c" + c1.ID + "_c" + c2.ID);
        travelTimeConstraints.add(constr);
      }
    }

    // 5. Vehicle load constraints (contraint 2h)
    for (Customer c1 : dataModel.customers) {
      if (c1 == dataModel.targetDepot
                    .customer)  // there are no arcs leaving the target depot
        continue;
      for (Customer c2 : dataModel.customers) {
        if (c2 == dataModel.sourceDepot.customer || c1 == c2) continue;
        IloLinearNumExpr expr1 = cplex.linearNumExpr();
        expr1.addTerm(1.0, loadVars.get(c1));
        expr1.setConstant(dataModel.VEHIC_CAP);
        for (Location l1 : c1.locations) {
          for (Location l2 : c2.locations) {
            DefaultWeightedEdge edge = dataModel.routingGraph.getEdge(l1, l2);
            expr1.addTerm(-dataModel.VEHIC_CAP, routingVars.get(edge));
          }
        }

        IloLinearNumExpr expr2 = cplex.linearNumExpr();
        expr2.addTerm(1.0, loadVars.get(c2));
        expr2.setConstant(c2.demand);

        cplex.addGe(expr1, expr2, "loadConstr_c" + c1.ID + "_c" + c2.ID);
      }
    }

    this.mipData = new MipData(cplex, routingVars, startTimeVars, loadVars);
    //        cplex.use(new LazyCutCallbackImpl(dataModel, mipData));
    //        cplex.use(new UserCutCallbackImpl(dataModel, mipData));
  }

  /**
   * Fix all variables the to their corresponding values in the provided initial
   * solution
   * @param initSolution initial solution
   * @throws IloException
   */
  public void fixSolution(RDLPSolution initSolution) throws IloException {
    throw new UnsupportedOperationException(
        "NOt yet implemented. Just copy paste from warmstart method");
  }

  /**
   * Warmstart the MIP model with the initial solution
   * @param initSolution initial solution
   * @throws IloException
   */
  public void warmstart(RDLPSolution initSolution) throws IloException {
    Map<DefaultWeightedEdge, Double> edgeToValue = new LinkedHashMap<>();
    for (DefaultWeightedEdge e : dataModel.routingGraph.edgeSet())
      edgeToValue.put(e, 0.0);
    Map<Customer, Double> customerStartTimeMap = new LinkedHashMap<>();
    for (Customer customer : dataModel.customers)
      customerStartTimeMap.put(customer, 1.0);

    for (List<Location> route : initSolution.routes) {
      Iterator<Location> it = route.iterator();
      Location l1 = it.next();
      double time = 0;
      while (it.hasNext()) {
        Location l2 = it.next();
        DefaultWeightedEdge edge = dataModel.routingGraph.getEdge(l1, l2);
        edgeToValue.put(edge, 1.0);

        time = Math.max(time + dataModel.routingGraph.getEdgeWeight(edge),
                        l2.start);
        double finalTime = time;  // java needs final values for lambda function
        customerStartTimeMap.computeIfPresent(
            l2.customer,
            (cust, value)
                -> Math.max(value,
                            finalTime));  // Needed to determine the arrival
                                          // time at the target depot

        l1 = l2;
      }
    }
    // Collect all variables
    List<IloNumVar> vars = new ArrayList<>();
    List<Double> values = new ArrayList<>();
    for (Map.Entry<DefaultWeightedEdge, Double> entry :
         edgeToValue.entrySet()) {
      vars.add(routingVars.get(entry.getKey()));
      values.add(entry.getValue());
    }
    for (Map.Entry<Customer, Double> entry : customerStartTimeMap.entrySet()) {
      vars.add(startTimeVars.get(entry.getKey()));
      values.add(entry.getValue());
    }
    IloNumVar[] varArray = vars.toArray(new IloNumVar[vars.size()]);
    double[] valueArray =
        values.stream().mapToDouble(Double::doubleValue).toArray();
    mipData.cplex.addMIPStart(varArray, valueArray);
  }

  /**
   * Rebuilds part of the model with the new travel times matrix. Travel times
   * are supposed encoded as edge weights. All constraints affected by the new
   * travel times are updated
   * @param routingGraph graph with the travel times as edge weights
   * @throws IloException
   */
  public void updateTravelTimes(
      Graph<Location, DefaultWeightedEdge> routingGraph) throws IloException {
    // Delete the objective
    mipData.cplex.remove(mipData.cplex.getObjective());

    // Delete the travel time constraints
    mipData.cplex.remove(travelTimeConstraints.toArray(
        new IloAddable[travelTimeConstraints.size()]));
    travelTimeConstraints.clear();

    // add new objective
    IloLinearIntExpr objective = mipData.cplex.linearIntExpr();
    for (DefaultWeightedEdge e : routingGraph.edgeSet())
      objective.addTerm((int)routingGraph.getEdgeWeight(e), routingVars.get(e));
    mipData.cplex.addMinimize(objective);

    // add new travel time constraints
    for (Customer c1 : dataModel.customers) {
      if (c1 == dataModel.targetDepot.customer) continue;
      for (Customer c2 : dataModel.customers) {
        if (c2 == dataModel.sourceDepot.customer || c1 == c2) continue;

        IloLinearNumExpr expr = mipData.cplex.linearNumExpr();
        expr.addTerm(1.0, startTimeVars.get(c1));
        expr.setConstant(-dataModel.TIME_HORIZON);
        for (Location l1 : c1.locations) {
          for (Location l2 : c2.locations) {
            DefaultWeightedEdge edge = dataModel.routingGraph.getEdge(l1, l2);
            expr.addTerm(
                routingGraph.getEdgeWeight(edge) + dataModel.TIME_HORIZON,
                routingVars.get(edge));
          }
        }

        IloConstraint constr = mipData.cplex.addGe(
            startTimeVars.get(c2), expr, "travel_c" + c1.ID + "_c" + c2.ID);
        travelTimeConstraints.add(constr);
      }
    }
  }
}
