package rdlp.algorithms.exact.mip;

import ilog.concert.IloIntVar;
import ilog.concert.IloNumVar;
import ilog.cplex.IloCplex;
import org.jgrapht.graph.DefaultWeightedEdge;
import rdlp.algorithms.exact.mip.cutgeneration.CutGenerator;
import rdlp.model.Customer;

import java.util.HashMap;
import java.util.Map;

/**
 * Storage object for MIP data. Can be passed to different classes.
 */
public class MipData {
  public final IloCplex cplex;
  public final Map<DefaultWeightedEdge, IloIntVar> routingVars;
  public final Map<Customer, IloNumVar> startTimeVars;
  public final Map<Customer, IloNumVar> loadVars;

  public final Map<String, Integer> cutCount = new HashMap<>();

  public MipData(IloCplex cplex,
                 Map<DefaultWeightedEdge, IloIntVar> routingVars,
                 Map<Customer, IloNumVar> startTimeVars,
                 Map<Customer, IloNumVar> loadVars) {
    this.cplex = cplex;
    this.routingVars = routingVars;
    this.startTimeVars = startTimeVars;
    this.loadVars = loadVars;
  }

  /**
   * Method to keep track of the cut generators and their statistics
   * @param cutGenerator Cut generator
   */
  public void registerCut(CutGenerator cutGenerator) {
    cutCount.put(cutGenerator.getName(), 0);
  }

  /**
   * Tracks number of cuts generated for
   * @param cutGenerator Cut generator
   * @param increment Number of cuts generated
   */
  public void incrementCutCount(CutGenerator cutGenerator, int increment) {
    cutCount.put(cutGenerator.getName(),
                 cutCount.get(cutGenerator.getName()) + increment);
  }
}
