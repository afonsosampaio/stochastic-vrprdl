package rdlp.algorithms.exact.mip;

import ilog.concert.IloException;
import ilog.concert.IloIntVar;
import ilog.concert.IloNumVarType;
import ilog.concert.IloRange;
import ilog.cplex.IloCplex;
import org.jgrapht.Graph;
import org.jgrapht.graph.DefaultWeightedEdge;
import rdlp.algorithms.exact.mip.cutgeneration.CapacityCutGenerator;
import rdlp.algorithms.exact.mip.cutgeneration.CutGenerator;
import rdlp.model.Location;
import rdlp.model.RDLPInstance;
import rdlp.model.RDLPSolution;
import rdlp.util.Constants;
import rdlp.util.MathProgrammingUtil;

import java.util.*;

/**
 * Implementation of the MIP model from
 *  Reyes, D. Savelsbergh, M. Toriello, A. Vehicle Routing with Roaming Delivery
 * Locations. Transportation Research part C.
 */
public class MIP {
  private final RDLPInstance dataModel;
  private ModelBuilder modelBuilder;
  private MipData mipData;

  /* RDLP solution */
  private RDLPSolution solution;
  /* Best objective found after mip */
  private double objectiveValue = -1;
  /* Indicator whether the optimal solution has been found */
  private boolean optimal = false;
  /* Indicator whether a feasible solution has been found */
  private boolean isFeasible = true;

  public MIP(RDLPInstance dataModel) {
    this.dataModel = dataModel;

    try {
      modelBuilder = new ModelBuilder(dataModel);
      mipData = modelBuilder.getModel();
    } catch (IloException e) {
      e.printStackTrace();
    }
  }

  public void solve() throws IloException {
    if (Constants.WRITE_MIP_TO_FILE) mipData.cplex.exportModel("rdlp.lp");
    //        mipData.cplex.writeParam(arg0) //Write parameters
    //		mipData.cplex.exportModel("mip.sav"); //Export as sav
    //		mipData.cplex.exportModel("mip.mps"); //Write MIP start
    //		mipData.cplex.setParam(BooleanParam.PreInd, false); //Disable
    //presolve.
    mipData.cplex.setParam(IloCplex.IntParam.TimeLimit,
                           Constants.MIP_TIMELIMIT * Constants.MAXTHREADS);
    mipData.cplex.setParam(IloCplex.IntParam.ClockType, 1);  // CPU time
    mipData.cplex.setParam(IloCplex.IntParam.Threads, Constants.MAXTHREADS);
    mipData.cplex.setOut(null);  // Disable Cplex output

    if (mipData.cplex.solve() &&
        (mipData.cplex.getStatus() == IloCplex.Status.Feasible ||
         mipData.cplex.getStatus() == IloCplex.Status.Optimal)) {
      this.objectiveValue =
          MathProgrammingUtil.doubleToInt(mipData.cplex.getObjValue());
      this.optimal = mipData.cplex.getStatus() == IloCplex.Status.Optimal;
      this.isFeasible = true;
      this.solution = this.buildIntegerSolution();
    } else if (mipData.cplex.getStatus() == IloCplex.Status.Infeasible) {
      this.isFeasible = false;
      this.optimal = true;
    } else if (mipData.cplex.getCplexStatus() ==
               IloCplex.CplexStatus.AbortTimeLim) {
      this.isFeasible = true;  // Technically there is no proof whether or not a
                               // feasible solution exists
      this.optimal = false;
    } else {
      throw new RuntimeException("Cplex solve terminated with status: " +
                                 mipData.cplex.getStatus());
    }
  }

  /**
   * Solve problem as Linear Program. All cuts are separated. Cplex cuts (e.g.
   * gomory cuts) are not added.
   * @throws IloException
   */
  public void solveLP() throws IloException {
    int numberOfCuts = 0;
    // Cast integer variables to continues variables
    for (IloIntVar var : mipData.routingVars.values())
      mipData.cplex.add(mipData.cplex.conversion(var, IloNumVarType.Float));

    // Optional: separate cuts:
    CutGenerator[] cutGenerators =
        new CutGenerator[] {new CapacityCutGenerator(dataModel, mipData)};

    Map<DefaultWeightedEdge, Double> routingValues = new LinkedHashMap<>();
    List<IloRange> newCuts = new ArrayList<>();

    boolean foundCuts;
    do {
      mipData.cplex.solve();
      for (DefaultWeightedEdge edge : dataModel.routingGraph.edgeSet()) {
        IloIntVar var = mipData.routingVars.get(edge);
        double value = mipData.cplex.getValue(var);
        routingValues.put(edge, value);
      }

      for (CutGenerator cutGen : cutGenerators) {
        newCuts.addAll(cutGen.generateInequalities(routingValues));
      }
      for (IloRange cut : newCuts) mipData.cplex.add(cut);
      numberOfCuts += newCuts.size();
      foundCuts = !newCuts.isEmpty();
      newCuts.clear();
      System.out.println("total number of cuts: " + numberOfCuts);
    } while (foundCuts);
    System.out.println("Lp relaxation: " + mipData.cplex.getObjValue());
    this.objectiveValue = mipData.cplex.getObjValue();
  }

  /**
   * Solve Root node of Branch and Bound tree. Cplex cuts are added. Other cuts
   * are only added if UserCutCallback or LazyCutCallbacks are enabled
   * @throws IloException
   */
  public void solveRootNode() throws IloException {
    if (Constants.WRITE_MIP_TO_FILE) mipData.cplex.exportModel("bound.lp");

    // To solve a pure LP, use:
    //         mipData.cplex.setParam(IloCplex.DoubleParam.CutsFactor, 100.0);
    //         //Set to 1: disables cut, otherwise limits the max number of cuts
    //         that can be added per iteration
    //        mipData.cplex.setParam(IloCplex.IntParam.LiftProjCuts, -1);
    //        mipData.cplex.setParam(IloCplex.IntParam.FracCuts, -1);
    //        mipData.cplex.setParam(IloCplex.IntParam.ZeroHalfCuts, -1);
    //        mipData.cplex.setParam(IloCplex.IntParam.MIRCuts, -1);
    //        mipData.cplex.setParam(IloCplex.BooleanParam.PreInd, false);
    //        //Disable presolve.
    //        mipData.cplex.setParam(IloCplex.LongParam.HeurFreq, -1); //disable
    //        heuristic

    // mipData.cplex.writeParam(arg0)
    //		mipData.cplex.exportModel("mip.sav");
    //		mipData.cplex.exportModel("mip.mps");
    //		mipData.cplex.writeMIPStart("start.mst");
    //		mipData.cplex.writeParam("param.prm");

    mipData.cplex.setParam(IloCplex.LongParam.NodeLim, 1);
    mipData.cplex.setParam(IloCplex.IntParam.MIPEmphasis,
                           IloCplex.MIPEmphasis.BestBound);
    mipData.cplex.setParam(IloCplex.IntParam.TimeLimit,
                           Constants.MIP_TIMELIMIT);
    mipData.cplex.solve();
    this.objectiveValue = mipData.cplex.getBestObjValue();
  }

  /**
   * Close cplex and release resources (memory)
   */
  public void close() { mipData.cplex.end(); }

  /**
   * Warmstarts MIP model
   * @param initSolution warmstart solution
   * @throws IloException
   */
  public void warmstart(RDLPSolution initSolution) throws IloException {
    modelBuilder.warmstart(initSolution);
  }

  /**
   * Fixes all variables to their corresponding values in the provided solution.
   * Used to verify feasibility of a solution/correctness of the model
   * @param initSolution initial solution
   * @throws IloException
   */
  public void fixSolution(RDLPSolution initSolution) throws IloException {
    modelBuilder.fixSolution(initSolution);
  }

  /**
   * Transforms the MIP model solution into an @{@link RDLPSolution}
   * @return solution to the RDLP solution
   * @throws IloException
   */
  private RDLPSolution buildIntegerSolution() throws IloException {
    Map<Location, Location> successor = new HashMap<>();
    List<List<Location>> routes = new ArrayList<>();

    // Construct partial routes and a successor array
    for (DefaultWeightedEdge edge : dataModel.routingGraph.edgeSet()) {
      IloIntVar var = mipData.routingVars.get(edge);
      double value = mipData.cplex.getValue(var);
      if (value >= Constants.PRECISION) {
        Location l1 = dataModel.routingGraph.getEdgeSource(edge);
        Location l2 = dataModel.routingGraph.getEdgeTarget(edge);
        if (l1 == dataModel.sourceDepot) {  // Start of new route
          List<Location> route = new ArrayList<>();
          route.add(l1);
          route.add(l2);
          routes.add(route);
        } else  // Part of a route
          successor.put(l1, l2);
      }
    }
    // Complete all partial routes
    for (List<Location> route : routes) {
      Location last = route.get(1);
      while (last != dataModel.targetDepot) {
        Location next = successor.get(last);
        route.add(next);
        last = next;
      }
    }
    for (List<Location> route : routes) System.out.println(route);

    return new RDLPSolution(dataModel, routes,
                            (int)mipData.cplex.getObjValue());
  }

  /**
   * Returns the solution of the MIP model, or null if no solution was found
   * @return solution, or null if no solution was found
   */
  public RDLPSolution getSolution() { return this.solution; }

  /**
   * Get bound on the objective value
   * @return lower bound on the objective value
   */
  public double getLowerBound() {
    try {
      return mipData.cplex.getBestObjValue();
    } catch (IloException e) {
      e.printStackTrace();
    }
    return -1;
  }

  /**
   * Indicates whether solution is optimal
   */
  public boolean isOptimal() { return optimal; }

  /**
   * Returns size of search tree (nr of nodes)
   * @return size of the search tree, expressed in number of nodes
   */
  public int getNrOfNodes() { return mipData.cplex.getNnodes(); }

  /**
   * Returns whether there is a feasible solution
   * @return true if one or more feasible solutions were found within the time
   * limit.
   */
  public boolean isFeasible() { return isFeasible; }

  /**
   * Returns objective value, or -1 if no feasible solution has been found.
   * @return objective value
   */
  public double getObjective() { return objectiveValue; }

  /**
   * Prints the non-zero values from the MIP model
   * @throws IloException
   */
  public void printSolutionVariables() throws IloException {
    for (DefaultWeightedEdge edge : dataModel.routingGraph.edgeSet()) {
      IloIntVar var = mipData.routingVars.get(edge);
      double value = mipData.cplex.getValue(var);
      if (value >= Constants.PRECISION)
        System.out.println("" + edge + ": " + value);
    }
  }

  /* Info about cuts */

  /**
   * Returns the number of capacity cuts generated
   * @return the number of capacity cuts that have been generated
   */
  public int getNrCapacityCuts() {
    return mipData.cutCount.computeIfAbsent("CapacityCut", x -> 0);
  }

  /**
   * Updates the model with new travel times
   * @param routingGraph routing graph with new travel times.
   * @throws IloException
   */
  public void updateTravelTimes(
      Graph<Location, DefaultWeightedEdge> routingGraph) throws IloException {
    this.modelBuilder.updateTravelTimes(routingGraph);
  }
}
