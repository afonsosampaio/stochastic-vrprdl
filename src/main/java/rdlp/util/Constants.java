package rdlp.util;

public final class Constants {
  /* =========== CPLEX PARAMS ============== */

  /* Branch and bound time limit (in seconds) */
  public static final int MIP_TIMELIMIT = 60;

  /* Number of threads used by Cplex */
  public static final int MAXTHREADS = 4;

  /* Boolean defining whether the MIP/LP model is written to a file */
  public static final boolean WRITE_MIP_TO_FILE = false;

  /* =========== Constructive Heuristic ============== */

  /* Pool size, see Savelsbergh paper. Technically this is just the number of
   * time the constructive heuristic is invoked; only the best solution is
   * returned. */
  public static final int N = 100;

  /* Restricted candidate size, see Savelsbergh paper */
  public static final int K = 2;

  /* =========== Improvement Heuristic ============== */
  // Number of iterations to execute
  public static final int I = 15000;
  public static final int STOP_NO_IMPROVEMENT = 2500;

  // How often change the neighborhood operators (removal and insert) if no
  // improvement is found
  public static final int SWITCH = 100;

  // When to perform a (on-time) diversification (divide the route in two)
  public static final int SPLIT = 1000;

  /* =========== GENERAL PARAMS ============== */

  /* Precision (used for rounding) */
  public static final double PRECISION = 0.000001;

  public static final long BYTE_TO_MBYTE = 1024 * 1024;

  /* =========== SSA PARAMS ============== */
  public static final double MAX_OVERTIME = 120;
  public static final double OVERTIME_MULTIPLIER = 2;
  public static final int UNVISITED_MULTIPLIER = 10;
  public static final int SCENARIOS_TO_INCREASE = 5;
  public static final int MAX_INCREASE = 20;
  public static final double CONVERGENCE_TOLERANCE = 0.0125;
  public static final int CONVERGENCE_ITERATIONS = 15;
  public static final double SAA_GAP = 0.05;
}
