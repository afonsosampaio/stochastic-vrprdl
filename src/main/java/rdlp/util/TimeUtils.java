package rdlp.util;

import java.lang.management.ManagementFactory;
import java.lang.management.ThreadMXBean;

/**
 * Time
 */
public class TimeUtils {
  public static final double NANOSECONDS_TO_MILLISECONDS = 1.0 / 1000000.0;

  private static ThreadMXBean bean = ManagementFactory.getThreadMXBean();

  /** Get CPU time in nanoseconds. */
  public static long getCpuTime() { return bean.getCurrentThreadCpuTime(); }

  /** Get user time in nanoseconds. */
  public static long getUserTime() { return bean.getCurrentThreadUserTime(); }
}
