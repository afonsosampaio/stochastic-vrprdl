package rdlp.benchmark;

import ilog.concert.IloException;
import rdlp.algorithms.exact.mip.MIP;
import rdlp.algorithms.heuristic.constructive.ConstructiveHeuristic;
import rdlp.io.RDLParser;
import rdlp.model.RDLPInstance;
import rdlp.model.RDLPSolution;
import rdlp.util.TimeUtils;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static rdlp.util.TimeUtils.NANOSECONDS_TO_MILLISECONDS;

/**
 * Solves a RDLP instances provided as input parameter using a MIP model and
 * prints its solution.
 */
public class MIPBenchmark {
  public static void main(String[] args) throws IloException {
    if (args.length != 1)
      throw new IllegalArgumentException("Usage: MIPBenchmark <InstanceFile>");

    File file = new File(args[0]);
    RDLPInstance dataModel = RDLParser.parse(file, 1.0);

    MIP mip = new MIP(dataModel);

    // OPTIONAL: warmstart MIP model
    ConstructiveHeuristic heuristic = new ConstructiveHeuristic(dataModel);
    heuristic.run();
    RDLPSolution initSolution = heuristic.getSolution();
    if (initSolution != null) {
      initSolution.verify(dataModel.routingGraph);
      mip.warmstart(initSolution);
    }

    double runTime = TimeUtils.getCpuTime();
    mip.solve();
    runTime = (TimeUtils.getCpuTime() - runTime) * NANOSECONDS_TO_MILLISECONDS;

    String instanceName = dataModel.name;
    int nrCustomers = dataModel.NR_CUST;
    int nrLocations = dataModel.NR_LOC;
    double objective = mip.getObjective();
    double lowerBound = mip.getLowerBound();
    boolean feasible = mip.isFeasible();
    boolean optimal = mip.isOptimal();

    if (feasible) mip.getSolution().verify(dataModel.routingGraph);
    mip.close();

    // Print result
    List<Object> output =
        Arrays.asList(instanceName, nrCustomers, nrLocations, lowerBound,
                      objective, feasible, optimal, runTime);

    System.out.println("tag: " + output.stream()
                                     .map(Object::toString)
                                     .collect(Collectors.joining("\t")));
  }
}
