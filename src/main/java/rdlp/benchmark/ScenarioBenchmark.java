package rdlp.benchmark;

import rdlp.algorithms.heuristic.HeuristicSolution;
import rdlp.algorithms.heuristic.improvement.ImprovementHeuristic;
import rdlp.algorithms.heuristic.constructive.ConstructiveHeuristic;
import rdlp.io.RDLParser;
import rdlp.model.RDLPInstance;
import rdlp.model.RDLPSolution;
import rdlp.util.TimeUtils;

import org.jgrapht.Graph;
import org.jgrapht.graph.DefaultWeightedEdge;
import org.jgrapht.graph.AsWeightedGraph;
import rdlp.model.Location;
import rdlp.model.Customer;
import rdlp.algorithms.heuristic.Route;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import rdlp.algorithms.stochastic.*;
import rdlp.algorithms.stochastic.Recourse;

import java.util.*;
import javafx.util.Pair;

import static rdlp.util.TimeUtils.NANOSECONDS_TO_MILLISECONDS;

import java.text.DecimalFormat;

import rdlp.util.Constants;

import com.beust.jcommander.*;
import com.beust.jcommander.Parameter;

public class ScenarioBenchmark {
  public static void main(String[] args) {
    ArgParser argparse = new ArgParser();
    JCommander jc = JCommander.newBuilder().addObject(argparse).build();
    try {
      jc.parse(args);
    } catch (ParameterException e) {
      jc.usage();
      throw new IllegalArgumentException("Illegal arguments!");
    }

    double congestion_level = 0.35;
    double cv = argparse.varianceCoef.doubleValue();
    int Np = argparse.numSamples.intValue();

    DecimalFormat df = new DecimalFormat();
    df.setMaximumFractionDigits(2);

    File file = new File(argparse.instanceFile);
    RDLPInstance dataModel = RDLParser.parse(file, cv);

    double maxRange = dataModel.maxCustomerRange();

    Graph<Location, DefaultWeightedEdge> backupGraph = new AsWeightedGraph<>(
        dataModel.routingGraph,
        e -> dataModel.routingGraph.getEdgeWeight(e), true, false);

    // generate a large set of scenarios to evaluate the recourse action
    ScenarioGenerator scenarioGenerator = new ScenarioGenerator(dataModel);
    List<Graph<Location, DefaultWeightedEdge> > scenariosLarge =
        new ArrayList<Graph<Location, DefaultWeightedEdge> >();
    for (int i = 0; i < Np; ++i)
      scenariosLarge.add(scenarioGenerator.getScenario());

    ConstructiveHeuristic constructive;
    ImprovementHeuristic heuristic;
    RDLPSolution solution;
    Graph<Location, DefaultWeightedEdge> scenario;
    List<Double> results = new ArrayList<>();
    List<Double> edges = new ArrayList<>();
    List<Double> edges_bkp = new ArrayList<>();

    for (DefaultWeightedEdge e : dataModel.routingGraph.edgeSet())
      edges_bkp.add(dataModel.routingGraph.getEdgeWeight(e));

    int counter = 0;
    // for(DefaultWeightedEdge e : dataModel.routingGraph.edgeSet())
    // {
    //   System.out.println(e+"="+dataModel.routingGraph.getEdgeWeight(e));
    //   if(counter++>10)
    //     break;
    // }
    // System.out.println("-------------------------------");
    dataModel.targetDepot.end = 840;
    double aux;

    for (int i = 0; i < Np; ++i) {
      scenario = scenariosLarge.get(i);
      edges.clear();

      counter = 0;
      for (DefaultWeightedEdge e : dataModel.routingGraph.edgeSet())
        dataModel.routingGraph.setEdgeWeight(e, edges_bkp.get(counter++));

      // get a new travel time for each edge
      for (DefaultWeightedEdge e : scenario.edgeSet())
        edges.add(Math.floor(scenario.getEdgeWeight(e)));

      counter = 0;
      for (DefaultWeightedEdge e : dataModel.routingGraph.edgeSet())
        dataModel.routingGraph.setEdgeWeight(e, edges.get(counter++));

      // counter = 0;
      // for(DefaultWeightedEdge e : dataModel.routingGraph.edgeSet())
      // {
      //   System.out.println(e+"="+dataModel.routingGraph.getEdgeWeight(e));
      //   if(counter++>10)
      //     break;
      // }

      availableLocations(scenario, dataModel);

      constructive = new ConstructiveHeuristic(dataModel);
      constructive.run();
      // System.out.println("Initial Solution:");
      // System.out.println(constructive.getSolution().toString());
      heuristic =
          new ImprovementHeuristic(constructive.getSolution(), dataModel);
      heuristic.run(0);  // no recourse (deterministic case)

      solution = heuristic.getSolution();

      aux = 0.0;
      for (Customer c : heuristic._getSolution().getNotServicedCustomers())
        aux += Constants.UNVISITED_MULTIPLIER * c.penalty;
      for (Route route : heuristic._getSolution().routes)
        aux += Constants.OVERTIME_MULTIPLIER *
               Math.max(route.routeEnd.start - 720, 0);

      aux += heuristic._getSolution().getRoutingCost();
      // System.out.println("Solution:");
      // System.out.println(heuristic._getSolution().getRoutingCost());
      // System.out.println(heuristic._getSolution());
      // System.out.println(heuristic._getSolution().getNotServicedCustomers());
      // System.out.println("Total cost:"+aux);
      // System.out.println("-----------------------------");
      results.add(aux);

      rollBackCustomersTW(dataModel);
    }
    System.out.println(results);

    // evaluate the recourse actions for this solution
    List<Recourse> recourseStrategies = new ArrayList<Recourse>();
    recourseStrategies.add(new SimpleRecourse(dataModel));
    recourseStrategies.add(new SkipRecourse(dataModel));
    recourseStrategies.add(new SkipDPRecourse(dataModel));
    recourseStrategies.add(new RevisitRecourse(dataModel));
    recourseStrategies.add(new RevisitDPRecourse(dataModel));
  }
  public static class ArgParser {
    @Parameter(names = "-inst", description = "VRPRDL Instance File",
               required = true)
    public String instanceFile;

    @Parameter(names = "-cv",
               description =
                   "(Squared) Coefficient of variance for sampling scenarios")
    public Double varianceCoef = 1.0;

    @Parameter(names = "-sc", description = "Number of scenarios to evaluate")
    public Integer numSamples = 100;
  }

  public static void availableLocations(
      Graph<Location, DefaultWeightedEdge> scenario, RDLPInstance dataModel) {
    for (Customer customer : dataModel.customers) {
      // System.out.println("Customer:"+customer);
      // save original time windows
      for (Location loc : customer.locations) {
        // node = locationToNodeMap.get(loc);
        loc.start_aux = loc.start;
        loc.end_aux = loc.end;
        // System.out.print("["+loc.start+","+loc.end+"],");
      }
      // System.out.println("\n");
      if (customer.locations.size() <= 1) continue;

      // System.out.println("\nAfter travel time reveal:");
      Location prev = null;
      int travelTime, order = 0;

      // new time windows given the revealed scenario
      for (Location loc : customer.locations) {
        if (order > 0) {
          travelTime = (int)scenario.getEdgeWeight(scenario.getEdge(prev, loc));
          // loc.start = Math.max(loc.start, prev.start + travelTime);
          loc.start = Math.max(loc.start, prev.end + travelTime);
          loc.end = Math.max(loc.end, loc.start);
          if (loc.start > loc.end_aux) loc.start = loc.end = 0;  // not
                                                                 // available
        }
        // System.out.print("["+loc.start+","+loc.end+"],");
        prev = loc;
        order++;
      }
      // System.out.println("\n---------------------");
    }
  }

  public static void rollBackCustomersTW(RDLPInstance dataModel) {
    for (Customer customer : dataModel.customers) {
      for (Location loc : customer.locations) {
        loc.start = loc.start_aux;
        loc.end = loc.end_aux;
      }
    }
  }
}
