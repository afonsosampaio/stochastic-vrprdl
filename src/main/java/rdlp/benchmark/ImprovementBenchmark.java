package rdlp.benchmark;

import rdlp.algorithms.heuristic.HeuristicSolution;
import rdlp.algorithms.heuristic.improvement.ImprovementHeuristic;
import rdlp.algorithms.heuristic.constructive.ConstructiveHeuristic;
import rdlp.io.RDLParser;
import rdlp.model.RDLPInstance;
import rdlp.model.RDLPSolution;
import rdlp.util.TimeUtils;

import org.jgrapht.Graph;
import org.jgrapht.graph.DefaultWeightedEdge;
import rdlp.model.Location;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import rdlp.algorithms.stochastic.*;

import java.util.*;
import javafx.util.Pair;

import static rdlp.util.TimeUtils.NANOSECONDS_TO_MILLISECONDS;

import java.text.DecimalFormat;

import com.beust.jcommander.*;
import com.beust.jcommander.Parameter;

public class ImprovementBenchmark {
  public static void main(String[] args) {
    ArgParser argparse = new ArgParser();
    JCommander jc = JCommander.newBuilder().addObject(argparse).build();
    try {
      jc.parse(args);
    } catch (ParameterException e) {
      jc.usage();
      throw new IllegalArgumentException("Illegal arguments!");
    }

    double congestion_level = 0.35;
    double cv = argparse.varianceCoef.doubleValue();

    DecimalFormat df = new DecimalFormat();
    df.setMaximumFractionDigits(2);

    File file = new File(argparse.instanceFile);
    RDLPInstance dataModel = RDLParser.parse(file, cv);

    double maxRange = dataModel.maxCustomerRange();

    ConstructiveHeuristic constructive = new ConstructiveHeuristic(dataModel);

    double runTime = TimeUtils.getCpuTime();
    // System.out.println("Starting constructive heuristic for
    // "+dataModel.name);
    constructive.run();
    // System.out.println("Initial Solution:");
    // System.out.println(constructive.getSolution().toString());

    ImprovementHeuristic heuristic =
        new ImprovementHeuristic(constructive.getSolution(), dataModel);
    heuristic.run(0);  // no recourse (deterministic case)
    runTime = (TimeUtils.getCpuTime() - runTime) * NANOSECONDS_TO_MILLISECONDS;
    RDLPSolution solution = heuristic.getSolution();

    System.out.println("Solution:");
    System.out.println(heuristic._getSolution().getRoutingCost());
    System.out.println(heuristic._getSolution());

    // evaluate the recourse actions for this solution
    List<Recourse> recourseStrategies = new ArrayList<Recourse>();
    recourseStrategies.add(new SimpleRecourse(dataModel));
    recourseStrategies.add(new SkipRecourse(dataModel));
    recourseStrategies.add(new SkipDPRecourse(dataModel));
    recourseStrategies.add(new RevisitRecourse(dataModel));
    recourseStrategies.add(new RevisitDPRecourse(dataModel));

    // generate a large set of scenarios to evaluate the recourse action
    int Np = 10000;
    ScenarioGenerator scenarioGenerator = new ScenarioGenerator(dataModel);
    List<Graph<Location, DefaultWeightedEdge> > scenariosLarge =
        new ArrayList<Graph<Location, DefaultWeightedEdge> >();
    for (int i = 0; i < Np; ++i)
      scenariosLarge.add(scenarioGenerator.getScenario());

    // remove expected disruption to travel time (congestion_level*t_{ij})
    for (DefaultWeightedEdge e : dataModel.routingGraph.edgeSet())
      dataModel.routingGraph.setEdgeWeight(
          e, Math.ceil(dataModel.routingGraph.getEdgeWeight(e) /
                       (1.0 + congestion_level)));

    HeuristicSolution heuSolution =
        new HeuristicSolution(solution, dataModel, scenariosLarge);
    System.out.println(heuSolution.getRoutingCost());
    System.out.println(heuSolution);

    Double[] recourseCost = new Double[3 * recourseStrategies.size()];
    double total;
    Pair<Double, Double> availableTW;
    for (int r = 0; r < recourseStrategies.size(); ++r) {
      heuSolution.getObjective(recourseStrategies.get(r));
      recourseCost[3 * r] = heuSolution.getObjective();
      recourseCost[3 * r + 1] = heuSolution.getRecourseExpCost();
      recourseCost[3 * r + 2] =
          (double)(recourseStrategies.get(r).missedCustomers);
      // System.out.println(df.format(heuSolution.getObjective())+"["+df.format(heuSolution.getRoutingCost())+","+df.format(heuSolution.getRecourseExpCost())+"]");
    }

    availableTW = (Pair<Double, Double>)heuSolution.averageAvailableTime();
    // System.out.println(100*(availableTW.getKey()/dataModel.TIME_HORIZON)+"
    // "+100*(availableTW.getValue()/dataModel.TIME_HORIZON));
    int num_routes = heuSolution.getNumRoutes();

    List<Object> output;
    if (solution != null) {
      // solution.verify(dataModel.routingGraph);
      boolean feasible = true;
      output = Arrays.asList(
          dataModel.name, dataModel.NR_CUST, dataModel.NR_LOC,
          df.format(availableTW.getKey()),
          df.format(100 * (availableTW.getValue() / dataModel.TIME_HORIZON)),
          df.format(maxRange),
          // df.format(solution.objective),
          df.format(heuSolution.getRoutingCost()), num_routes,
          df.format(recourseCost[1]), df.format(recourseCost[2]),
          df.format(recourseCost[4]), df.format(recourseCost[5]),
          df.format(recourseCost[7]), df.format(recourseCost[8]),
          df.format(recourseCost[10]), df.format(recourseCost[11]),
          df.format(recourseCost[13]), df.format(recourseCost[14]),
          df.format(runTime));
    } else
      output = Arrays.asList(dataModel.name, dataModel.NR_CUST, 999999, 999999,
                             99999, 999999, 999999, runTime);

    System.out.println("tag: " + output.stream()
                                     .map(Object::toString)
                                     .collect(Collectors.joining("\t")));
  }
  public static class ArgParser {
    @Parameter(names = "-inst", description = "VRPRDL Instance File",
               required = true)
    public String instanceFile;

    @Parameter(names = "-cv",
               description =
                   "(Squared) Coefficient of variance for sampling scenarios")
    public Double varianceCoef = 1.0;
  }
}
