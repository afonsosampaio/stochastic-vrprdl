package rdlp.benchmark;

import rdlp.algorithms.heuristic.constructive.ConstructiveHeuristic;
import rdlp.io.RDLParser;
import rdlp.model.RDLPInstance;
import rdlp.model.RDLPSolution;
import rdlp.util.TimeUtils;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static rdlp.util.TimeUtils.NANOSECONDS_TO_MILLISECONDS;

public class ConstructiveBenchmark {
  public static void main(String[] args) {
    if (args.length != 1)
      throw new IllegalArgumentException(
          "Usage: ConstructiveBenchmark <InstanceFile>");

    File file = new File(args[0]);
    RDLPInstance dataModel = RDLParser.parse(file, 1.0);

    ConstructiveHeuristic heuristic = new ConstructiveHeuristic(dataModel);

    double runTime = TimeUtils.getCpuTime();
    // System.out.println("Starting constructive heuristic for
    // "+dataModel.name);
    heuristic.run();
    runTime = (TimeUtils.getCpuTime() - runTime) * NANOSECONDS_TO_MILLISECONDS;
    RDLPSolution solution = heuristic.getSolution();

    List<Object> output;
    if (solution != null) {
      solution.verify(dataModel.routingGraph);
      boolean feasible = true;
      output = Arrays.asList(dataModel.name, dataModel.NR_CUST, feasible,
                             solution.objective, runTime);
    } else
      output = Arrays.asList(dataModel.name, false, 999999, runTime);

    System.out.println("tag: " + output.stream()
                                     .map(Object::toString)
                                     .collect(Collectors.joining("\t")));
  }
}
