package rdlp.benchmark;

import ilog.concert.IloException;
import org.jgrapht.Graphs;
import rdlp.algorithms.exact.mip.MIP;
import rdlp.algorithms.heuristic.constructive.ConstructiveHeuristic;
import rdlp.algorithms.heuristic.improvement.ImprovementHeuristic;
import rdlp.algorithms.heuristic.HeuristicSolution;

import rdlp.io.RDLParser;

import rdlp.model.Location;
import rdlp.model.RDLPInstance;
import rdlp.model.RDLPSolution;
import rdlp.util.TimeUtils;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static rdlp.util.TimeUtils.NANOSECONDS_TO_MILLISECONDS;

import org.jgrapht.Graph;
import org.jgrapht.graph.DefaultWeightedEdge;

import rdlp.algorithms.stochastic.*;

import java.util.*;
import rdlp.util.Constants;
import java.text.DecimalFormat;
import com.beust.jcommander.*;
import com.beust.jcommander.Parameter;

/**
 * Solves a RDLP instance provided as input parameter using a SAA approach
 */
public class SAABenchmark {
  public static void main(String[] args) throws IloException {
    ArgParser argparse = new ArgParser();
    JCommander jc = JCommander.newBuilder().addObject(argparse).build();
    try {
      jc.parse(args);
    } catch (ParameterException e) {
      jc.usage();
      throw new IllegalArgumentException("Illegal arguments!");
    }

    DecimalFormat df = new DecimalFormat();
    df.setMaximumFractionDigits(2);

    File file = new File(argparse.instanceFile);
    RDLPInstance dataModel =
        RDLParser.parse(file, argparse.varianceCoef.doubleValue());
    if (argparse.jsonFile != "") dataModel.jsonify(argparse.jsonFile);

    int recourse = argparse.recourseP.intValue();
    double congestion_level = 0.35;

    // solve deterministic model before changing travel times
    ConstructiveHeuristic constructive = new ConstructiveHeuristic(dataModel);
    constructive.run();
    ImprovementHeuristic heuristic =
        new ImprovementHeuristic(constructive.getSolution(), dataModel);
    heuristic.run(0);  // no recourse (deterministic case)
    dataModel.MAX_ROUTES = heuristic._getSolution().getNumRoutes();
    System.out.println("EV solution:");
    System.out.println(heuristic._getSolution());
    System.out.println("obj:"+heuristic._getSolution().getObjective());
    System.out.println("-------------------------------");
    heuristic.getSolution().verify(dataModel.routingGraph);

    // change travel times, does not include delay
    double aux_var;
    for (DefaultWeightedEdge e : dataModel.routingGraph.edgeSet()) {
      aux_var = Math.ceil(dataModel.routingGraph.getEdgeWeight(e) / (1.0 + congestion_level));
      dataModel.routingGraph.setEdgeWeight(e, aux_var);
    }

    ScenarioGenerator sc = new ScenarioGenerator(dataModel);
    dataModel.routingGraphExp = sc.getPercentileScenario(argparse.percentile);

    List<Graph<Location, DefaultWeightedEdge> > evaluationScenarios = new ArrayList<Graph<Location, DefaultWeightedEdge> >();
    for (int i = 0; i < argparse.Np.intValue(); ++i)
      evaluationScenarios.add(sc.getScenario());

//    int stop = 0;
//    for (DefaultWeightedEdge e : evaluationScenarios.get(100).edgeSet()){//dataModel.routingGraph.edgeSet()) {
//      Location s = evaluationScenarios.get(100).getEdgeSource(e);//dataModel.routingGraph.getEdgeSource(e);
//      Location t = evaluationScenarios.get(100).getEdgeTarget(e);//dataModel.routingGraph.getEdgeTarget(e);
//      DefaultWeightedEdge ee = evaluationScenarios.get(100).getEdge(s,t);//dataModel.routingGraphExp.getEdge(s, t);
//      System.out.print(s + " " + t +": ");
//      System.out.println(evaluationScenarios.get(100).getEdgeWeight(e) + " " + (int)(evaluationScenarios.get(100).getEdgeWeight(ee)));
//      if (++stop > 10)
//        System.exit(1);
//    }

    Recourse recourseStrategy;
    SimpleRecourse simpleRecourse = new SimpleRecourse(dataModel);
    SkipRecourse skipRecourse = new SkipRecourse(dataModel);
    SkipDPRecourseEV skipDPRecourseEV = new SkipDPRecourseEV(dataModel);
    RevisitRecourseEV revisitRecourseEV = new RevisitRecourseEV(dataModel);
    RevisitDPRecourseEV revisitDPRecourseEV = new RevisitDPRecourseEV(dataModel);

    SAA saa_solver;
    switch (recourse) {
      case 1:
        saa_solver = new SAA(dataModel, skipRecourse,
                        argparse.Np.intValue(), argparse.N.intValue(), evaluationScenarios);
        recourseStrategy = skipRecourse;
        break;
      case 2:
        saa_solver = new SAA(dataModel, skipDPRecourseEV,
                        argparse.Np.intValue(), argparse.N.intValue(), evaluationScenarios);
        recourseStrategy = skipDPRecourseEV;
        break;
      case 3:
        saa_solver = new SAA(dataModel, revisitRecourseEV,
                        argparse.Np.intValue(), argparse.N.intValue(), evaluationScenarios);
        recourseStrategy = revisitRecourseEV;
        break;
      case 4:
        saa_solver = new SAA(dataModel, revisitDPRecourseEV,
                        argparse.Np.intValue(), argparse.N.intValue(), evaluationScenarios);
        recourseStrategy = revisitDPRecourseEV;
        break;
      default:
        saa_solver = new SAA(dataModel, simpleRecourse,
                        argparse.Np.intValue(), argparse.N.intValue(), evaluationScenarios);
        recourseStrategy = simpleRecourse;
    }

    saa_solver.setGap(argparse.saaGap.doubleValue());
    saa_solver.setCI(argparse.Ci.intValue());  //# iterations to decide on flattening

    if (argparse.Sc)
      saa_solver.set_stopConv();

    saa_solver.setTimeLim(argparse.TimeLim);

    saa_solver.setInitialSolution(heuristic._getSolution());

    List<Graph<Location, DefaultWeightedEdge> > scenariosLarge =
                                                    saa_solver.getNpSamples();  //evaluationScenarios
    HeuristicSolution heuSolution = new HeuristicSolution(heuristic.getSolution(),
                                                          dataModel,
                                                          scenariosLarge);
    double second_stage = 0.0;
    double EVmissed = 0.0;
    double EVrescheduled = 0.0;
    for (int i = 0; i < scenariosLarge.size(); ++i) {
      second_stage +=
        recourseStrategy.evaluateScenario(heuSolution, scenariosLarge.get(i));

      EVmissed += recourseStrategy.missedCustomers;

      EVrescheduled += recourseStrategy.rescheduledCustomers;
    }
    second_stage = second_stage / scenariosLarge.size();
    EVmissed = EVmissed / scenariosLarge.size();
    EVrescheduled = EVrescheduled / scenariosLarge.size();

    double EVrouting = heuSolution.getRoutingCost();
    double EVrecourse = second_stage;
    double avg_tw = heuSolution.getAverageSlackTW();

    // test
//    List<Recourse> recoursesList = new ArrayList<Recourse>();
//    recoursesList.add(simpleRecourse);
//    recoursesList.add(skipRecourse);
//    recoursesList.add(skipDPRecourseEV);
//    recoursesList.add(revisitRecourseEV);
//    recoursesList.add(revisitDPRecourseEV);
//    saa_solver.setRecourses(recoursesList);

    double runTime = TimeUtils.getCpuTime();
    saa_solver.run();

    runTime = (TimeUtils.getCpuTime() - runTime) * NANOSECONDS_TO_MILLISECONDS;
    // System.out.println("Final Solution:");
    // System.out.println(ssa_solver.getSolution()+"
    // "+ssa_solver.getSolution().getObjective());
    // System.out.println(ssa_solver.getSolution().getRoutingCost()+"
    // "+ssa_solver.getSolution().getRecourseExpCost());

    String instanceName = dataModel.name;
    int nrCustomers = dataModel.NR_CUST;
    double Objective = saa_solver.getObjective();
    double FstStgObjective = saa_solver.getSolution().getRoutingCost();
    double SndStgObjective = saa_solver.getExpRecourseCost();
    double saa_gap = saa_solver.getGapEstimator();
    double saa_lb = saa_solver.getLowerBound();
    double saa_incTime = saa_solver.getTimeToBest();
    int num_routes = saa_solver.getSolution().getNumRoutes();
    double saa_cputime = saa_solver.getCpuTime();
    double saa_missed = saa_solver.getMissed();
    double saa_rescheduled = saa_solver.getRescheduled();
    double avg_twS = saa_solver.getSolution().getAverageSlackTW();

    List<Object> output = Arrays.asList(
        instanceName, Integer.toString(nrCustomers - 2), df.format(recourse),
        df.format(Objective), df.format(saa_lb), df.format(saa_gap),
        df.format(FstStgObjective), df.format(SndStgObjective),
        df.format(EVrouting), df.format(EVrecourse), num_routes,
        df.format(avg_tw), df.format(avg_twS), df.format(saa_missed),
        df.format(saa_rescheduled), df.format(EVmissed), df.format(EVrescheduled), df.format(saa_incTime), df.format(saa_cputime));

    System.out.println("tag: " + output.stream()
                                     .map(Object::toString)
                                     .collect(Collectors.joining("\t")));
  }

  public static class ArgParser {
    @Parameter(names = "-inst", description = "VRPRDL Instance File",
               required = true)
    public String instanceFile;

    @Parameter(names = "-rec", description = "Recourse Policy for Second Stage",
               required = true)
    public Integer recourseP = 0;

    @Parameter(names = "-cv",
               description =
                   "(Squared) Coefficient of variance for sampling scenarios")
    public Double varianceCoef = 1.0;

    @Parameter(names = "-N",
               description = "Initial sample size for SAA problems")
    public Integer N = 1;

    @Parameter(names = "-Np", description = "Large sample size")
    public Integer Np = 10000;

    @Parameter(names = "-gap",
               description = "Maximum (estimate) optimality gap")
    public Double saaGap = Constants.SAA_GAP;

    @Parameter(names = "-ci", description = "Convergence iterations")
    public Integer Ci = Constants.CONVERGENCE_ITERATIONS;
    ;

    @Parameter(names = "-sc", description = "Stop when LB converges")
    public boolean Sc = false;

    @Parameter(names = "-tl",
               description = "Limits total execution time (seconds)")
    public double TimeLim = Double.POSITIVE_INFINITY;

    @Parameter(names = "-json",
               description = "JSON file representing the instance")
    public String jsonFile = "";

    @Parameter(names = "-te",
              description = "Travel time estimation multiplier")
    public double percentile = 0.25;
  }
}
