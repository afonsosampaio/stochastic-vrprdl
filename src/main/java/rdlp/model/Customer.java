package rdlp.model;

import java.util.Set;

/**
 * Class representing a customer
 */
public class Customer {
  /* Unique customer ID */
  public final int ID;
  /* Customer demand */
  public final int demand;
  /* Array of locations associated with this customer. */
  public final Set<Location> locations;

  // penalty for not servicing the customer (based on his/her home location)
  public double penalty;

  public boolean isServiced;

  public Customer(int id, int demand, Set<Location> locations) {
    ID = id;
    this.demand = demand;
    this.locations = locations;
    locations.stream().forEach(l -> l.linkCustomer(this));
    this.penalty = 0.0;
    this.isServiced = false;
  }

  @Override
  public int hashCode() {
    return ID;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == null || !(obj instanceof Location)) return false;
    Location other = (Location)obj;
    return this.ID == other.ID;
  }

  @Override
  public String toString() {
    return "c" + ID;
  }
}
