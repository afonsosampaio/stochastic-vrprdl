package rdlp.model;

import org.apache.commons.math3.distribution.GammaDistribution;
import org.apache.commons.math3.random.JDKRandomGenerator;
import org.apache.commons.math3.random.RandomGenerator;

import org.jgrapht.Graph;
import org.jgrapht.Graphs;
import org.jgrapht.graph.DefaultDirectedWeightedGraph;
import org.jgrapht.graph.DefaultWeightedEdge;

import java.util.List;
import java.util.HashMap;
import java.util.Map;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;

import org.json.simple.JSONObject;
import org.json.simple.JSONArray;

import javafx.geometry.Point2D;

/**
 * Representation of an RDLP Instance
 */
public class RDLPInstance {
  /* Instance name */
  public final String name;
  /* Customers */
  public final List<Customer> customers;
  /* Routing graph */
  public final Graph<Location, DefaultWeightedEdge> routingGraph;
  /* Routing graph EXPECTED*/
  public Graph<Location, DefaultWeightedEdge> routingGraphExp;
  /* Vehicle Source Depot */
  public final Location sourceDepot;
  /* Vehicle Target Depot */
  public final Location targetDepot;
  /* Vehicle capacity */
  public final int VEHIC_CAP;
  /* Number of customers */
  public final int NR_CUST;
  /* Number of locations */
  public final int NR_LOC;
  /* Time Horizon */
  public final int TIME_HORIZON;

  public int MAX_ROUTES;

  /*scenario generation*/
  private final int SEED = 13131313;

  /* Some gamma distribution with mean equal to 1.0 */
  private final double shape;  // NOTE:parameters for gamma distribution for
                               // random travel time for one unit of time
  private final double scale;  // NOTE:to sample T_ij, scale parameter change (i.e.scale*t_ij)
  private final double CV2;
  private final double congestion_level = 0.35;
  public final GammaDistribution gammaDistribution;
  public final Map<DefaultWeightedEdge, GammaDistribution> arcMapDistribution = new HashMap();

  public RDLPInstance(String name, List<Customer> customers,
                      Graph<Location, DefaultWeightedEdge> routingGraph,
                      Location sourceDepot, Location targetDepot, int vehicCap,
                      double cv2) {
    this.name = name;
    this.customers = customers;
    this.routingGraph = routingGraph;
    this.sourceDepot = sourceDepot;
    this.targetDepot = targetDepot;
    VEHIC_CAP = vehicCap;
    NR_CUST = customers.size();
    NR_LOC = routingGraph.vertexSet().size();
    TIME_HORIZON = targetDepot.end;
    MAX_ROUTES = NR_CUST;

    this.CV2 = cv2;

    shape = 1 / CV2;
    scale = CV2;

    // System.out.println("CV="+cv2+" shape="+shape+" scale="+scale+"*t_ij");
    // scenario generation
//  RandomGenerator rng = new JDKRandomGenerator((int)System.currentTimeMillis());
    RandomGenerator rng = new JDKRandomGenerator(SEED);

    gammaDistribution =
        new GammaDistribution(rng, shape, scale);  //~G(shape,scale)

    // remove expected disruption to travel time (congestion_level*t_{ij})
    // before creating the distribution for modelling disruption
    for (DefaultWeightedEdge e : routingGraph.edgeSet())  //~G(shape,scale*t_ij)
      if (routingGraph.getEdgeWeight(e) > 0)              // strictly positive
        arcMapDistribution.put(e, new GammaDistribution(rng, shape,scale *
          (congestion_level * (routingGraph.getEdgeWeight(e) / (1.0 + congestion_level)))));
      else
        arcMapDistribution.put(e, new GammaDistribution(rng, shape, scale));

    // compute penalty for not servicing a customer
    double penalty = 0.0;
    double avg_penalty = 0.0;
    for (Customer c : customers) {  // assuming first location in the Location
                                    // list is the customer's home
      for (Location l : c.locations) {
        if (l != sourceDepot && l != targetDepot) {
          penalty = ((int)routingGraph.getEdgeWeight(
                         routingGraph.getEdge(sourceDepot, l)) +
                     (int)routingGraph.getEdgeWeight(
                         routingGraph.getEdge(l, targetDepot)));
          break;
        }
      }
      c.penalty = penalty;
      avg_penalty += penalty;
      // System.out.println("Customer: "+c+" penalty: "+penalty);
    }

    avg_penalty = avg_penalty / (NR_CUST - 2);
    // for(Customer c : customers)
    //   c.penalty = avg_penalty;
    //
    // System.out.println("Avg penalty:"+avg_penalty);
  }

  /**
   * Returns the travel time between two locations
   * @param l1 first location
   * @param l2 second location
   * @return travel time
   */
  public double getTravelTime(Location l1, Location l2) {
    return routingGraph.getEdgeWeight(routingGraph.getEdge(l1, l2));
  }

  public double maxCustomerRange() {
    double max_range = 0;
    double max_RANGE = 0;
    double min_range = Double.MAX_VALUE;
    double min_RANGE = Double.MAX_VALUE;
    double avg_locations = 0;
    double avg_widthTW = 0;
    for (Customer c : customers) {
      max_range = 0;
      min_range = Double.MAX_VALUE;
      if (c.locations.size() > 1)
        for (Location l1 : c.locations)
          for (Location l2 : c.locations)
            if (l1 != l2 && l1 != sourceDepot && l1 != targetDepot) {
              if (getTravelTime(l1, l2) >
                  0)  // first and last locations are the customer's home
                min_range = Math.min(getTravelTime(l1, l2), min_range);
              max_range = Math.max(getTravelTime(l1, l2), max_range);
            }
      // System.out.println("Customer: "+c);
      // System.out.println("Max customer range:"+max_range);
      // System.out.println("Min customer range:"+min_range);

      max_RANGE = Math.max(max_RANGE, max_range);
      min_RANGE = Math.min(min_RANGE, min_range);
    }
    // System.out.println("Max customer range:"+max_RANGE);
    // System.out.println("Min customer range:"+min_RANGE);
    return max_RANGE;
  }

  public void jsonify(String jsonFile) {
    try (BufferedWriter br = new BufferedWriter(new FileWriter(jsonFile))) {
      JSONObject obj = new JSONObject();
      JSONObject locs = new JSONObject();
      boolean flag;

      // for each customer, a list with the correspondent locations
      for (Customer c : customers) {
        JSONArray roaming = new JSONArray();
        flag = false;
        for (Location l : c.locations) {
          roaming.add(l.toString());
          flag = l == sourceDepot || l == targetDepot ? true : false;
        }
        if (!flag)  // do not store depots as customers, only as locations
          obj.put(c.toString(), roaming);
      }

      // the full list of locations
      for (Customer c : customers) {
        for (Location l : c.locations) {
          JSONObject loc = new JSONObject();
          Point2D coord = l.coordinate;
          loc.put("x", (int)(coord.getX()));
          loc.put("y", (int)(coord.getY()));
          if (l == sourceDepot)
            locs.put("DepotI", loc);
          else if (l == targetDepot)
            locs.put("DepotF", loc);
          else
            locs.put(l.toString(), loc);
        }
      }
      obj.put("Locations", locs);

      StringWriter out = new StringWriter();
      obj.writeJSONString(out);
      String jsonText = out.toString();
      br.write(jsonText);
      br.close();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  @Override
  public String toString() {
    return "Instance: " + name;
  }
}
