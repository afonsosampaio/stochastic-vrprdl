package rdlp.model;

import javafx.geometry.Point2D;

/**
 * Class representing a location. Every location belongs to a unique customer.
 * If two customers share a physical location, then this has to be represented
 * by 2 Location instances having the same coordinates.
 */
public class Location {
  /* Unique ID of this location */
  public final int ID;
  /* Coordinates of the location */
  public final Point2D coordinate;
  /* Left hand side of time window during which this location can be visited */
  public int start;
  /* Right hand side of time window during which this location can be visited */
  public int end;  // final
  /* Unique customer to which this location belongs*/
  public Customer customer;

  public int start_aux;
  public int end_aux;

  public Location(int id, Point2D coordinate, int start, int end) {
    ID = id;
    this.coordinate = coordinate;
    this.start = start;
    this.end = end;
  }

  /**
   * Sets the customer associated with this location
   * @param customer customer associated with this location
   */
  void linkCustomer(Customer customer) { this.customer = customer; }

  @Override
  public int hashCode() {
    return ID;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == null || !(obj instanceof Location)) return false;
    Location other = (Location)obj;
    return this.ID == other.ID;
  }

  @Override
  public String toString() {
    return String.format("c%d(l%d)", customer.ID, ID);
  }
}
