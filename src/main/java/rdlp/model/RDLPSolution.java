package rdlp.model;

import java.util.HashSet;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;

import rdlp.algorithms.heuristic.*;

import rdlp.model.Customer;
import rdlp.model.Location;

import org.jgrapht.Graph;
import org.jgrapht.graph.DefaultWeightedEdge;

/**
 * Representation of an RDLP Solution. The solution is presented as a list of
 * routes, one route for each vehicle. The solution does not explicitly record
 * start and end times for each customer and/or location. For a given feasible
 * route, a schedule that satisfies all time windows can be computed in linear
 * time.
 */
public class RDLPSolution {
  private final RDLPInstance dataModel;
  public final List<List<Location>> routes;
  public final int objective;

  public RDLPSolution(RDLPInstance dataModel, List<List<Location>> routes,
                      int objective) {
    this.dataModel = dataModel;
    this.routes = routes;
    this.objective = objective;
  }

  /**
   * Convencience method which is used to verify the correctness of the
   * solution.
   */
  public void verify(final Graph<Location, DefaultWeightedEdge> routingGraph) {
    int objectiveVerified = 0;
    Set<Customer> visited = new HashSet<>();

    for (List<Location> route : routes) {
      if (route.isEmpty()) throw new RuntimeException("Route is empty");
      // Routes must start and end at resp. the source and target depot
      if (route.get(0) != dataModel.sourceDepot)
        throw new RuntimeException(
            "Route does not start at source depot. Route: " + route);
      if (route.get(route.size() - 1) != dataModel.targetDepot)
        throw new RuntimeException(
            "Route does not end at target depot. Route: " + route);
      // Every customer can only be visited once, and vehicle demand cannot be
      // exceeded
      int demand = 0;
      for (Location location : route.subList(1, route.size() - 1)) {
        boolean visit = visited.add(location.customer);
        if (!visit)
          throw new RuntimeException("Customer " + location.customer +
                                     " is visited multiple times!");
        demand += location.customer.demand;
      }
      if (demand > dataModel.VEHIC_CAP)
        throw new RuntimeException(
            "Vehicle capacity exceeded! Capacity: " + dataModel.VEHIC_CAP +
            " sum of demands in route: " + demand);

      // Ensure that a feasible schedule that satisfies all time windows exists
      int startTime = dataModel.sourceDepot.start;
      for (int i = 0; i < route.size() - 1; i++) {
        int travelTime = 0;
        travelTime = (int)routingGraph.getEdgeWeight(
            routingGraph.getEdge(route.get(i), route.get(i + 1)));
        objectiveVerified += travelTime;
        startTime = Math.max(startTime + travelTime, route.get(i + 1).start);
        if (startTime > route.get(i + 1).end)
          throw new RuntimeException(
              "Customer visited outside time window! Time: " + startTime +
              " location: " + route.get(i + 1));
//        else
//          System.out.println(route.get(i + 1) + "@" + startTime + ","+travelTime);
      }
    }

    // Ensure that all customers have been visited
    visited.add(dataModel.sourceDepot.customer);
    visited.add(dataModel.targetDepot.customer);
    if (visited.size() != dataModel.NR_CUST)
      throw new RuntimeException(
          "Not all customers have been visited! Visited: " + visited);

    // Verify objective
    if (objective != objectiveVerified)
      throw new RuntimeException("Objective is invalid. Expected: " +
                                 objective + " verified: " + objectiveVerified);
  }

  @Override
  public String toString() {
    StringBuilder s = new StringBuilder();
    for (List<Location> route : routes) s.append(route).append("\n");
    return s.toString();
  }

  public static RDLPSolution convertSolution(HeuristicSolution sol,
                                             RDLPInstance dataModel) {
    if (sol == null) return null;

    // else, construct RDLPSolution
    List<List<Location>> routes = new ArrayList<>();
    for (Route route : sol.routes) {
      if (route.size() ==
          2)  // empty route containing just the source and target depot.
        continue;
      List<Location> locationSequence = new ArrayList<>();
      for (Node node : route) locationSequence.add(node.location);
      routes.add(locationSequence);
    }
    return new RDLPSolution(dataModel, routes, (int)sol.getObjective());
  }
}
