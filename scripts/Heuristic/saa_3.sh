#!/bin/bash

#benchmark for the greedy skip recourse action

set -e
#get the root dir (3rd ancestor of the location where this script is stored)
SRC_DIR=`dirname "$BASH_SOURCE"`/../..

function runBenchmark(){
  instances=($(cat ./scripts/Heuristic/instances.lst))
  outputdir=./output
  filename=R3_CV025.txt

  message="Sample Average Approximation benchmark"

  #test whether output dir exists
  if ! [ -d "$outputdir" ]; then
    mkdir ${outputdir}
  fi

  #create the output file and write the message and header to this file
  outputfile=$outputdir/$filename
  touch $outputfile
  echo "${prefix}${message}" > $outputfile

  #compile the code to ensure that we are running the latest version
  #mvn clean; mvn package

  echo "${prefix}Percentile=10%" >> $outputfile
  #run the benchmark
  printf "%s\n" "${instances[@]}" | parallel --no-notice -P 10 -k --eta --colsep ' ' "java -Xmx5000m -cp ./target/rdlp-4.2.jar rdlp.benchmark.SAABenchmark -inst {} -rec 3 -N 1 -cv 0.25 -Np 10000 -te 0.1" >> $outputfile

  echo "${prefix}Percentile=25%" >> $outputfile
  #run the benchmark
  printf "%s\n" "${instances[@]}" | parallel --no-notice -P 10 -k --eta --colsep ' ' "java -Xmx5000m -cp ./target/rdlp-4.2.jar rdlp.benchmark.SAABenchmark -inst {} -rec 3 -N 1 -cv 0.25 -Np 10000 -te 0.25" >> $outputfile

  echo "${prefix}Percentile=50%" >> $outputfile
  #run the benchmark
  printf "%s\n" "${instances[@]}" | parallel --no-notice -P 10 -k --eta --colsep ' ' "java -Xmx5000m -cp ./target/rdlp-4.2.jar rdlp.benchmark.SAABenchmark -inst {} -rec 3 -N 1 -cv 0.25 -Np 10000 -te 0.5" >> $outputfile

  echo "${prefix}Percentile=75%" >> $outputfile
  #run the benchmark
  printf "%s\n" "${instances[@]}" | parallel --no-notice -P 10 -k --eta --colsep ' ' "java -Xmx5000m -cp ./target/rdlp-4.2.jar rdlp.benchmark.SAABenchmark -inst {} -rec 3 -N 1 -cv 0.25 -Np 10000 -te 0.75" >> $outputfile

}

pushd $SRC_DIR
runBenchmark
popd
