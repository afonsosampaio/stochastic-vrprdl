#!/bin/bash

set -e
#get the root dir (3rd ancestor of the location where this script is stored)
SRC_DIR=`dirname "$BASH_SOURCE"`/../../..

function runBenchmark(){
  instances=($(cat ./scripts/Heuristic/instances.lst))
  outputdir=./output
  filename=DeterministicBenchmark_CV100.txt

  message="Improvement Heuristic benchmark"
  header="name    customers   RoutingC    Recourse0   Recourse1   Recourse2   Recourse3   runtime"

  #test whether output dir exists
  if ! [ -d "$outputdir" ]; then
    mkdir ${outputdir}
  fi

  #create the output file and write the message and header to this file
  outputfile=$outputdir/$filename
  touch $outputfile
  echo "${prefix}${message}" > $outputfile
  echo "${prefix}${header}" >> $outputfile

  #compile the code to ensure that we are running the latest version
  #mvn clean; mvn package

  #run the benchmark
  printf "%s\n" "${instances[@]}" | parallel --no-notice -P 2 -k --eta --colsep ' ' "java -Xmx3000m -cp ./target/rdlp-3.0.jar rdlp.benchmark.ImprovementBenchmark -inst {} -cv 1.00" >> $outputfile
}

#pushd $SRC_DIR
runBenchmark
#popd
