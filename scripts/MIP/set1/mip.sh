#!/bin/bash

set -e
#get the root dir (3rd ancestor of the location where this script is stored)
SRC_DIR=`dirname "$BASH_SOURCE"`/../../..

# Invoke MIP on the rdlp instance files in paralel.
function runBenchmark(){
    
    instances=($(cat ./scripts/MIP/set1/instances.lst))
    outputdir=./output
    filename=MIPBenchmark.txt
    

    message="MIP benchmark, 5 min, 4 threads"
    header="name	nrCustomers    nrLocations LB   UB	feasible    optimal runtime"
    prefix="tag: "
    
    #read local configuration
    if test -f local.conf ; then
        . local.conf
    else
        echo "local.conf not found!"
        exit 1
    fi

    #test whether output dir exists
    if ! [ -d "$outputdir" ]; then
        mkdir ${outputdir}
    fi

    #create the output file and write the message and header to this file
    outputfile=$outputdir/$filename
    touch $outputfile
    echo "${prefix}${message}" > $outputfile
    echo "${prefix}${header}" >> $outputfile
    
    #compile the code to ensure that we are running the latest version
    mvn clean; mvn package

    #run the benchmark
    printf "%s\n" "${instances[@]}" | parallel --no-notice -P 2 -k --eta --colsep ' ' "java -Djava.library.path=${CPLEX_PATH}:${CP_PATH} -cp ./target/rdlp-1.0.jar rdlp.benchmark.MIPBenchmark {}" >> $outputfile
    
    #printf "HERE ${objective} %s\n" "${instances[@]}" | parallel -P 2 -k --eta --colsep ' ' 'java -Djava.library.path=/opt/ILOG/CPLEX_Studio128/cplex/bin/x86-64_linux:/opt/ILOG/CPLEX_Studio128/cpoptimizer/bin/x86-64_linux -cp /opt/ILOG/CPLEX_Studio128/cpoptimizer/lib/ILOG.CP.jar:./snowplowing.jar snowPlowing.benchmarks.BoundsBenchmark {} {} {}' >> ${outputFile}

    #only preserve tagged lines, and remove the tag
    mv $outputfile "${outputfile}_tmp"
    grep ${prefix} "${outputfile}_tmp" | sed -e "s/$prefix//g" > $outputfile
    rm "${outputfile}_tmp"

}


#switch to the root directory. This allows us to invoke this script from any directory. Then run the benchmark.
pushd $SRC_DIR
runBenchmark
popd
