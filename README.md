## Software requirements: ##

- Java 8
- Maven 3
- git (apt-get install git)
- Cplex 12.8 (https://ibm.onthehub.com/WebStore/OfferingDetails.aspx?o=613c3d21-0ce1-e711-80fa-000d3af41938)
- GNU parallel (apt-get install parallel)


## Installation: ##

1. Clone repository: `git clone ...`
2. Install dependencies: `mvn validate`
3. Copy `./etc/local.conf_example` to `./`, rename to `local.conf` and update its contents
4. Compile the code: `mvn package`
5. Run the example benchmark mip.sh under ./scripts/MIP/set1/